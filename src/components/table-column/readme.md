# gl-table-column

The gl-table-column component defines one column of a [gl-table](../table/).
One will want to create a column for each feature property one wishes to display in the table.

<!-- Auto Generated Below -->


## Properties

| Property             | Attribute              | Description                                                                                                                                                                                                                                                                                   | Type      | Default     |
| -------------------- | ---------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ----------- |
| `attribute`          | `attribute`            | What attribute in the data this specific column represents                                                                                                                                                                                                                                    | `string`  | `undefined` |
| `cellClassAttribute` | `cell-class-attribute` | The name of the attribute which holds the css class name for the cells of this column.                                                                                                                                                                                                        | `string`  | `undefined` |
| `component`          | `component`            | The name of the JSX component which is used to render this cell, if any                                                                                                                                                                                                                       | `string`  | `undefined` |
| `componentProps`     | `component-props`      | Additional properties passed to a JSX component used to render this cell                                                                                                                                                                                                                      | `any`     | `undefined` |
| `csv`                | `csv`                  | Whether or not this column will be exported if the user exports the table as a csv document. In effect setting this option to false is a way to exclude this column from csv exports.                                                                                                         | `boolean` | `true`      |
| `format`             | `format`               | If the contents of this column are text based, the format of that text. This could either be the strings "currency", "decimal", "percent", or an object that represents the options sent to a toLocaleString call.  E.g. format="currency" or format={style:"decimal", notation:"scientific"} | `any`     | `undefined` |
| `label`              | `label`                | A label for this column, used in the toggle-visibility button and when choosing ascending or descending sort order.                                                                                                                                                                           | `string`  | `undefined` |
| `pdf`                | `pdf`                  | Whether or not this column will be exported if the user exports the table as a pdf document. In effect setting this option to false is a way to exclude this column from pdf exports.                                                                                                         | `boolean` | `true`      |
| `sortable`           | `sortable`             | Whether or not the data can be sorted by the contents of this column.                                                                                                                                                                                                                         | `boolean` | `true`      |
| `visible`            | `visible`              | Whether or not this column is rendered and visible to the user.                                                                                                                                                                                                                               | `boolean` | `true`      |


## Events

| Event                  | Description                                                                                | Type               |
| ---------------------- | ------------------------------------------------------------------------------------------ | ------------------ |
| `glTableColumnChanged` | An event emitted when the column updates for any reason, including when it is disconnected | `CustomEvent<any>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
