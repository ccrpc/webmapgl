import { Component, Event, EventEmitter, Prop, Watch } from "@stencil/core";

@Component({
  tag: "gl-table-column",
})
export class TableColumn {
  /**
   * An event emitted when the column updates for any reason, including when it is disconnected
   */
  @Event() glTableColumnChanged: EventEmitter;

  /**
   * What attribute in the data this specific column represents
   */
  @Prop() readonly attribute: string;

  /**
   * The name of the attribute which holds the css class name for the cells of this column.
   */
  @Prop() readonly cellClassAttribute: string;

  /**
   * The name of the JSX component which is used to render this cell, if any
   */
  @Prop() readonly component: string;

  /**
   * Additional properties passed to a JSX component used to render this cell
   */
  @Prop() readonly componentProps: any;

  /**
   * Whether or not this column will be exported if the user exports the table as a csv document.
   * In effect setting this option to false is a way to exclude this column from csv exports.
   */
  @Prop() readonly csv: boolean = true;

  /**
   * If the contents of this column are text based, the format of that text.
   * This could either be the strings "currency", "decimal", "percent", or an object that represents the options sent to a toLocaleString call.
   *
   * E.g. format="currency" or format={style:"decimal", notation:"scientific"}
   */
  @Prop({ mutable: true }) format: string | any;

  /**
   * A label for this column, used in the toggle-visibility button and when choosing ascending or descending sort order.
   */
  @Prop() readonly label: string;

  /**
   * Whether or not this column will be exported if the user exports the table as a pdf document.
   * In effect setting this option to false is a way to exclude this column from pdf exports.
   */
  @Prop() readonly pdf: boolean = true;

  /**
   * Whether or not the data can be sorted by the contents of this column.
   */
  @Prop() readonly sortable: boolean = true;

  /**
   * Whether or not this column is rendered and visible to the user.
   */
  @Prop() readonly visible: boolean = true;

  componentWillLoad() {
    this.updateFormat();
  }

  componentDidLoad() {
    this.change();
  }

  componentDidUpdate() {
    this.change();
  }

  disconnectedCallback() {
    this.change();
  }

  @Watch("format")
  updateFormat() {
    if (typeof this.format === "string") {
      let format: any;
      try {
        format = JSON.parse(this.format);
      } catch (e) {
        if (/SyntaxError/.test(e)) {
          if (["currency", "decimal", "percent"].indexOf(this.format) !== -1) {
            format = {
              style: this.format,
            };
            if (this.format === "currency") format["currency"] = "USD";
          }
        } else {
          throw e;
        }
      }
      this.format = format;
    }
  }

  private change() {
    this.glTableColumnChanged.emit(this);
  }
}
