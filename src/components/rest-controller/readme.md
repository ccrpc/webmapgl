# gl-rest-controller

The REST controller is designed to communicate with a [Features REST
API](https://gitlab.com/ccrpc/features). It can be used to persist user-added
GeoJSON features in a PostGIS database.

## Options
For details of the options available, see the [`RestOptions`
interface](interface.ts).

<!-- Auto Generated Below -->


## Methods

### `create(feature: any, options: RestOptions) => Promise<Response>`

Makes a request to a features REST API. It resolves to the server
response object.

#### Returns

Type: `Promise<Response>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
