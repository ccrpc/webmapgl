import { Component, Method } from "@stencil/core";
import { RestOptions } from "./interface";

@Component({
  tag: "gl-rest-controller",
})
export class RestController {
  /**
   * Makes a request to a features REST API. It resolves to the server
   * response object.
   * @param feature GeoJSON feature to be sent to the API.
   * @param options Options object.
   */
  @Method()
  async create(feature: any, options: RestOptions) {
    let headers = {};
    headers["Content-Type"] = "application/json";
    if (options.token != undefined)
      headers["Authorization"] = "Bearer " + options.token;

    return await fetch(options.url, {
      body: options.method === "GET" ? undefined : JSON.stringify(feature),
      headers: headers,
      method: options.method || "POST",
      mode: options.mode || "cors",
    });
  }
}
