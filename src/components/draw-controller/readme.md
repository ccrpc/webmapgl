# gl-draw-controller

The draw controller component starts a drawing session, allowing the user
to add or edit features on the map. The drawing session can be initialized
with a GeoJSON feature collection. It also accepts options that determine
the drawing operations available.

## Options


| Option         | Description                                                                                                                                                     | Type                          | Default     |
| -------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------- | ----------- |
| `combine`      | Features can be combined and split.                                                                                                                             | `boolean`                     | `false`     |
| `delete`       | Features can be deleted.                                                                                                                                        | `boolean`                     | `false`     |
| `mapId`        | ID of the [gl-map](../map) being edited.                                                                                                                        | `string`                      | `undefined` |
| `mode`         | Default drawing mode. See the [Mapbox GL Draw documentation](https://github.com/mapbox/mapbox-gl-draw/blob/main/docs/API.md#modes) for details.                 | `draw`, `simple`, or `select` | `draw`      |
| `multiple`     | Multiple features can be added.                                                                                                                                 | `boolean`                     | `false`     |
| `styles`       | An array of map style objects. See the [Mapbox GL Draw documentation](https://github.com/mapbox/mapbox-gl-draw/blob/main/docs/API.md#styling-draw) for details. | `Array`                       | `undefined` |
| `toolbarLabel` | Label for the [gl-draw-toolbar](../draw-toolbar).                                                                                                               | `string`                      | `undefined` |
| `type`         | Type of feature to draw.                                                                                                                                        | `point`, `line`, or `polygon` | `point`     |

<!-- Auto Generated Below -->


## Methods

### `create(featureCollection: FeatureCollection, options: DrawOptions) => Promise<any>`

Starts a drawing session.

#### Returns

Type: `Promise<any>`




## Dependencies

### Depends on

- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-draw-controller --> ion-thumbnail
  style gl-draw-controller fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
