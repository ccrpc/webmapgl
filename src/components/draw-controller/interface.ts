import { Style } from "maplibre-gl";

export interface DrawOptions {
  combine?: boolean;
  delete?: boolean;
  mapId?: string;
  mode?: "draw" | "simple" | "direct";
  multiple?: boolean;
  styles?: Style[];
  toolbarLabel?: string;
  type?: "point" | "line" | "polygon";
}
