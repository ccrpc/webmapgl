import { Component, Method } from "@stencil/core";
import { DrawOptions } from "./interface";
import { getMap } from "../utils";
import { FeatureCollection } from "geojson";

@Component({
  tag: "gl-draw-controller",
})
export class DrawController {
  private defaultOptions: DrawOptions = {
    type: "point",
    multiple: false,
    combine: false,
    delete: false,
    mode: "draw",
  };

  /**
   * Starts a drawing session.
   * @param featureCollection Features with which the session will be
   * initialized.
   * @param options Drawing session options.
   */
  @Method()
  async create(
    featureCollection: FeatureCollection,
    options: DrawOptions
  ): Promise<any> {
    let map = getMap(options.mapId);
    if (map == undefined) throw "Draw action map not found";

    let toolbar = document.querySelector("gl-draw-toolbar");
    if (toolbar != undefined && options.toolbarLabel != undefined)
      toolbar.label = options.toolbarLabel;

    map.drawOptions = this.getControlOptions(options);
    map.drawing = true;
    if (featureCollection != undefined) map.draw.set(featureCollection);

    return await new Promise<FeatureCollection>((resolve) => {
      let cancel = () => {
        map.drawing = false;
        resolve(undefined);
        toolbar.removeEventListener("glDrawCancel", cancel);
        toolbar.removeEventListener("glDrawConfirm", confirm);
      };
      let confirm = () => {
        let featureCollection = map.draw.getAll();
        map.drawing = false;
        resolve(featureCollection);
        toolbar.removeEventListener("glDrawConfirm", confirm);
        toolbar.removeEventListener("glDrawConfirm", confirm);
      };
      toolbar.addEventListener("glDrawCancel", cancel);
      toolbar.addEventListener("glDrawConfirm", confirm);
    });
  }

  private getControlOptions(options?: DrawOptions) {
    let opts = { ...this.defaultOptions, ...(options || {}) };

    let mode = "simple_select";
    if (opts.mode === "direct") mode = "direct_select";
    if (opts.mode === "draw") {
      if (opts.type === "point") mode = "draw_point";
      if (opts.type === "line") mode = "draw_line_string";
      if (opts.type === "polygon") mode = "draw_polygon";
    }

    let result = {
      controls: {
        point: opts.multiple && opts.type === "point",
        line_string: opts.multiple && opts.type === "line",
        polygon: opts.multiple && opts.type === "polygon",
        trash: opts.delete,
        combine_features: opts.multiple && opts.combine,
        uncombine_features: opts.multiple && opts.combine,
      },
      defaultMode: mode,
    };

    if (opts.styles != undefined) (result as any).styles = opts.styles;

    return result;
  }
}
