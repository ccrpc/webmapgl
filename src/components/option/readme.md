# gl-option

The option component represents a choice in a [`gl-field`](../field) of type
select.

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                                                                              | Type     | Default     |
| -------- | --------- | ------------------------------------------------------------------------------------------------------------------------ | -------- | ----------- |
| `image`  | `image`   | Thumbnail image URL.                                                                                                     | `string` | `undefined` |
| `label`  | `label`   | Option label. It is displayed to the user.                                                                               | `string` | `undefined` |
| `value`  | `value`   | Option value. If this option is selected, it is used to set the value of the property corresponding to the parent field. | `any`    | `undefined` |


## Events

| Event             | Description                                        | Type               |
| ----------------- | -------------------------------------------------- | ------------------ |
| `glOptionChanged` | Emitted when the option is selected or deselected. | `CustomEvent<any>` |


## Dependencies

### Used by

 - [gl-form-page](../form-page)

### Depends on

- ion-item
- ion-radio
- ion-label
- ion-select-option
- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-option --> ion-item
  gl-option --> ion-radio
  gl-option --> ion-label
  gl-option --> ion-select-option
  gl-option --> ion-thumbnail
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  gl-form-page --> gl-option
  style gl-option fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
