import {
  h,
  Component,
  Element,
  Event,
  EventEmitter,
  Prop,
} from "@stencil/core";
import { getThumbnail } from "../utils";

@Component({
  tag: "gl-option",
})
export class Option {
  @Element() el: HTMLGlOptionElement;

  /**
   * Emitted when the option is selected or deselected.
   */
  @Event() glOptionChanged: EventEmitter;

  /**
   * Thumbnail image URL.
   */
  @Prop() readonly image: string;

  /**
   * Option label. It is displayed to the user.
   */
  @Prop() readonly label: string;

  /**
   * Option value. If this option is selected, it is used to set the value
   * of the property corresponding to the parent field.
   */
  @Prop() readonly value: any;

  private select() {
    this.glOptionChanged.emit(this.value);
  }

  private callSelect = () => this.select();

  private getRadio() {
    const field = this.el.closest("gl-field");
    const checked = field.getValue() === this.value;
    return (
      <ion-item>
        {getThumbnail(this)}
        <ion-radio
          onIonSelect={this.callSelect}
          value={this.value}
          checked={checked}
        >
          <ion-label text-wrap>{this.label}</ion-label>
        </ion-radio>
      </ion-item>
    );
  }

  private getSelectOption() {
    return (
      <ion-select-option value={this.value}>{this.label}</ion-select-option>
    );
  }

  render() {
    let field = this.el.closest("gl-field");
    return field.widget === "radio" ? this.getRadio() : this.getSelectOption();
  }
}
