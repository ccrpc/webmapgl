import { h, Component, Element, Prop, Host } from "@stencil/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-table-column-visibility",
})
export class TableColumnVisibility {
  @Element() el: HTMLGlTableColumnVisibilityElement;

  /**
   * The title of the column.
   */
  @Prop() readonly label: string = _t("webmapgl.table-column-visibility.label");

  /**
   * The id of the table this column is a part of.
   */
  @Prop() readonly tableId: string;

  private closePopover() {
    this.el.closest("ion-modal").dismiss();
  }

  private createToggles() {
    let id = this.tableId != undefined ? `#${this.tableId}` : "";
    let selector = `gl-table${id} gl-table-column`;
    return Array.from(document.querySelectorAll(selector)).map(
      (column: HTMLGlTableColumnElement) => (
        <gl-table-column-visibility-toggle
          column={column}
          enabled={column.visible}
        ></gl-table-column-visibility-toggle>
      )
    );
  }

  private closeClick = () => this.closePopover();

  render() {
    return (
      <Host>
        <ion-header>
          <ion-toolbar>
            <ion-title>{this.label}</ion-title>
            <ion-buttons slot="end">
              <ion-button
                title={_t("webmapgl.table-column-visibility.close")}
                onClick={this.closeClick}
              >
                <ion-icon slot="icon-only" name="close" />
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <ion-list>{this.createToggles()}</ion-list>
        </ion-content>
      </Host>
    );
  }
}
