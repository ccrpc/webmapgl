# gl-table-column-visibility

The gl-table-column-visibility component lists the columns in a [gl-table](../table/) and allow the user to decide whether or not each column should be displayed.

<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description                                   | Type     | Default                                        |
| --------- | ---------- | --------------------------------------------- | -------- | ---------------------------------------------- |
| `label`   | `label`    | The title of the column.                      | `string` | `_t("webmapgl.table-column-visibility.label")` |
| `tableId` | `table-id` | The id of the table this column is a part of. | `string` | `undefined`                                    |


## Dependencies

### Used by

 - [gl-table-column-visibility-button](../table-column-visibility-button)

### Depends on

- [gl-table-column-visibility-toggle](../table-column-visibility-toggle)
- ion-header
- ion-toolbar
- ion-title
- ion-buttons
- ion-button
- ion-icon
- ion-content
- ion-list

### Graph
```mermaid
graph TD;
  gl-table-column-visibility --> gl-table-column-visibility-toggle
  gl-table-column-visibility --> ion-header
  gl-table-column-visibility --> ion-toolbar
  gl-table-column-visibility --> ion-title
  gl-table-column-visibility --> ion-buttons
  gl-table-column-visibility --> ion-button
  gl-table-column-visibility --> ion-icon
  gl-table-column-visibility --> ion-content
  gl-table-column-visibility --> ion-list
  gl-table-column-visibility-toggle --> ion-item
  gl-table-column-visibility-toggle --> ion-label
  gl-table-column-visibility-toggle --> ion-toggle
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-toggle --> ion-icon
  ion-button --> ion-ripple-effect
  gl-table-column-visibility-button --> gl-table-column-visibility
  style gl-table-column-visibility fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
