import { h, Component, Prop } from "@stencil/core";
import { popoverController } from "@ionic/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-share-button",
})
export class ShareButton {
  /**
   * Icon from the Ionicons set used in the button.
   */
  @Prop() readonly icon: string = "share-social";

  private async openPopover(ev: UIEvent) {
    const options = {
      component: document.createElement("gl-share"),
      ev: ev,
    };
    const popover = await popoverController.create(options);
    await popover.present();
    return popover;
  }

  private clickOpen = async (e: UIEvent) => await this.openPopover(e);

  render() {
    return (
      <ion-button
        title={_t("webmapgl.share-button.title")}
        onClick={this.clickOpen}
      >
        <ion-icon slot="icon-only" name={this.icon}></ion-icon>
      </ion-button>
    );
  }
}
