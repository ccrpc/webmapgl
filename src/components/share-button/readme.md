# gl-share-button

The share button component opens an `ion-popover` containing the
[`gl-share`](../share) component.

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                    | Type     | Default          |
| -------- | --------- | ---------------------------------------------- | -------- | ---------------- |
| `icon`   | `icon`    | Icon from the Ionicons set used in the button. | `string` | `"share-social"` |


## Dependencies

### Depends on

- [gl-share](../share)
- ion-button
- ion-icon

### Graph
```mermaid
graph TD;
  gl-share-button --> gl-share
  gl-share-button --> ion-button
  gl-share-button --> ion-icon
  gl-share --> ion-item
  gl-share --> ion-icon
  gl-share --> ion-label
  gl-share --> ion-list
  gl-share --> ion-list-header
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-button --> ion-ripple-effect
  style gl-share-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
