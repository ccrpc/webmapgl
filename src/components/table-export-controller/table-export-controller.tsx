import { Component, Method } from "@stencil/core";
import { TableExportOptions, PdfDocDef } from "./interfaces";
import { fetchPdf } from "../utils";
import { default as fileDownload } from "js-file-download";
import * as pdfMake from "pdfmake/build/pdfmake";
import { TDocumentDefinitions } from "pdfmake/interfaces";

// PDF Fonts
const pdfFonts = {
  // download default Roboto font from cdnjs.com
  Roboto: {
    normal:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/fonts/Roboto/Roboto-Regular.ttf",
    bold: "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/fonts/Roboto/Roboto-Medium.ttf",
    italics:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/fonts/Roboto/Roboto-Italic.ttf",
    bolditalics:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/fonts/Roboto/Roboto-MediumItalic.ttf",
  },
};

function csvQuote(value: string) {
  if (value == undefined) return "";
  return value.indexOf(",") !== -1 ? `"${value.replace(/"/g, '""')}"` : value;
}

@Component({
  tag: "gl-table-export-controller",
})
export class TableExportController {
  /**
   * Exports the table specified by options
   * @param options
   */
  @Method()
  async create(options: TableExportOptions) {
    let tableSelector =
      "gl-table" + (options.tableId != undefined ? `#${options.tableId}` : "");
    let table: HTMLGlTableElement =
      options.table != undefined
        ? options.table
        : document.querySelector(tableSelector);
    let features =
      options.features != undefined
        ? options.features
        : await table.getSortedFeatures();
    let columns =
      options.columns != undefined
        ? options.columns
        : Array.from(table.querySelectorAll("gl-table-column"));

    if (options.format === "csv")
      this.exportCsv(table, features, columns, options);
    if (options.format === "pdf")
      this.exportPdf(table, features, columns, options);
  }

  /**
   * Exports the contents of the table as a Csv file
   * @param table
   * @param features
   * @param columns
   * @param options
   */
  private async exportCsv(
    table: HTMLGlTableElement,
    features: { properties: { [key: string]: any } }[],
    columns: HTMLGlTableColumnElement[],
    options: TableExportOptions
  ) {
    let csv = await this.createCsvDoc(table, features, columns);
    fileDownload(csv, this.getFilename(options), "text/csv");
  }

  /**
   * Exports the contents of the table as a Pdf file
   * @param table
   * @param features
   * @param columns
   * @param options
   */
  private async exportPdf(
    table: HTMLGlTableElement,
    features: { properties: { [key: string]: any } }[],
    columns: HTMLGlTableColumnElement[],
    options: TableExportOptions
  ) {
    let dd = await this.createDocDef(table, features, columns, options);
    if (options.url != undefined) {
      fetchPdf(options.url, options.filename, dd);
    } else {
      pdfMake
        .createPdf(dd, undefined, pdfFonts)
        .download(this.getFilename(options));
    }
  }

  private getFilename(options: TableExportOptions) {
    if (options.filename != undefined) return options.filename;
    if (options.pageTitle == undefined) return `table.${options.format}`;
    return `${options.pageTitle.toLowerCase().replace(/[^a-z0-9]+/g, "_")}.${
      options.format
    }`;
  }

  private async createCsvDoc(
    table: HTMLGlTableElement,
    features: { properties: { [key: string]: any } }[],
    columns: HTMLGlTableColumnElement[]
  ) {
    let filteredColumns = columns.filter((col) => col.visible && col.csv);

    let headerRow = filteredColumns.map((col) => csvQuote(col.label));
    let featureRows = (
      await Promise.all(
        features.map((feature) =>
          this.getTableRow(table, feature, filteredColumns, "text")
        )
      )
    ).map((row: string[]) => row.map((cell) => csvQuote(cell)));

    return [headerRow, ...featureRows].map((row) => row.join(",")).join("\n");
  }

  private getTableRow(
    table: HTMLGlTableElement,
    feature: { properties: { [key: string]: any } },
    columns: HTMLGlTableColumnElement[],
    type: "pdf" | "text" = "pdf"
  ) {
    return Promise.all(
      columns.map((column) => table.getCellContents(feature, column, type))
    );
  }

  private async createDocDef(
    table: HTMLGlTableElement,
    features: { properties: { [key: string]: any } }[],
    columns: HTMLGlTableColumnElement[],
    options: TableExportOptions
  ): Promise<TDocumentDefinitions> {
    let filteredColumns = columns.filter((col) => col.visible && col.pdf);

    let header = filteredColumns.map((col) => {
      return { text: col.label, style: "th" };
    });

    let featureRows = await Promise.all(
      features.map((feature) =>
        this.getTableRow(table, feature, filteredColumns)
      )
    );

    let formattedRows = featureRows.map((row: any[], i) => {
      return row.map((cell) => {
        return {
          text: cell,
          style: i % 2 == 0 ? "td-zebra" : "td",
        };
      });
    });

    let docDef: PdfDocDef = {
      info: {},
      pageOrientation: options.pageOrientation,
      content: [
        {
          table: {
            headerRows: 1,
            body: [header, ...formattedRows],
          },
        },
      ],
    };

    if (options.pageSubtitle != undefined)
      docDef["content"].unshift({
        text: options.pageSubtitle,
        style: "h2",
        alignment: "center",
      });
    if (options.pageTitle != undefined) {
      docDef["info"]["title"] = options.pageTitle;
      docDef["content"].unshift({
        text: options.pageTitle,
        style: "h1-title",
        alignment: "center",
      });
    }
    if (options.author != undefined) docDef["info"]["author"] = options.author;
    return docDef;
  }
}
