# gl-table-export-controller

The table export controller component manages exporting
the contents of a [gl-table](../table/) to either a pdf or csv document.

<!-- Auto Generated Below -->


## Methods

### `create(options: TableExportOptions) => Promise<void>`

Exports the table specified by options

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-table-export-controller --> ion-thumbnail
  style gl-table-export-controller fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
