import { Content as PdfContent } from "pdfmake/interfaces";

export interface TableExportOptions {
  author?: string;
  columns?: HTMLGlTableColumnElement[];
  features?: { properties: { [key: string]: any } }[];
  filename?: string;
  format: "csv" | "pdf";
  pageOrientation?: "landscape" | "portrait";
  pageSubtitle?: string;
  pageTitle?: string;
  table?: HTMLGlTableElement;
  tableId?: string;
  url?: string;
}

export interface PdfDocDef {
  content: PdfContent[];
  info: PdfInfo;
  pageOrientation: "landscape" | "portrait";
  styles?: any;
}

interface PdfInfo {
  author?: string;
  title?: string;
}
