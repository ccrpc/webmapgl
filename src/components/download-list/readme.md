# gl-download-list

The download list component renders a list of
[gl-download-file](../download-file) components, allowing the user to download
files. In most cases, the download list is created automatically by a
[gl-download-button](../download-button).

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                                        | Type                          | Default     |
| -------- | --------- | ---------------------------------------------------------------------------------- | ----------------------------- | ----------- |
| `files`  | --        | Array of [gl-download-file](../download-file) elements to be included in the list. | `HTMLGlDownloadFileElement[]` | `undefined` |


## Dependencies

### Used by

 - [gl-download-button](../download-button)

### Depends on

- ion-item
- ion-label
- ion-button
- ion-icon
- ion-content
- ion-list
- ion-list-header

### Graph
```mermaid
graph TD;
  gl-download-list --> ion-item
  gl-download-list --> ion-label
  gl-download-list --> ion-button
  gl-download-list --> ion-icon
  gl-download-list --> ion-content
  gl-download-list --> ion-list
  gl-download-list --> ion-list-header
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-button --> ion-ripple-effect
  gl-download-button --> gl-download-list
  style gl-download-list fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
