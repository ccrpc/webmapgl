import { h, Prop, Component } from "@stencil/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-download-list",
})
export class DownloadList {
  /**
   * Array of [gl-download-file](../download-file) elements to be included
   * in the list.
   */
  @Prop() readonly files: HTMLGlDownloadFileElement[];

  render() {
    const header = _t("webmapgl.download-list.header");
    let fileList = this.files.map((file) => {
      let formatString = [file.format, file.size]
        .filter((item) => item)
        .join(", ");
      if (formatString != undefined) formatString = `(${formatString})`;

      return (
        <ion-item>
          <ion-label text-wrap>
            {file.label} {formatString}
          </ion-label>
          <ion-button
            slot="end"
            href={file.url}
            target="_blank"
            title={_t("webmapgl.download-list.download", { file: file.label })}
          >
            <ion-icon slot="icon-only" name="download"></ion-icon>
          </ion-button>
        </ion-item>
      );
    });

    return (
      <ion-content>
        <ion-list>
          <ion-list-header>{header}</ion-list-header>
          {fileList}
        </ion-list>
      </ion-content>
    );
  }
}
