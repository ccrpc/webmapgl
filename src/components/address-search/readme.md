# gl-address-search

The address search component displays a location search box. It uses
[gl-geocode-controller](../geocode-controller) to perform the geocoding
and displays the results to the user. If the user selects a result, the map
pans and zooms to its bounding box.

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                                             | Type                                         | Default                                |
| ------------- | -------------- | --------------------------------------------------------------------------------------- | -------------------------------------------- | -------------------------------------- |
| `bbox`        | `bbox`         | Bounding box for the search in the format [west, south, east, north].                   | `[number, number, number, number] \| string` | `undefined`                            |
| `jobId`       | `job-id`       | Identifier for the geocode operation.                                                   | `string`                                     | `"gl-address-search"`                  |
| `limit`       | `limit`        | Maximum number of results to display.                                                   | `number`                                     | `10`                                   |
| `maxZoom`     | `max-zoom`     | Maximum level the map will zoom to when a result is selected (e.g., for point results). | `number`                                     | `18`                                   |
| `mode`        | `mode`         |                                                                                         | `"ios" \| "md"`                              | `undefined`                            |
| `placeholder` | `placeholder`  | Placeholder text displayed in the input element.                                        | `string`                                     | `_t("webmapgl.address-search.prompt")` |
| `url`         | `url`          | URL of the geocoding endpoint.                                                          | `string`                                     | `undefined`                            |
| `zoomPadding` | `zoom-padding` | Padding in pixels used when zooming to a selected result.                               | `number`                                     | `20`                                   |


## Dependencies

### Depends on

- ion-item
- ion-icon
- ion-label
- ion-list
- ion-searchbar
- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-address-search --> ion-item
  gl-address-search --> ion-icon
  gl-address-search --> ion-label
  gl-address-search --> ion-list
  gl-address-search --> ion-searchbar
  gl-address-search --> ion-thumbnail
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-searchbar --> ion-icon
  style gl-address-search fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
