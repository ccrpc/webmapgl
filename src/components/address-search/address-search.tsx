import {
  Host,
  h,
  Component,
  Element,
  Listen,
  Prop,
  State,
  Watch,
} from "@stencil/core";
import { Mode } from "@ionic/core";
import { _t } from "../i18n/i18n";
import { findOrCreateOnReady } from "../utils";
import {
  ForwardGeocodeOptions,
  GeocodeResponse,
  Address,
} from "../geocode-controller/geocode-interface";

@Component({
  styleUrls: {
    ios: "address-search.ios.css",
    md: "address-search.md.css",
  },
  tag: "gl-address-search",
})
export class AddressSearchBox {
  private input?: HTMLIonSearchbarElement;
  private lazyGeocodeCtrl: HTMLGlGeocodeControllerElement;

  /**
   * @virtualProp {"ios" | "md"} mode - The mode determines which platform styles to use.
   */
  @Prop() readonly mode!: Mode;

  @Element() el: HTMLGlAddressSearchElement;

  @State() bboxArray: [number, number, number, number];
  @State() hasFocus: boolean = false;
  @State() hasValue: boolean = false;
  @State() results: GeocodeResponse[] = [];

  /** @internal */
  /**
   * Bounding box for the search in the format [west, south, east, north].
   */
  @Prop() readonly bbox: [number, number, number, number] | string;

  /**
   * Identifier for the geocode operation.
   */
  @Prop() readonly jobId: string = "gl-address-search";

  /**
   * Maximum number of results to display.
   */
  @Prop() readonly limit: number = 10;

  /**
   * Maximum level the map will zoom to when a result is selected (e.g., for point results).
   */
  @Prop() readonly maxZoom: number = 18;

  /**
   * Placeholder text displayed in the input element.
   */
  @Prop() readonly placeholder: string = _t("webmapgl.address-search.prompt");

  /**
   * URL of the geocoding endpoint.
   */
  @Prop() readonly url: string;

  /**
   * Padding in pixels used when zooming to a selected result.
   */
  @Prop() readonly zoomPadding: number = 20;

  async componentWillLoad() {
    this.parseBbox();
    if (this.lazyGeocodeCtrl == undefined)
      this.lazyGeocodeCtrl = await findOrCreateOnReady("gl-geocode-controller");
  }

  @Listen("glForwardGeocode", { target: "body" })
  async handleGeocode(
    e: CustomEvent<{
      options: ForwardGeocodeOptions;
      response: GeocodeResponse[];
    }>
  ) {
    if (e.detail.options.jobId === this.jobId) this.results = e.detail.response;
  }

  @Listen("click", { target: "body" })
  handleBodyClick() {
    this.hasFocus = false;
  }

  @Listen("keydown")
  // @ts-ignore handleKey is listening to the key presses rather than being called elsewhere.
  private handleKey(e: KeyboardEvent) {
    if (e.key === "ArrowUp") this.handleUp(e);
    else if (e.key === "ArrowDown") this.handleDown(e);
  }

  private handleDown(e: KeyboardEvent) {
    let nextItem: Element;
    if (document.activeElement.tagName === "ION-ITEM") {
      this.unsetFocus();
      nextItem = document.activeElement.nextElementSibling;
    }
    if (nextItem == undefined) nextItem = this.el.querySelector("ion-item");
    if (nextItem != null) {
      e.preventDefault();
      this.setFocus(nextItem);
    }
  }

  private handleUp(e: KeyboardEvent) {
    if (document.activeElement.tagName === "ION-ITEM") {
      e.preventDefault();
      this.unsetFocus();
      let prevItem = document.activeElement.previousElementSibling;
      if (prevItem != undefined) {
        this.setFocus(prevItem);
      } else if (this.input != undefined) {
        this.input.querySelector("input").focus();
      }
    }
  }

  @Watch("bbox")
  parseBbox() {
    this.bboxArray =
      typeof this.bbox === "string"
        ? (this.bbox.split(",").map((c) => parseFloat(c)) as [
            number,
            number,
            number,
            number
          ])
        : this.bbox;
  }

  private setFocus(el: Element) {
    let button = el.shadowRoot.querySelector("button");
    button.focus();
    button.style.color = "#3880ff";
  }

  private unsetFocus() {
    let button = document.activeElement.shadowRoot.querySelector("button");
    button.style.color = null;
  }

  private formatAddress(address: Address, displayName: string) {
    let parts = [];
    if (address.name != null && address.name !== address.city)
      parts.push(address.name);
    if (address.street != null) {
      parts.push(
        address.housenumber != null
          ? address.housenumber + " " + address.street
          : address.street
      );
    }
    if (address.city != null) parts.push(address.city);
    if (parts.length !== 0 && address.county != null)
      parts.push(address.county);
    if (parts.length !== 0) return parts.join(", ");
    else return displayName;
  }

  private async selectResult(result: GeocodeResponse) {
    let mapEl = this.el.closest("gl-map");
    mapEl.map.fitBounds(result.bbox, {
      maxZoom: this.maxZoom,
      padding: this.zoomPadding,
    });
    this.hasFocus = false;
  }

  private async geocode() {
    this.lazyGeocodeCtrl.forward({
      address: this.input.value,
      url: this.url,
      bbox: this.bboxArray,
      bounded: true,
      jobId: this.jobId,
      limit: this.limit,
    });
  }

  private resultButtonFunction(result: GeocodeResponse) {
    return () => this.selectResult(result);
  }

  private renderResults() {
    if (this.results.length === 0 || !this.hasValue || !this.hasFocus)
      return null;
    let items = this.results.map((result) => (
      <ion-item button={true} onClick={this.resultButtonFunction(result)}>
        <ion-icon name="pin" slot="start"></ion-icon>
        <ion-label>
          {this.formatAddress(result.address, result.display)}
        </ion-label>
      </ion-item>
    ));
    return <ion-list lines="full">{items}</ion-list>;
  }

  private handleChange() {
    if (this.input?.value != undefined) {
      this.hasValue = true;
      this.geocode();
    } else {
      this.hasValue = false;
      if (this.results.length !== 0) this.results = [];
    }
  }

  private handleFocus() {
    this.hasFocus = true;
  }

  private handleClick(e: MouseEvent) {
    // Prevent propagation of click events from the searchbar so that
    // it does not lose focus.
    e.stopPropagation();
  }

  private callChange = () => this.handleChange();
  private callFocus = () => this.handleFocus();
  private callClick = (e: MouseEvent) => this.handleClick(e);

  render() {
    return (
      <Host>
        <ion-searchbar
          ref={(r: HTMLIonSearchbarElement) => (this.input = r)}
          placeholder={this.placeholder}
          onIonChange={this.callChange}
          onIonFocus={this.callFocus}
          onClick={this.callClick}
        ></ion-searchbar>
        {this.renderResults()}
      </Host>
    );
  }
}
