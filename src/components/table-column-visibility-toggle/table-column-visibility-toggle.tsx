import { h, Component, Prop } from "@stencil/core";

@Component({
  tag: "gl-table-column-visibility-toggle",
})
export class TableColumnVisibilityToggle {
  /**
   * The column this visibility toggle works on.
   */
  @Prop() readonly column: HTMLGlTableColumnElement;

  /**
   * When the column is currently visible or invisible.
   */
  @Prop({ mutable: true }) enabled: boolean = true;

  private changeHandler(e: CustomEvent) {
    this.enabled = e.detail.checked;
    this.column.visible = this.enabled;
  }

  private toggleChanged = (e: CustomEvent) => this.changeHandler(e);

  render() {
    return (
      <ion-item>
        <ion-label>{this.column.label}</ion-label>
        <ion-toggle
          value="toggle"
          checked={this.enabled}
          onIonChange={this.toggleChanged}
        ></ion-toggle>
      </ion-item>
    );
  }
}
