# gl-table-column-visibility-toggle

The gl-table-column-visibility-toggle component is a toggle for a specific column of a [gl-table](../table/).
This component is used in the [gl-table-column-visibility](../table-column-visibility/) modal.

<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description                                        | Type                       | Default     |
| --------- | --------- | -------------------------------------------------- | -------------------------- | ----------- |
| `column`  | --        | The column this visibility toggle works on.        | `HTMLGlTableColumnElement` | `undefined` |
| `enabled` | `enabled` | When the column is currently visible or invisible. | `boolean`                  | `true`      |


## Dependencies

### Used by

 - [gl-table-column-visibility](../table-column-visibility)

### Depends on

- ion-item
- ion-label
- ion-toggle

### Graph
```mermaid
graph TD;
  gl-table-column-visibility-toggle --> ion-item
  gl-table-column-visibility-toggle --> ion-label
  gl-table-column-visibility-toggle --> ion-toggle
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-toggle --> ion-icon
  gl-table-column-visibility --> gl-table-column-visibility-toggle
  style gl-table-column-visibility-toggle fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
