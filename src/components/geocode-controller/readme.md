# gl-geocode-controller

The geocode controller component provides a standard interface for forward
and reverse geocoding. It supports pluggable client backends to perform the
geocoding and comes with a client for
[Nominatim](https://wiki.openstreetmap.org/wiki/Nominatim).

## Options
For details of the options available, see the [`ForwardGeocodeOptions` and 
`ReverseGeocodeOptions` interfaces](geocode-interface.ts).

<!-- Auto Generated Below -->


## Properties

| Property        | Attribute        | Description                                                                                                                | Type     | Default       |
| --------------- | ---------------- | -------------------------------------------------------------------------------------------------------------------------- | -------- | ------------- |
| `defaultClient` | `default-client` | Default client used to perform the geocoding. It can be overridden in the options provided to `forward()` and `reverse()`. | `string` | `"nominatim"` |


## Events

| Event              | Description                                       | Type               |
| ------------------ | ------------------------------------------------- | ------------------ |
| `glForwardGeocode` | Emitted when a forward geocode request completes. | `CustomEvent<any>` |
| `glReverseGeocode` | Emitted when a reverse geocode request completes. | `CustomEvent<any>` |


## Methods

### `addClient(id: string, service: geocodeClient) => Promise<void>`

Adds a client to the geocode controller

#### Returns

Type: `Promise<void>`



### `forward(options: ForwardGeocodeOptions, clientOptions?: geocodeClientOptions) => Promise<GeocodeResponse[]>`

Performs forward geocoding by converting an address to a geolocation.

#### Returns

Type: `Promise<GeocodeResponse[]>`



### `reverse(options: ReverseGeocodeOptions, clientOptions?: geocodeClientOptions) => Promise<any>`

Performs reverse geocoding by converting a geolocation to an address.

#### Returns

Type: `Promise<any>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
