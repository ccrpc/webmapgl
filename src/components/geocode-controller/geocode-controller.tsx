import { Component, Event, EventEmitter, Method, Prop } from "@stencil/core";
import {
  ForwardGeocodeOptions,
  ReverseGeocodeOptions,
  GeocodeResponse,
  geocodeClient,
  geocodeClientOptions,
} from "./geocode-interface";
import { NominatimClient } from "./nominatim";

@Component({
  tag: "gl-geocode-controller",
})
export class GeocodeController {
  private static clients: { [index: string]: geocodeClient } = {};

  /**
   * Default client used to perform the geocoding. It can be overridden in
   * the options provided to `forward()` and `reverse()`.
   */
  @Prop() readonly defaultClient: string = "nominatim";

  /**
   * Emitted when a forward geocode request completes.
   */
  @Event() glForwardGeocode: EventEmitter;

  /**
   * Emitted when a reverse geocode request completes.
   */
  @Event() glReverseGeocode: EventEmitter;

  /**
   * Performs forward geocoding by converting an address to a geolocation.
   * @param options Forward geocoding options object.
   * @param clientOptions Additional client options object.
   */
  @Method()
  async forward(
    options: ForwardGeocodeOptions,
    clientOptions?: geocodeClientOptions
  ): Promise<GeocodeResponse[]> {
    let client = this.getClient(options.client);
    let res = await client.forward(options, clientOptions);
    this.glForwardGeocode.emit({
      options: options,
      response: res,
    });
    return res;
  }

  /**
   * Performs reverse geocoding by converting a geolocation to an address.
   * @param options Reverse geocoding options object.
   * @param clientOptions Additional client options.
   */
  @Method()
  async reverse(
    options: ReverseGeocodeOptions,
    clientOptions?: geocodeClientOptions
  ) {
    let client = this.getClient(options.client);
    let res = await client.reverse(options, clientOptions);
    this.glReverseGeocode.emit({
      options: options,
      response: res,
    });
    return res;
  }

  private getClient(id?: string) {
    return GeocodeController.clients[id || this.defaultClient];
  }

  /**
   * Adds a client to the geocode controller
   * @param id The ide of the client
   * @param service A client able to handle nominatim results. For an example see {@link ./nominatim}
   */
  @Method()
  static async addClient(id: string, service: geocodeClient) {
    GeocodeController.clients[id] = service;
  }
}

GeocodeController.addClient("nominatim", new NominatimClient());
