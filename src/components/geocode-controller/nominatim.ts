import {
  ForwardGeocodeOptions,
  ReverseGeocodeOptions,
  GeocodeResponse,
} from "./geocode-interface";

/**
 * The start of a nominatim clients object interface
 * Based partially off of usage and partially off of
 * https://nominatim.org/release-docs/develop/api/Search/
 * https://nominatim.org/release-docs/develop/api/Reverse/
 *
 * More could definitely be added to it and if you (developer reading this in the future) need more options
 * feel free to add those to this interface.
 */
export interface NominatimClientOptions {
  addressdetails?: string;
  countrycodes?: string | string[];
  mode?: RequestMode;
  zoom?: string;
}

export class NominatimClient {
  async forward(
    options: ForwardGeocodeOptions,
    clientOptions: NominatimClientOptions = {}
  ): Promise<GeocodeResponse[]> {
    let params = {
      format: "jsonv2",
      addressdetails: clientOptions.addressdetails || "1",
    };

    if (clientOptions.countrycodes != undefined)
      params["countrycodes"] = clientOptions.countrycodes;

    if (typeof options.address === "string") {
      params["q"] = options.address;
    } else {
      Object.assign(params, options.address);
    }

    if (options.bbox != undefined) params["viewbox"] = options.bbox.join(",");
    if (options.bounded) params["bounded"] = "1";
    if (options.limit != undefined) params["limit"] = options.limit;

    let res = await fetch(this.addParams(options.url, params), {
      mode: clientOptions.mode || "cors",
    });
    let json = await res.json();

    if (json.error) return json;
    return this.formatResponse(json);
  }

  async reverse(
    options: ReverseGeocodeOptions,
    clientOptions: NominatimClientOptions = {}
  ) {
    let params = {
      format: "jsonv2",
      addressdetails: clientOptions.addressdetails || "1",
      zoom: clientOptions.zoom || "18",
    };

    params["lon"] = options.location.lon.toString();
    params["lat"] = options.location.lat.toString();

    let res = await fetch(this.addParams(options.url, params), {
      mode: clientOptions.mode || "cors",
    });
    let json = await res.json();

    if (json.error) return json;
    return this.formatResponseItem(json);
  }

  formatResponse(res: any): GeocodeResponse[] {
    return res.map((item: any) => this.formatResponseItem(item));
  }

  formatResponseItem(item: any): GeocodeResponse {
    let bbox = null;
    if (item.boundingbox) {
      let coords = item.boundingbox.map((coord) => parseFloat(coord));
      bbox = [coords[2], coords[0], coords[3], coords[1]];
    }
    let gr: GeocodeResponse = {
      address: {
        name: item.address[item.type] || item.address[item.category] || null,
        housenumber: item.address.house_number || null,
        street: item.address.road || null,
        city: item.address.city || null,
        county: item.address.county || null,
        state: item.address.state || null,
        country: item.address.country || null,
        countrycode: item.address.country_code || null,
        postalcode: item.address.postcode || null,
      },
      bbox: bbox,
      client: {
        osm_id: item.osm_id || null,
        osm_type: item.osm_type || null,
        category: item.category || null,
        type: item.type || null,
        importance: item.importance || null,
        place_id: item.place_id || null,
        place_rank: item.place_rank || null,
      },
      display: item.display_name || null,
      location: {
        lon: parseFloat(item.lon),
        lat: parseFloat(item.lat),
      },
      polygon: item.geojson || null,
    };
    return gr;
  }

  addParams(endpoint: string, params: any) {
    let url = new URL(endpoint);
    Object.keys(params).forEach((key) =>
      url.searchParams.append(key, params[key])
    );
    return url.toString();
  }
}
