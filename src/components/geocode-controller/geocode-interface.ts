import { NominatimClient, NominatimClientOptions } from "./nominatim";

export interface Address {
  name?: string;
  housenumber?: string;
  street?: string;
  city?: string;
  county?: string;
  state?: string;
  country?: string;
  countrycode?: string;
  postalcode?: string;
}

export interface Location {
  lon: number;
  lat: number;
}

export interface ForwardGeocodeOptions {
  address: string | Address;
  url: string;
  client?: string;
  bbox?: [number, number, number, number];
  jobId?: string;
  limit?: number;
  bounded?: boolean;
}

export interface ReverseGeocodeOptions {
  location: Location;
  url: string;
  client?: string;
  jobId?: string;
}

export interface GeocodeResponse {
  address: Address;
  bbox: [number, number, number, number];
  client?: any;
  display: string;
  location: Location;
  polygon: any;
}

/**
 * A list of geocode clients implemented.
 * Currently only the Nominatim Client has been created so the type looks a bit silly
 * but if in the future another client is implemented, let's call it MapLibreGeocodeClient, the type will look like
 *
 * export geocodeClient = NominatimClient | MapLibreGeocodeClient
 */
export type geocodeClient = NominatimClient;
export type geocodeClientOptions = NominatimClientOptions;
