import {
  h,
  Component,
  Element,
  Listen,
  Prop,
  State,
  Host,
} from "@stencil/core";
import { _t } from "../i18n/i18n";

@Component({
  styleUrl: "form-page.css",
  tag: "gl-form-page",
})
export class FormPage {
  @Element() el: HTMLGlFormPageElement;

  @State() canSubmit = false;

  /**
   * Text for the back button.
   */
  @Prop() readonly backText: string = _t("webmapgl.form-page.back");

  /**
   * Facets for the page.
   */
  @Prop() readonly facets: any[];

  /**
   * Fields for the page.
   */
  @Prop() readonly fields: any[];

  /**
   * Currently-selected facet. It is used the set the CSS class of the page.
   */
  @Prop() readonly formFacet: string;

  /**
   * Label text displayed in the form toolbar.
   */
  @Prop() readonly label: string;

  /**
   * This is the root page in the form. If it is `true`, the back button
   * is not displayed.
   */
  @Prop() readonly root: boolean = false;

  /**
   * Text for the submit button.
   */
  @Prop() readonly submitText: string = _t("webmapgl.form-page.submit");

  /**
   * Text for the cancel button.
   */
  @Prop() readonly cancelText: string = _t("webmapgl.form-page.cancel");

  componentDidLoad() {
    this.updateValidationStatus();
  }

  @Listen("glFormFeatureChanged", { target: "body" })
  async updateValidationStatus() {
    if (this.el.querySelectorAll("gl-field").length === 0) {
      this.canSubmit = false;
    } else {
      this.canSubmit = (await this.validate()).length === 0;
    }
  }

  private async validate() {
    let messages = await Promise.all(
      Array.from(this.el.querySelectorAll("gl-field")).map(
        async (field) => await field.validate()
      )
    );

    return messages.filter((message) => message != null);
  }

  private cancel() {
    const form = this.el.closest("gl-form");
    form.cancel();
  }

  private submit() {
    const form = this.el.closest("gl-form");
    form.submit();
  }

  private renderFacets() {
    return this.facets.map((facet) => (
      <gl-facet
        label={facet.label}
        value={facet.value}
        image={facet.image}
        widget={facet.widget}
      ></gl-facet>
    ));
  }

  private renderFields() {
    return this.fields.map((field) => (
      <gl-field
        attribute={field.attribute}
        label={field.label}
        image={field.image}
        required={field.required}
        type={field.type}
        widget={field.widget}
      >
        {this.renderOptions(field.options)}
      </gl-field>
    ));
  }

  private renderOptions(options: any[]) {
    if (options == undefined) return;
    return options.map((option) => (
      <gl-option
        image={option.image}
        label={option.label}
        value={option.value}
      ></gl-option>
    ));
  }

  private clickSubmit = () => {
    if (this.canSubmit) this.submit();
  };

  private clickCancel = () => this.cancel();

  render() {
    let small = screen.width <= 640;
    return (
      <Host class={`gl-form-page-facet-${this.formFacet || "none"}`}>
        <ion-header>
          <ion-toolbar color="primary">
            {!this.root ? (
              <ion-buttons slot="start">
                <ion-nav-pop>
                  <ion-button>
                    <ion-icon
                      slot={small ? "icon-only" : "start"}
                      name="arrow-back"
                    ></ion-icon>
                    {small ? null : this.backText}
                  </ion-button>
                </ion-nav-pop>
              </ion-buttons>
            ) : null}
            <ion-title>{this.label}</ion-title>
            <ion-buttons slot="end">
              <ion-button onClick={this.clickCancel}>
                <ion-icon
                  slot={small ? "icon-only" : "start"}
                  name="close"
                ></ion-icon>
                {small ? null : this.cancelText}
              </ion-button>
              <ion-button onClick={this.clickSubmit} disabled={!this.canSubmit}>
                <ion-icon
                  slot={small ? "icon-only" : "start"}
                  name="checkmark"
                ></ion-icon>
                {small ? null : this.submitText}
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <ion-list>
            {this.renderFacets()}
            {this.renderFields()}
          </ion-list>
        </ion-content>
      </Host>
    );
  }
}
