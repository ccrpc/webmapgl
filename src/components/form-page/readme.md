# gl-attributes-form

The form page component displays a subset of the facets and fields in a
[`gl-form`](../form) based on the currently-selected facet. It is created
by the form.

<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description                                                                           | Type      | Default                           |
| ------------ | ------------- | ------------------------------------------------------------------------------------- | --------- | --------------------------------- |
| `backText`   | `back-text`   | Text for the back button.                                                             | `string`  | `_t("webmapgl.form-page.back")`   |
| `cancelText` | `cancel-text` | Text for the cancel button.                                                           | `string`  | `_t("webmapgl.form-page.cancel")` |
| `facets`     | --            | Facets for the page.                                                                  | `any[]`   | `undefined`                       |
| `fields`     | --            | Fields for the page.                                                                  | `any[]`   | `undefined`                       |
| `formFacet`  | `form-facet`  | Currently-selected facet. It is used the set the CSS class of the page.               | `string`  | `undefined`                       |
| `label`      | `label`       | Label text displayed in the form toolbar.                                             | `string`  | `undefined`                       |
| `root`       | `root`        | This is the root page in the form. If it is `true`, the back button is not displayed. | `boolean` | `false`                           |
| `submitText` | `submit-text` | Text for the submit button.                                                           | `string`  | `_t("webmapgl.form-page.submit")` |


## Dependencies

### Used by

 - [gl-form](../form)

### Depends on

- [gl-facet](../facet)
- [gl-field](../field)
- [gl-option](../option)
- ion-header
- ion-toolbar
- ion-buttons
- ion-button
- ion-icon
- ion-title
- ion-content
- ion-list

### Graph
```mermaid
graph TD;
  gl-form-page --> gl-facet
  gl-form-page --> gl-field
  gl-form-page --> gl-option
  gl-form-page --> ion-header
  gl-form-page --> ion-toolbar
  gl-form-page --> ion-buttons
  gl-form-page --> ion-button
  gl-form-page --> ion-icon
  gl-form-page --> ion-title
  gl-form-page --> ion-content
  gl-form-page --> ion-list
  gl-facet --> ion-list-header
  gl-facet --> ion-item
  gl-facet --> ion-label
  gl-facet --> ion-thumbnail
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  gl-field --> ion-list-header
  gl-field --> ion-radio-group
  gl-field --> ion-item
  gl-field --> ion-label
  gl-field --> ion-select
  gl-field --> ion-textarea
  gl-field --> ion-input
  ion-select --> ion-select-popover
  ion-select --> ion-popover
  ion-select --> ion-action-sheet
  ion-select --> ion-alert
  ion-select --> ion-icon
  ion-select-popover --> ion-item
  ion-select-popover --> ion-checkbox
  ion-select-popover --> ion-label
  ion-select-popover --> ion-radio-group
  ion-select-popover --> ion-radio
  ion-select-popover --> ion-list
  ion-select-popover --> ion-list-header
  ion-popover --> ion-backdrop
  ion-action-sheet --> ion-backdrop
  ion-action-sheet --> ion-icon
  ion-action-sheet --> ion-ripple-effect
  ion-alert --> ion-ripple-effect
  ion-alert --> ion-backdrop
  ion-input --> ion-icon
  gl-option --> ion-item
  gl-option --> ion-radio
  gl-option --> ion-label
  gl-option --> ion-select-option
  gl-option --> ion-thumbnail
  ion-button --> ion-ripple-effect
  gl-form --> gl-form-page
  style gl-form-page fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
