import { h, Component, Element } from "@stencil/core";
import { popoverController } from "@ionic/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-download-button",
})
export class DownloadButton {
  @Element() el: HTMLGlDownloadButtonElement;

  private async openPopover(ev: UIEvent) {
    const options = {
      component: document.createElement("gl-download-list"),
      componentProps: {
        files: Array.from(this.el.querySelectorAll("gl-download-file")),
      },
      ev: ev,
    };
    const popover = await popoverController.create(options);
    await popover.present();
    return popover;
  }

  private clickPopover = (ev: UIEvent) => this.openPopover(ev);

  render() {
    let title = _t("webmapgl.download-button.title");
    return (
      <ion-button onClick={this.clickPopover} title={title}>
        <ion-icon slot="icon-only" name="download-sharp"></ion-icon>
      </ion-button>
    );
  }
}
