# gl-download-button

The download button component renders a button that opens a
[gl-download-list](../download-list) in a popover.

<!-- Auto Generated Below -->


## Dependencies

### Depends on

- [gl-download-list](../download-list)
- ion-button
- ion-icon

### Graph
```mermaid
graph TD;
  gl-download-button --> gl-download-list
  gl-download-button --> ion-button
  gl-download-button --> ion-icon
  gl-download-list --> ion-item
  gl-download-list --> ion-label
  gl-download-list --> ion-button
  gl-download-list --> ion-icon
  gl-download-list --> ion-content
  gl-download-list --> ion-list
  gl-download-list --> ion-list-header
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-button --> ion-ripple-effect
  style gl-download-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
