# gl-table-column-visibility-button

The table-column-visibility button component displays a button that opens a [gl-table-column-visibility](../table-column-visibility/) modal.

<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description                                                            | Type     | Default                                        |
| --------- | ---------- | ---------------------------------------------------------------------- | -------- | ---------------------------------------------- |
| `label`   | `label`    | A label for the button. In english it defaults to 'Column Visibility'. | `string` | `_t("webmapgl.table-column-visibility.label")` |
| `tableId` | `table-id` | An id for the table.                                                   | `string` | `undefined`                                    |


## Dependencies

### Depends on

- [gl-table-column-visibility](../table-column-visibility)
- ion-button

### Graph
```mermaid
graph TD;
  gl-table-column-visibility-button --> gl-table-column-visibility
  gl-table-column-visibility-button --> ion-button
  gl-table-column-visibility --> gl-table-column-visibility-toggle
  gl-table-column-visibility --> ion-header
  gl-table-column-visibility --> ion-toolbar
  gl-table-column-visibility --> ion-title
  gl-table-column-visibility --> ion-buttons
  gl-table-column-visibility --> ion-button
  gl-table-column-visibility --> ion-icon
  gl-table-column-visibility --> ion-content
  gl-table-column-visibility --> ion-list
  gl-table-column-visibility-toggle --> ion-item
  gl-table-column-visibility-toggle --> ion-label
  gl-table-column-visibility-toggle --> ion-toggle
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-toggle --> ion-icon
  ion-button --> ion-ripple-effect
  style gl-table-column-visibility-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
