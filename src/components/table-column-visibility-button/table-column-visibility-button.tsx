import { h, Component, Prop } from "@stencil/core";
import { modalController } from "@ionic/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-table-column-visibility-button",
})
export class TableColumnVisibilityButton {
  /**
   * A label for the button.
   * In english it defaults to 'Column Visibility'.
   */
  @Prop() readonly label: string = _t("webmapgl.table-column-visibility.label");

  /**
   * An id for the table.
   */
  @Prop() readonly tableId: string;

  private async openModal(ev: UIEvent) {
    const options = {
      component: document.createElement("gl-table-column-visibility"),
      componentOptions: {
        tableId: this.tableId,
      },
      event: ev,
    };
    const modal = await modalController.create(options);
    await modal.present();
    return modal;
  }

  private openClick = (e: UIEvent) => this.openModal(e);

  render() {
    return <ion-button onClick={this.openClick}>{this.label}</ion-button>;
  }
}
