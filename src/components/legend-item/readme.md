# gl-legend-item

The legend item component creates a legend entry. It should be used within a
[`gl-legend`](../legend). The legend item optionally can be used to toggle
on or off an associated layer or layers.

<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description                                                                                                                                       | Type                  | Default     |
| --------- | ---------- | ------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- | ----------- |
| `image`   | `image`    | The URL of the image for the legend item.                                                                                                         | `string`              | `undefined` |
| `layers`  | `layers`   | The IDs of the style layers controlled by this legend item. If controls which layers are toggled or on off by this legend item.                   | `string \| string[]`  | `undefined` |
| `styleId` | `style-id` | ID of the `gl-style` containing the layers referenced in `layers`.                                                                                | `string`              | `undefined` |
| `toggle`  | `toggle`   | Show a toggle switch to turn the corresponding layer on and off. If it is `true`, `layers` and `styleId` must be provided.                        | `boolean`             | `false`     |
| `widget`  | `widget`   | Widget used to render the legend item. The `divider` value renders an `ion-list-divider` component suitable for grouping subsequent legend items. | `"divider" \| "item"` | `"item"`    |


## Dependencies

### Depends on

- ion-thumbnail
- ion-label
- ion-toggle
- ion-item-divider
- ion-item

### Graph
```mermaid
graph TD;
  gl-legend-item --> ion-thumbnail
  gl-legend-item --> ion-label
  gl-legend-item --> ion-toggle
  gl-legend-item --> ion-item-divider
  gl-legend-item --> ion-item
  ion-toggle --> ion-icon
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  style gl-legend-item fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
