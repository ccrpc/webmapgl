import {
  h,
  Component,
  Element,
  Listen,
  Prop,
  State,
  Watch,
} from "@stencil/core";
import { toArray } from "../utils";
import { ToggleCustomEvent } from "@ionic/core";
import { StyleSpecification } from "maplibre-gl";

@Component({
  tag: "gl-legend-item",
  styleUrl: "legend-item.css",
})
export class LegendItem {
  @Element() el: HTMLGlLegendItemElement;

  @State() visible: boolean;

  /**
   * The IDs of the style layers controlled by this legend item. If controls
   * which layers are toggled or on off by this legend item.
   */
  @Prop() readonly layers: string | string[];

  /**
   * The URL of the image for the legend item.
   */
  @Prop() readonly image: string;

  /**
   * Show a toggle switch to turn the corresponding layer on and off. If
   * it is `true`, `layers` and `styleId` must be provided.
   */
  @Prop() readonly toggle: boolean = false;

  /**
   * ID of the `gl-style` containing the layers referenced in `layers`.
   */
  @Prop() readonly styleId: string;

  /**
   * Widget used to render the legend item. The `divider` value renders an
   * `ion-list-divider` component suitable for grouping subsequent legend items.
   */
  @Prop() readonly widget: "divider" | "item" = "item";

  async componentWillLoad() {
    this.handleStyleId();
  }

  @Watch("styleId")
  async handleStyleId() {
    let styleEl = await this.getStyle();
    if (styleEl != undefined) this.update(styleEl.json);
  }

  @Listen("glStyleElementAdded", { target: "body" })
  handleStyleAdded(e: CustomEvent) {
    if (e.detail.id === this.styleId) this.update(e.detail.style.json);
  }

  @Listen("glStyleElementModified", { target: "body" })
  handleStyleUpdated(e: CustomEvent) {
    if (e.detail.id === this.styleId) this.update(e.detail.style.json);
  }

  private async getStyle(): Promise<HTMLGlStyleElement> {
    let styleEl: HTMLGlStyleElement = document.querySelector(
      `gl-style#${this.styleId}`
    );

    if (styleEl != undefined) await styleEl.componentOnReady();
    return styleEl;
  }

  private getVisible(json: StyleSpecification) {
    const layers = toArray(this.layers);
    if (layers == undefined || layers.length === 0) return true;
    if (json.layers == undefined || json.layers.length === 0) return false;
    for (let layer of json.layers) {
      if (layers.indexOf(layer.id) !== -1) {
        if ("layout" in layer && layer.layout != undefined) {
          return layer.layout.visibility !== "none";
        } else return true;
      }
    }
  }

  private async setVisible(visible: boolean) {
    if (this.visible === visible) return;

    let layers = toArray(this.layers);
    let styleEl = await this.getStyle();
    if (styleEl == undefined) return;

    let json: StyleSpecification = { ...styleEl.json };
    json.layers = (json.layers || []).map((layer) => {
      if (layers.indexOf(layer.id) !== -1) {
        if (!("layout" in layer) || layer.layout == undefined)
          layer["layout"] = {};
        layer["layout"]["visibility"] = visible ? "visible" : "none";
      }
      return layer;
    });
    styleEl.json = json;
  }

  private update(json: StyleSpecification) {
    if (json != null) this.visible = this.getVisible(json);
  }

  private onToggle = (e: ToggleCustomEvent) =>
    this.setVisible(e.detail.checked);

  render() {
    let content = [];
    if (this.image != undefined)
      content.push(
        <ion-thumbnail slot="start">
          <img src={this.image} alt={this.el.textContent} />
        </ion-thumbnail>
      );
    content.push(
      <ion-label>
        <slot />
      </ion-label>
    );
    if (this.toggle)
      content.push(
        <ion-toggle
          slot="end"
          checked={this.visible}
          onIonChange={this.onToggle}
        ></ion-toggle>
      );

    if (this.widget === "divider")
      return <ion-item-divider>{content}</ion-item-divider>;
    return <ion-item>{content}</ion-item>;
  }
}
