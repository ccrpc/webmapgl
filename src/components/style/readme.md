# gl-style

The style component represents a Mapbox GL style. It should be a child of
[`gl-map`](../map). The map component merges all of the enabled `gl-style`
children to create the final map style. Layer and source IDs are prefixed
with the ID of the `gl-style` element and a colon in the merged style to
prevent conflicts.

The style JSON can be specified using either the `url` property or the `json`
property. For details, see the [Mapbox GL Style Specification
documentation](https://docs.mapbox.com/mapbox-gl-js/style-spec/).

<!-- Auto Generated Below -->


## Properties

| Property             | Attribute              | Description                                                                                                                        | Type                                                                                                                                                                                                                                                                                                                                                    | Default                                                                |
| -------------------- | ---------------------- | ---------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------- |
| `basemap`            | `basemap`              | This style represents a basemap and should be included in `gl-basemap-switcher`.                                                   | `boolean`                                                                                                                                                                                                                                                                                                                                               | `false`                                                                |
| `clickableLayers`    | `clickable-layers`     | Array of layers that should be clickable.                                                                                          | `string \| string[]`                                                                                                                                                                                                                                                                                                                                    | `[]`                                                                   |
| `enabled`            | `enabled`              | Include this style in the combined style for the parent `gl-map`.                                                                  | `boolean`                                                                                                                                                                                                                                                                                                                                               | `true`                                                                 |
| `insertBeneathLayer` | `insert-beneath-layer` | Name of the specific layer under which to insert this style's layers.                                                              | `string`                                                                                                                                                                                                                                                                                                                                                | `undefined`                                                            |
| `insertIntoStyle`    | `insert-into-style`    | ID of the style in which to insert this style's layers Note: The style you are inserting into must precede this style in the DOM.  | `string`                                                                                                                                                                                                                                                                                                                                                | `undefined`                                                            |
| `json`               | --                     | Style JSON object. If `url` is set, it is loaded from that URL.                                                                    | `{ version: 8; name?: string; metadata?: unknown; center?: number[]; zoom?: number; bearing?: number; pitch?: number; light?: LightSpecification; terrain?: TerrainSpecification; sources: { [_: string]: SourceSpecification; }; sprite?: SpriteSpecification; glyphs?: string; transition?: TransitionSpecification; layers: LayerSpecification[]; }` | `{     version: 8,     sources: undefined,     layers: undefined,   }` |
| `name`               | `name`                 | Human readable name. It is used by `gl-basemap-switcher`.                                                                          | `string`                                                                                                                                                                                                                                                                                                                                                | `undefined`                                                            |
| `thumbnail`          | `thumbnail`            | URL of the thumbnail image. It is used by `gl-basemap-switcher`.                                                                   | `string`                                                                                                                                                                                                                                                                                                                                                | `undefined`                                                            |
| `token`              | `token`                | Access token for sources GeoJSON sources. If set, it is used to replace the `${TOKEN}` placeholder in the style's `data` property. | `string`                                                                                                                                                                                                                                                                                                                                                | `undefined`                                                            |
| `url`                | `url`                  | URL of the style JSON.                                                                                                             | `string`                                                                                                                                                                                                                                                                                                                                                | `undefined`                                                            |


## Events

| Event                    | Description                                                                                                             | Type                                              |
| ------------------------ | ----------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------- |
| `glFeatureClick`         | Emitted when a feature is clicked. Only features in layers listed in `clickableLayers` trigger this event when clicked. | `CustomEvent<{ features: MapGeoJSONFeature[]; }>` |
| `glStyleElementAdded`    | Emitted when a style component is initialized.                                                                          | `CustomEvent<styleEvent>`                         |
| `glStyleElementModified` | Emitted when the style JSON changes.                                                                                    | `CustomEvent<styleEvent>`                         |
| `glStyleElementRemoved`  | Emitted when a style component unloads.                                                                                 | `CustomEvent<styleEvent>`                         |


## Dependencies

### Depends on

- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-style --> ion-thumbnail
  style gl-style fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
