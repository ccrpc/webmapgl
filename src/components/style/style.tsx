import {
  Component,
  Element,
  Event,
  EventEmitter,
  Prop,
  Watch,
} from "@stencil/core";
import { toArray } from "../utils";
import {
  GeoJSONSourceSpecification,
  MapMouseEvent,
  MapGeoJSONFeature,
  StyleSpecification,
} from "maplibre-gl";

let _nextId = 0;

export interface styleEvent {
  id: string;
  style: Style;
}

@Component({
  tag: "gl-style",
})
export class Style {
  private handlers = {
    click: (
      e: MapMouseEvent & {
        features?: MapGeoJSONFeature[];
      }
    ) => this.handleClick(e),
    mouseenter: () => this.handleMouseenter(),
    mouseleave: () => this.handleMouseleave(),
  };

  @Element() el: HTMLGlStyleElement;

  /**
   * Emitted when a feature is clicked. Only features in layers listed in
   * `clickableLayers` trigger this event when clicked.
   */
  @Event() glFeatureClick: EventEmitter<{
    features: MapGeoJSONFeature[];
  }>;

  /**
   * Emitted when a style component is initialized.
   */
  @Event() glStyleElementAdded: EventEmitter<styleEvent>;

  /**
   * Emitted when the style JSON changes.
   */
  @Event() glStyleElementModified: EventEmitter<styleEvent>;

  /**
   * Emitted when a style component unloads.
   */
  @Event() glStyleElementRemoved: EventEmitter<styleEvent>;

  /**
   * This style represents a basemap and should be included in
   * `gl-basemap-switcher`.
   */
  @Prop() readonly basemap: boolean = false;

  /**
   * Array of layers that should be clickable.
   */
  @Prop() readonly clickableLayers: string | string[] = [];

  /**
   * Include this style in the combined style for the parent `gl-map`.
   */
  @Prop() readonly enabled: boolean = true;

  /**
   * Human readable name. It is used by `gl-basemap-switcher`.
   */
  @Prop() readonly name: string;

  /**
   * URL of the thumbnail image. It is used by `gl-basemap-switcher`.
   */
  @Prop() readonly thumbnail: string;

  /**
   * Access token for sources GeoJSON sources. If set, it is used to replace
   * the `${TOKEN}` placeholder in the style's `data` property.
   */
  @Prop() readonly token: string;

  /**
   * URL of the style JSON.
   */
  @Prop() readonly url: string;

  /**
   * Style JSON object. If `url` is set, it is loaded from that URL.
   */
  @Prop({ mutable: true }) json: StyleSpecification = {
    version: 8,
    sources: undefined,
    layers: undefined,
  };

  /**
   * ID of the style in which to insert this style's layers
   * Note: The style you are inserting into must precede this style in the DOM.
   */
  @Prop() readonly insertIntoStyle?: string;

  /**
   * Name of the specific layer under which to insert this style's layers.
   */
  @Prop() readonly insertBeneathLayer?: string;

  componentWillLoad() {
    if (this.el.id == undefined) {
      this.el.id = "style-" + _nextId.toString();
      _nextId += 1;
    }
    if (this.url != undefined) this.fetchJSON();
  }

  componentDidLoad() {
    if (this.clickableLayers != undefined && this.clickableLayers.length !== 0)
      this.updateClickable(this.clickableLayers);
    this.glStyleElementAdded.emit(this.eventDetail());
  }

  componentDidUpdate() {
    this.glStyleElementModified.emit(this.eventDetail());
  }

  disconnectedCallback() {
    this.glStyleElementRemoved.emit(this.eventDetail());
  }

  private eventDetail(): styleEvent {
    return {
      id: this.el.id,
      style: this,
    };
  }

  private getMap() {
    return this.el.closest("gl-map").map;
  }

  private async fetchJSON() {
    let res = await fetch(this.url);
    this.json = (await res.json()) as StyleSpecification;
    if (this.token && this.json.sources != undefined) {
      for (let srcName in this.json.sources) {
        let src = this.json.sources[srcName] as GeoJSONSourceSpecification;
        if (src.data != undefined)
          src.data = (src.data as string).replace(/\$\{TOKEN\}/g, this.token);
      }
    }
  }

  @Watch("clickableLayers")
  updateClickable(newLayers: string[] | string, oldLayers?: string[] | string) {
    const map = this.getMap();
    newLayers = toArray(newLayers);
    oldLayers = toArray(oldLayers);

    for (let layer of newLayers) {
      const layerIdx = oldLayers.indexOf(layer);
      if (layerIdx === -1) {
        let layerName = `${this.el.id}:${layer}`;
        map.on("click", layerName, this.handlers.click);
        map.on("mouseenter", layerName, this.handlers.mouseenter);
        map.on("mouseleave", layerName, this.handlers.mouseleave);
      } else {
        oldLayers.splice(layerIdx, 1);
      }
    }

    for (let layer of oldLayers) {
      let layerName = `${this.el.id}:${layer}`;
      map.off("click", layerName, this.handlers.click);
      map.off("mouseenter", layerName, this.handlers.mouseenter);
      map.off("mouseleave", layerName, this.handlers.mouseleave);
    }
  }

  private handleClick(e: MapMouseEvent & { features?: MapGeoJSONFeature[] }) {
    this.glFeatureClick.emit({ features: e.features });
  }

  private handleMouseenter() {
    this.getMap().getCanvas().style.cursor = "pointer";
  }

  private handleMouseleave() {
    this.getMap().getCanvas().style.cursor = "";
  }
}
