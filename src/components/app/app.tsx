import { h, Component, Element, Prop } from "@stencil/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-app",
  styleUrl: "app.css",
})
export class App {
  @Element() el: HTMLGlAppElement;

  /**
   * Application name in the main title bar.
   */
  @Prop() readonly label: string;

  /**
   * Menu type. A value of `false` disables the menu, while a `custom` menu
   * type creates a menu slot.
   */
  @Prop() readonly menu: "custom" | boolean = true;

  /**
   * Heading text for the menu.
   */
  @Prop() readonly menuLabel: string = _t("webmapgl.app.menulabel");

  /**
   * Should the application use `ion-split-pane` to display the menu?
   */
  @Prop() readonly splitPane: boolean = true;

  /**
   * What type of side menu to use, valid options are `push`, `overlay`, and `reveal`.
   */
  @Prop() readonly menuType: "push" | "overlay" | "reveal" = "push";

  componentDidLoad() {
    this.el.querySelector("gl-map").resizeMap();
  }

  private getMenu() {
    if (this.menu === true) {
      return (
        <ion-menu content-id="main" type={this.menuType}>
          <ion-header>
            <ion-toolbar>
              <ion-title>{this.menuLabel}</ion-title>
            </ion-toolbar>
          </ion-header>
          <ion-content>
            <div class="gl-menu-content">
              <slot name="menu" />
            </div>
          </ion-content>
        </ion-menu>
      );
    } else if (this.menu === "custom") {
      return <slot name="menu" />;
    }
  }

  private getMenuButton() {
    if (this.menu === true)
      return (
        <ion-menu-toggle>
          <ion-button>
            <ion-icon slot="icon-only" name="menu"></ion-icon>
          </ion-button>
        </ion-menu-toggle>
      );
  }

  private getMain() {
    return (
      <div class="pane-main" id="main">
        <ion-header>
          <ion-toolbar>
            <ion-buttons slot="start">
              {this.getMenuButton()}
              <slot name="start-buttons" />
            </ion-buttons>
            <ion-buttons slot="end">
              <slot name="end-buttons" />
            </ion-buttons>
            <ion-title>{this.label}</ion-title>
          </ion-toolbar>
        </ion-header>
        <ion-content scrollX={false} scrollY={false} class="map-content">
          <div class="fixed-content" slot="fixed">
            <slot />
          </div>
        </ion-content>
        <slot name="after-content" />
        <ion-footer>
          <slot name="footer" />
        </ion-footer>
      </div>
    );
  }

  render() {
    let content = [this.getMenu(), this.getMain()];
    if (this.splitPane)
      content = [<ion-split-pane content-id="main">{content}</ion-split-pane>];
    return <ion-app>{content}</ion-app>;
  }
}
