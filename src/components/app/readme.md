# gl-app

The app component provides a basic web map application layout using `ion-app`.
It includes a map slot and a configurable menu.

<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                                                                                      | Type                              | Default                        |
| ----------- | ------------ | ------------------------------------------------------------------------------------------------ | --------------------------------- | ------------------------------ |
| `label`     | `label`      | Application name in the main title bar.                                                          | `string`                          | `undefined`                    |
| `menu`      | `menu`       | Menu type. A value of `false` disables the menu, while a `custom` menu type creates a menu slot. | `"custom" \| boolean`             | `true`                         |
| `menuLabel` | `menu-label` | Heading text for the menu.                                                                       | `string`                          | `_t("webmapgl.app.menulabel")` |
| `menuType`  | `menu-type`  | What type of side menu to use, valid options are `push`, `overlay`, and `reveal`.                | `"overlay" \| "push" \| "reveal"` | `"push"`                       |
| `splitPane` | `split-pane` | Should the application use `ion-split-pane` to display the menu?                                 | `boolean`                         | `true`                         |


## Dependencies

### Depends on

- ion-menu
- ion-header
- ion-toolbar
- ion-title
- ion-content
- ion-menu-toggle
- ion-button
- ion-icon
- ion-buttons
- ion-footer
- ion-split-pane
- ion-app

### Graph
```mermaid
graph TD;
  gl-app --> ion-menu
  gl-app --> ion-header
  gl-app --> ion-toolbar
  gl-app --> ion-title
  gl-app --> ion-content
  gl-app --> ion-menu-toggle
  gl-app --> ion-button
  gl-app --> ion-icon
  gl-app --> ion-buttons
  gl-app --> ion-footer
  gl-app --> ion-split-pane
  gl-app --> ion-app
  ion-menu --> ion-backdrop
  ion-button --> ion-ripple-effect
  style gl-app fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
