import {
  h,
  Component,
  Element,
  Event,
  EventEmitter,
  Prop,
  State,
  Watch,
} from "@stencil/core";
import { LikeProxy } from "../like-controller/like-proxy";
import { findOrCreateOnReady } from "../utils";
import { Feature } from "geojson";
import { MapGeoJSONFeature } from "maplibre-gl";
@Component({
  tag: "gl-like-button",
})
export class LikeButton {
  @Element() el: HTMLGlLikeButtonElement;

  @State() count: number = 0;
  @State() liked: boolean = false;
  @State() proxy: LikeProxy;

  @State() likeCtrl: HTMLGlLikeControllerElement;
  @State() restCtrl: HTMLGlRestControllerElement;

  /**
   * The attribute which stores the number of likes this feature has received
   */
  @Prop() readonly attribute: string = "_likes";

  /**
   * Whether or not this like button is disabled
   */
  @Prop() readonly disabled: boolean = false;

  /**
   * The feature this like button is for
   */
  @Prop() readonly feature: Feature | MapGeoJSONFeature;

  /**
   * The icon for the like button if the user has not liked the feature
   */
  @Prop() readonly iconNo: string = "star-outline";

  /**
   * The icon for the like button if the user
   */
  @Prop() readonly iconYes: string = "star";

  /**
   * The HTTP method this like button uses to communicate a like
   */
  @Prop() readonly method: "GET" | "POST" | "PATCH" | "PUT" | "DELETE" = "POST";

  /**
   * The cors mode this like button uses to communicate
   */
  @Prop() readonly requestMode: RequestMode;

  /**
   * The authentication token this like button uses when communicating
   */
  @Prop() readonly token: string;

  /**
   * The url to the endpoint which which the like button communicates.
   * Most likely this will be the end point of a features-api instance.
   */
  @Prop() readonly url: string;

  /**
   * An event emitted when the user presses the like button
   */
  @Event() glLike: EventEmitter<{
    success: boolean;
    feature: Feature | MapGeoJSONFeature;
    action: "like" | "unlike";
    client_id: string;
  }>;

  async componentWillLoad() {
    await this.setState();
  }

  @Watch("feature")
  async setState() {
    if (this.feature != undefined) {
      this.count = this.feature.properties[this.attribute];
      if (this.likeCtrl == undefined) {
        this.likeCtrl = await findOrCreateOnReady("gl-like-controller");
      }
      this.proxy = await this.likeCtrl.create(this.feature);
      this.liked = this.proxy.isLiked();
    }
  }

  private async toggle() {
    this.liked = !this.liked;
    this.count += this.liked ? 1 : -1;
    if (this.feature?.properties != undefined)
      this.feature.properties[this.attribute] = this.count;
    this.liked ? this.proxy.like() : this.proxy.unlike();

    // Fail silently if request fails.
    let success = false;
    let clientId = this.proxy.getClientId();
    try {
      if (this.restCtrl == undefined) {
        this.restCtrl = await findOrCreateOnReady("gl-rest-controller");
      }
      await this.restCtrl.create(
        {
          type: "Feature",
          properties: {
            action: this.liked ? "like" : "unlike",
            feature_id: this.feature.id,
            client_id: clientId,
          },
        },
        {
          url: this.url,
          token: this.token,
          method: this.method,
          mode: this.requestMode,
        }
      );
      success = true;
    } catch (e) {}

    this.glLike.emit({
      success: success,
      feature: this.feature,
      action: this.liked ? "like" : "unlike",
      client_id: clientId,
    });
  }

  private likeClicked = () => {
    if (!this.disabled) this.toggle();
  };

  render() {
    return (
      <ion-button
        fill="clear"
        onClick={this.likeClicked}
        disabled={this.disabled}
      >
        <ion-icon
          slot="start"
          name={this.liked ? this.iconYes : this.iconNo}
        ></ion-icon>
        <span class="gl-like-button-count">{this.count}</span>
      </ion-button>
    );
  }
}
