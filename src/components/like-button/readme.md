# gl-like-button



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                                                                                          | Type                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Default          |
| ------------- | -------------- | ------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| `attribute`   | `attribute`    | The attribute which stores the number of likes this feature has received                                                             | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | `"_likes"`       |
| `disabled`    | `disabled`     | Whether or not this like button is disabled                                                                                          | `boolean`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | `false`          |
| `feature`     | --             | The feature this like button is for                                                                                                  | `Feature<Geometry, { [name: string]: any; }> \| GeoJSONFeature & { layer: (Omit<FillLayerSpecification, "source"> \| Omit<LineLayerSpecification, "source"> \| Omit<SymbolLayerSpecification, "source"> \| Omit<CircleLayerSpecification, "source"> \| Omit<HeatmapLayerSpecification, "source"> \| Omit<FillExtrusionLayerSpecification, "source"> \| Omit<RasterLayerSpecification, "source"> \| Omit<HillshadeLayerSpecification, "source"> \| Omit<BackgroundLayerSpecification, "source">) & { source: string; }; source: string; sourceLayer?: string; state: { [key: string]: any; }; }` | `undefined`      |
| `iconNo`      | `icon-no`      | The icon for the like button if the user has not liked the feature                                                                   | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | `"star-outline"` |
| `iconYes`     | `icon-yes`     | The icon for the like button if the user                                                                                             | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | `"star"`         |
| `method`      | `method`       | The HTTP method this like button uses to communicate a like                                                                          | `"DELETE" \| "GET" \| "PATCH" \| "POST" \| "PUT"`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | `"POST"`         |
| `requestMode` | `request-mode` | The cors mode this like button uses to communicate                                                                                   | `"cors" \| "navigate" \| "no-cors" \| "same-origin"`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | `undefined`      |
| `token`       | `token`        | The authentication token this like button uses when communicating                                                                    | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | `undefined`      |
| `url`         | `url`          | The url to the endpoint which which the like button communicates. Most likely this will be the end point of a features-api instance. | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | `undefined`      |


## Events

| Event    | Description                                            | Type                                                                                                                                                           |
| -------- | ------------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `glLike` | An event emitted when the user presses the like button | `CustomEvent<{ success: boolean; feature: Feature<Geometry, { [name: string]: any; }> \| MapGeoJSONFeature; action: "like" \| "unlike"; client_id: string; }>` |


## Dependencies

### Depends on

- ion-button
- ion-icon
- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-like-button --> ion-button
  gl-like-button --> ion-icon
  gl-like-button --> ion-thumbnail
  ion-button --> ion-ripple-effect
  style gl-like-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
