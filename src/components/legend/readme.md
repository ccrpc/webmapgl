# gl-legend

The legend component renders an `ion-list` containing
[`gl-legend-item`](../legend-item) components.

<!-- Auto Generated Below -->


## Dependencies

### Depends on

- ion-item-group

### Graph
```mermaid
graph TD;
  gl-legend --> ion-item-group
  style gl-legend fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
