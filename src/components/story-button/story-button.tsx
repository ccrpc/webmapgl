import { h, Component, Listen, Prop, State, Watch } from "@stencil/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-story-button",
})
export class StoryButton {
  @State() story: HTMLGlStoryElement;
  @State() disabled: boolean = false;

  /**
   * Where in the story this button will take you
   */
  @Prop() readonly action: "first" | "last" | "next" | "previous" | "close" =
    "next";

  /**
   * The icon for the button
   */
  @Prop() readonly icon: string;

  /**
   * An alternative name for this button
   */
  @Prop() readonly label: string;

  /**
   * The id of the story which this button acts upon
   */
  @Prop() readonly storyId: string;

  async componentWillLoad() {
    await this.updateStory();
  }

  @Watch("storyId")
  async updateStory() {
    this.story = document.querySelector(
      this.storyId != undefined ? `gl-story#${this.storyId}` : "gl-story"
    );
    await this.story.componentOnReady();
    this.updateDisabled();
  }

  @Listen("glStoryChange", { target: "body" })
  updateDisabled() {
    const story = this.story;
    if (story == undefined) return;

    this.disabled = {
      first: story.step === 0,
      last: story.step === story.steps - 1,
      next: story.step == undefined || story.step === story.steps - 1,
      previous: story.step == undefined || story.step === 0,
      exit: false,
    }[this.action];
  }

  private doAction() {
    const story = this.story;
    if (story == undefined) return;

    story.step = {
      first: 0,
      last: story.steps - 1,
      next: story.step + 1,
      previous: story.step - 1,
      exit: undefined,
    }[this.action];
  }

  private clickAction = () => this.doAction();

  render() {
    let title = _t(`webmapgl.story-button.${this.action}`);
    let icon =
      this.icon ||
      {
        first: "play-skip-back-outline",
        last: "play-skip-forward-outline",
        next: "play-forward-outline",
        previous: "play-back-outline",
        exit: "close-outline",
      }[this.action];

    return (
      <ion-button
        onClick={this.clickAction}
        title={title}
        disabled={this.disabled}
      >
        <ion-icon slot="icon-only" name={icon}></ion-icon>
      </ion-button>
    );
  }
}
