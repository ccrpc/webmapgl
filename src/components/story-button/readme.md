# gl-story-button



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description                                     | Type                                                   | Default     |
| --------- | ---------- | ----------------------------------------------- | ------------------------------------------------------ | ----------- |
| `action`  | `action`   | Where in the story this button will take you    | `"close" \| "first" \| "last" \| "next" \| "previous"` | `"next"`    |
| `icon`    | `icon`     | The icon for the button                         | `string`                                               | `undefined` |
| `label`   | `label`    | An alternative name for this button             | `string`                                               | `undefined` |
| `storyId` | `story-id` | The id of the story which this button acts upon | `string`                                               | `undefined` |


## Dependencies

### Depends on

- ion-button
- ion-icon

### Graph
```mermaid
graph TD;
  gl-story-button --> ion-button
  gl-story-button --> ion-icon
  ion-button --> ion-ripple-effect
  style gl-story-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
