# gl-attribute-values

The attribute values component displays the attributes of a GeoJSON feature or
features using an `ion-list`. It is often used in conjunction with 
[gl-popup](../popup).

The list of attributes to be displayed can be specified using
`attributeOptions`. Otherwise, the component displays all attributes using
their property names.

## Attribute Options

Each options object in the `attributeOptions` array can contain the following
keys:

* `alias`: Human-readable name of the attribute
* `name`: Property name of the attribute

<!-- Auto Generated Below -->


## Properties

| Property           | Attribute           | Description                                                                                                                   | Type                                 | Default                                  |
| ------------------ | ------------------- | ----------------------------------------------------------------------------------------------------------------------------- | ------------------------------------ | ---------------------------------------- |
| `attributeOptions` | `attribute-options` | An array of attribute configuration objects, or a stringified version of the array. See the section above for option details. | `attributeOptions[] \| string`       | `undefined`                              |
| `features`         | --                  | An array of GeoJSON features for which to display attributes.                                                                 | `MapGeoJSONFeature[]`                | `undefined`                              |
| `header`           | `header`            | Header text for the attributes list. It is ignored if `headerAttribute` is specified.                                         | `string`                             | `_t("webmapgl.attribute-values.header")` |
| `headerAttribute`  | `header-attribute`  | The name of an attribute that should be displayed in the list header.                                                         | `string`                             | `undefined`                              |
| `labelPosition`    | `label-position`    | Position property for `ion-label` elements.                                                                                   | `"fixed" \| "floating" \| "stacked"` | `undefined`                              |
| `limit`            | `limit`             | Maximum number of features to display.                                                                                        | `number`                             | `1`                                      |
| `valueMaxWidth`    | `value-max-width`   | Maximum width of the `ion-note` element used to display attribute values.                                                     | `string`                             | `"200px"`                                |


## Dependencies

### Depends on

- ion-item
- ion-label
- ion-note
- ion-item-divider
- ion-list

### Graph
```mermaid
graph TD;
  gl-attribute-values --> ion-item
  gl-attribute-values --> ion-label
  gl-attribute-values --> ion-note
  gl-attribute-values --> ion-item-divider
  gl-attribute-values --> ion-list
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  style gl-attribute-values fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
