import { h, Component, Element, Prop, Watch } from "@stencil/core";
import { _t } from "../i18n/i18n";
import { MapGeoJSONFeature } from "maplibre-gl";

export interface attributeOptions {
  /**
   * A display name for this attribute.
   */
  alias?: string;
  /**
   * The name of the attribute as it appears in the feature object.
   */
  name: string;
  /**
   * The display format of this attribute.
   *
   * Valid text options are "decimal", "currency", or "percent".
   *
   * Alternatively an options object for the toLocaleString function may also be passed.
   */
  format?: string | any;
}

@Component({
  styleUrl: "attribute-values.css",
  tag: "gl-attribute-values",
})
export class AttributeValues {
  private options: attributeOptions[] = [];

  @Element() el: HTMLGlAttributeValuesElement;

  /**
   * An array of attribute configuration objects, or a stringified version
   * of the array. See the section above for option details.
   */
  @Prop() readonly attributeOptions: string | attributeOptions[];

  /**
   * An array of GeoJSON features for which to display attributes.
   */
  @Prop() readonly features: MapGeoJSONFeature[];

  /**
   * Header text for the attributes list. It is ignored if `headerAttribute`
   * is specified.
   */
  @Prop() readonly header: string = _t("webmapgl.attribute-values.header");

  /**
   * The name of an attribute that should be displayed in the list header.
   */
  @Prop() readonly headerAttribute: string;

  /**
   * Position property for `ion-label` elements.
   */
  @Prop() readonly labelPosition: "fixed" | "floating" | "stacked";

  /**
   * Maximum number of features to display.
   */
  @Prop() readonly limit: number = 1;

  /**
   * Maximum width of the `ion-note` element used to display attribute values.
   */
  @Prop() readonly valueMaxWidth: string = "200px";

  componentWillLoad() {
    this.parseAttributeOptions();
  }

  /**
   * Converts a format attribute option from a string to the json object we can pass into toLocaleString.
   * If the format is one of the three possible string values, create a specific options object for that.
   */
  private parseFormat(format: string | any): object {
    if (typeof format !== "string") return format;

    let parsedFormat: any;
    try {
      parsedFormat = JSON.parse(format);
    } catch (e) {
      if (/SyntaxError/.test(e)) {
        if (["currency", "decimal", "percent"].indexOf(format) !== -1) {
          parsedFormat = {
            style: format,
          };
          if (format === "currency") parsedFormat["currency"] = "USD";
        }
      } else {
        throw e;
      }
    }
    return parsedFormat;
  }

  @Watch("attributeOptions")
  /**
   * If the attribute options are given to us as a string, attempt to parse them into an object.
   */
  private parseAttributeOptions() {
    this.options =
      typeof this.attributeOptions === "string"
        ? (JSON.parse(this.attributeOptions) as attributeOptions[])
        : this.attributeOptions;
  }

  private getItemsForFeature(
    feature: MapGeoJSONFeature,
    options: attributeOptions[]
  ) {
    return options
      .filter(
        (attr) =>
          attr.name !== this.headerAttribute &&
          feature.properties[attr.name] != undefined
      )
      .map((attr) => (
        <ion-item>
          <ion-label position={this.labelPosition}>
            {attr.alias || attr.name}
          </ion-label>
          <ion-note
            slot={this.labelPosition != "stacked" ? "end" : null}
            text-wrap
            style={{ "max-width": this.valueMaxWidth }}
          >
            {attr.format
              ? feature.properties[attr.name].toLocaleString(
                  {},
                  this.parseFormat(attr.format)
                )
              : feature.properties[attr.name]}
          </ion-note>
        </ion-item>
      ));
  }

  private generateOptions(feature: MapGeoJSONFeature): attributeOptions[] {
    let opts: attributeOptions[] = [];
    for (let prop in feature.properties) {
      opts.push({
        name: prop,
      });
    }
    return opts;
  }

  render() {
    if (this.features == undefined || this.features.length === 0) return null;

    let items = [];
    for (let feature of this.features.slice(0, this.limit)) {
      let header =
        this.headerAttribute != undefined
          ? feature.properties[this.headerAttribute]
          : this.header;
      items.push(
        <ion-item-divider>
          <ion-label>{header}</ion-label>
        </ion-item-divider>
      );

      let featureOpts = this.options || this.generateOptions(feature);
      Array.prototype.push.apply(
        items,
        this.getItemsForFeature(feature, featureOpts)
      );
    }

    return <ion-list>{items}</ion-list>;
  }
}
