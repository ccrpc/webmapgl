import { h, Component, Element, Prop } from "@stencil/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-share",
})
export class Share {
  @Element() el: HTMLGlShareElement;

  /**
   * Height of the popup window in pixels.
   */
  @Prop() readonly popupHeight: number = 480;

  /**
   * Width of the popup window in pixels.
   */
  @Prop() readonly popupWidth: number = 640;

  /**
   * Title of the item being shared.
   */
  @Prop() readonly shareTitle: string;

  /**
   * User attribution. It is used for sharing on Twitter.
   */
  @Prop() readonly shareUser: string;

  /**
   * Hashtags for the post. It is used for sharing on Twitter.
   */
  @Prop() readonly shareHashtags: string;

  /**
   * URL to share. If a URL is not provided, the current page URL is used.
   */
  @Prop() readonly shareUrl: string;

  private constructShareUrl(base: string, params: any) {
    let url = base;
    let qs = Object.keys(params || {})
      .filter((key) => params[key] != undefined)
      .map((key) => `${key}=${encodeURIComponent(params[key])}`)
      .join("&");

    if (qs != null) url += `?${qs}`;
    return url;
  }

  private share(platform: string) {
    let targetUrl = this.shareUrl || window.location.href;

    let title = this.shareTitle;
    if (title == undefined) {
      let app = this.el.closest("gl-app");
      if (app != undefined) title = app.label;
    }

    let url: string;
    if (platform === "twitter") {
      url = this.constructShareUrl("https://twitter.com/intent/tweet", {
        url: targetUrl,
        title: title,
        via: this.shareUser,
        hashtags: this.shareHashtags,
      });
    } else {
      url = this.constructShareUrl("https://www.facebook.com/sharer.php", {
        u: targetUrl,
      });
    }

    this.openUrl(url);
    this.closePopover();
  }

  private openUrl(url: string) {
    window.open(
      url,
      "gl-share",
      `width=${this.popupWidth},height=${this.popupHeight},` +
        `resizable,scrollbars=yes`
    );
  }

  private closePopover() {
    let popover = this.el.closest("ion-popover");
    if (popover != undefined) popover.dismiss();
  }

  private clickShare(name: string) {
    return () => this.share(name.toLocaleLowerCase());
  }

  render() {
    let items = ["Facebook", "Twitter"].map((name) => (
      <ion-item button onClick={this.clickShare(name)}>
        <ion-icon slot="start" name={`logo-${name.toLowerCase()}`}></ion-icon>
        <ion-label>{name}</ion-label>
      </ion-item>
    ));
    return (
      <ion-list>
        <ion-list-header>{_t("webmapgl.share.title")}</ion-list-header>
        {items}
      </ion-list>
    );
  }
}
