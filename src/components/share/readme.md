# gl-share

The share component allows the user to choose a social media platform
and share the application URL or another URL. It currently supports
Facebook and Twitter. Once the user selects a platform, the component launches
a popup window containing the pre-populated share form.

<!-- Auto Generated Below -->


## Properties

| Property        | Attribute        | Description                                                           | Type     | Default     |
| --------------- | ---------------- | --------------------------------------------------------------------- | -------- | ----------- |
| `popupHeight`   | `popup-height`   | Height of the popup window in pixels.                                 | `number` | `480`       |
| `popupWidth`    | `popup-width`    | Width of the popup window in pixels.                                  | `number` | `640`       |
| `shareHashtags` | `share-hashtags` | Hashtags for the post. It is used for sharing on Twitter.             | `string` | `undefined` |
| `shareTitle`    | `share-title`    | Title of the item being shared.                                       | `string` | `undefined` |
| `shareUrl`      | `share-url`      | URL to share. If a URL is not provided, the current page URL is used. | `string` | `undefined` |
| `shareUser`     | `share-user`     | User attribution. It is used for sharing on Twitter.                  | `string` | `undefined` |


## Dependencies

### Used by

 - [gl-share-button](../share-button)

### Depends on

- ion-item
- ion-icon
- ion-label
- ion-list
- ion-list-header

### Graph
```mermaid
graph TD;
  gl-share --> ion-item
  gl-share --> ion-icon
  gl-share --> ion-label
  gl-share --> ion-list
  gl-share --> ion-list-header
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  gl-share-button --> gl-share
  style gl-share fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
