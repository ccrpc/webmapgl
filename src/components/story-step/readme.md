# gl-story-step



<!-- Auto Generated Below -->


## Properties

| Property          | Attribute           | Description                                                                                                                                                                                                                                                    | Type      | Default     |
| ----------------- | ------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ----------- |
| `active`          | `active`            | Whether this step in the story is active                                                                                                                                                                                                                       | `boolean` | `false`     |
| `clickAfterIdle`  | `click-after-idle`  | Whether or not to click after the map has moved                                                                                                                                                                                                                | `boolean` | `true`      |
| `clickBeforeHash` | `click-before-hash` | Whether or not to try to locate the click feature before the map moves                                                                                                                                                                                         | `boolean` | `false`     |
| `clickDelay`      | `click-delay`       | Number of milliseconds to wait before clicking the map                                                                                                                                                                                                         | `number`  | `0`         |
| `clickLayer`      | `click-layer`       | What layer the click should be fired on                                                                                                                                                                                                                        | `string`  | `undefined` |
| `clickProperty`   | `click-property`    | The attribute name of the feature you are clicking on In essence you will be clicking on the features whose [clickProperty] = clickValue                                                                                                                       | `string`  | `undefined` |
| `clickValue`      | `click-value`       | The value the feature will have in the clickProperty attribute. In essence you will be clicking on the features whose [clickProperty] = clickValue  Alternatively if clickProperty is not set, clickValue refers to the featureID of what you are clicking on. | `number`  | `undefined` |
| `first`           | `first`             | Whether this is the first step in the story                                                                                                                                                                                                                    | `boolean` | `false`     |
| `hash`            | `hash`              | The 'hash' that represents the location of the story Visible in the browser url when the 'hash' property of {@link ../map} is set to true.                                                                                                                     | `string`  | `undefined` |
| `last`            | `last`              | Whether this is the last step in the story                                                                                                                                                                                                                     | `boolean` | `false`     |
| `mapId`           | `map-id`            | The ID of the map this story step works with                                                                                                                                                                                                                   | `string`  | `undefined` |


## Events

| Event            | Description                                                            | Type                                              |
| ---------------- | ---------------------------------------------------------------------- | ------------------------------------------------- |
| `glFeatureClick` | An event emitter that, when fired, emulates a user clicking on the map | `CustomEvent<{ features: MapGeoJSONFeature[]; }>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
