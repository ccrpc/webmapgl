import {
  h,
  Component,
  Element,
  Event,
  EventEmitter,
  Prop,
  Watch,
  Host,
} from "@stencil/core";
import { MapGeoJSONFeature, ExpressionSpecification } from "maplibre-gl";

@Component({
  tag: "gl-story-step",
})
export class StoryStep {
  @Element() el: HTMLGlStoryStepElement;

  /**
   * An event emitter that, when fired, emulates a user clicking on the map
   */
  @Event() glFeatureClick: EventEmitter<{
    features: MapGeoJSONFeature[];
  }>;

  /**
   * Whether this step in the story is active
   */
  @Prop() readonly active: boolean = false;

  /**
   * Whether this is the first step in the story
   */
  @Prop() readonly first: boolean = false;

  /**
   * Whether this is the last step in the story
   */
  @Prop() readonly last: boolean = false;

  /**
   * The 'hash' that represents the location of the story
   * Visible in the browser url when the 'hash' property of {@link ../map} is set to true.
   */
  @Prop() readonly hash: string;

  /**
   * Whether or not to try to locate the click feature before the map moves
   */
  @Prop() readonly clickBeforeHash: boolean = false;

  /**
   * Whether or not to click after the map has moved
   */
  @Prop() readonly clickAfterIdle: boolean = true;

  /**
   * Number of milliseconds to wait before clicking the map
   */
  @Prop() readonly clickDelay: number = 0;

  /**
   * What layer the click should be fired on
   */
  @Prop() readonly clickLayer: string;

  /**
   * The attribute name of the feature you are clicking on
   * In essence you will be clicking on the features whose [clickProperty] = clickValue
   */
  @Prop() readonly clickProperty: string;

  /**
   * The value the feature will have in the clickProperty attribute.
   * In essence you will be clicking on the features whose [clickProperty] = clickValue
   *
   * Alternatively if clickProperty is not set, clickValue refers to the featureID of what you are clicking on.
   */
  @Prop() readonly clickValue: number;

  /**
   * The ID of the map this story step works with
   */
  @Prop() readonly mapId: string;

  private _didClick: boolean = false;

  @Watch("active")
  activeChanged() {
    if (this.active) this.activate();
  }

  private activate() {
    let mapEl: HTMLGlMapElement = document.querySelector(
      this.mapId != undefined ? `gl-map#${this.mapId}` : "gl-map"
    );
    if (mapEl == undefined || mapEl.map == undefined) return;

    this._didClick = false;
    let doClick = () => this.scheduleClick(mapEl.map);

    // If configured, try locating the click feature before the map moves.
    if (this.clickBeforeHash) doClick();

    // Set the click event to fire once the map move has completed.
    // Then change the hash.
    if (this.hash != undefined) {
      if (this.clickAfterIdle) mapEl.map.once("idle", doClick);
      window.location.hash = this.hash[0] === "#" ? this.hash : `#${this.hash}`;
    }
  }

  private scheduleClick(map: maplibregl.Map) {
    if (
      this._didClick ||
      this.clickLayer == undefined ||
      this.clickValue == undefined
    )
      return;

    setTimeout(() => this.fireClickEvent(map), this.clickDelay);
  }

  private fireClickEvent(map: maplibregl.Map) {
    if (this._didClick) return;

    // If clickProperty is not set, we assume that clickValue refers to
    // the feature ID.
    let leftSide: ExpressionSpecification =
      this.clickProperty != undefined ? ["get", this.clickProperty] : ["id"];
    let features = map.queryRenderedFeatures(undefined, {
      layers: [this.clickLayer],
      filter: ["==", leftSide, this.clickValue],
    });

    if (features.length !== 0) {
      this.glFeatureClick.emit({
        features: features,
      });
      this._didClick = true;
    }
  }

  render() {
    return (
      <Host style={{ display: this.active ? "block" : "none" }}>
        <slot />
      </Host>
    );
  }
}
