import { h } from "@stencil/core";
import { HTMLStencilElement } from "@stencil/core/internal";
import { TDocumentDefinitions } from "pdfmake/interfaces";

export function getMap(id?: string): HTMLGlMapElement {
  return document.querySelector(id != undefined ? `gl-map#${id}` : "gl-map");
}

export function getThumbnail(item: { image: string }) {
  if (item.image != undefined)
    return (
      <ion-thumbnail slot="start">
        <img src={item.image} />
      </ion-thumbnail>
    );
}

export function toArray(value: string | string[], sep = ",") {
  if (value == undefined) return [];
  if (Array.isArray(value)) return value;
  return value.split(sep);
}

// Based on: https://davidwalsh.name/get-absolute-url
export const getAbsoluteUrl = (function () {
  let a: HTMLAnchorElement;
  return function (url: string) {
    if (a == undefined) a = document.createElement("a");
    a.href = url;
    return a.href;
  };
})();

export async function fetchPdf(
  url: string,
  filename: string,
  docDef: TDocumentDefinitions
) {
  let res = await fetch(url + filename, {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(docDef),
  });
  let blob = await res.blob();
  const nav = window.navigator as any;
  if (nav.msSaveOrOpenBlob) {
    nav.msSaveOrOpenBlob(blob, `${filename}.pdf`);
  } else {
    window.open(window.URL.createObjectURL(blob));
  }
}

export async function findOrCreateOnReady<
  E extends HTMLStencilElement = HTMLStencilElement
>(tagName: string): Promise<E> {
  let elm = document.querySelector<E>(tagName);
  if (elm == null) {
    elm = document.createElement(tagName) as E;
    document.body.appendChild(elm);
  }
  return typeof elm.componentOnReady === "function"
    ? elm.componentOnReady()
    : Promise.resolve(elm);
}
