import { Component, Prop } from "@stencil/core";

@Component({
  tag: "gl-download-file",
})
export class DownloadFile {
  /**
   * Human-readable name of the file.
   */
  @Prop() readonly label: string;

  /**
   * Human-readable file size (e.g., 2.5 MB).
   */
  @Prop() readonly size: string;

  /**
   * URL of the file.
   */
  @Prop() readonly url: string;

  /**
   * File format (e.g., PDF).
   */
  @Prop() readonly format: string;
}
