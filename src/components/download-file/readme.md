# gl-download-file

The download file component stores information for a file that will be
included in a [gl-download-list](../download-list).

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                              | Type     | Default     |
| -------- | --------- | ---------------------------------------- | -------- | ----------- |
| `format` | `format`  | File format (e.g., PDF).                 | `string` | `undefined` |
| `label`  | `label`   | Human-readable name of the file.         | `string` | `undefined` |
| `size`   | `size`    | Human-readable file size (e.g., 2.5 MB). | `string` | `undefined` |
| `url`    | `url`     | URL of the file.                         | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
