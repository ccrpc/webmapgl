import {
  h,
  Component,
  Element,
  Event,
  EventEmitter,
  Listen,
  Method,
  Prop,
  State,
  Host,
} from "@stencil/core";
import { _t } from "../i18n/i18n";
import {
  InputCustomEvent,
  SelectCustomEvent,
  TextareaCustomEvent,
  RadioGroupCustomEvent,
} from "@ionic/core";

@Component({
  tag: "gl-field",
})
export class Field {
  @Element() el: HTMLGlFieldElement;

  /**
   * Emitted when the field value is changed by the user.
   */
  @Event() glFieldValueChanged: EventEmitter;

  @State() message: string;

  /**
   * The feature property that corresponds to the form field.
   */
  @Prop() readonly attribute: string;

  /**
   * Image URL for the field item.
   */
  @Prop() readonly image: string;

  /**
   * Label text for the field item.
   */
  @Prop() readonly label: string;

  /**
   * The field must be filled out before the form can be submitted.
   */
  @Prop() readonly required: boolean = false;

  /**
   * Field type. It is not currently used.
   */
  @Prop() readonly type: any;

  /**
   * Widget used to render the field.
   */
  @Prop() readonly widget: "input" | "radio" | "select" | "textarea";

  @Listen("glOptionChanged")
  optionChanged(e) {
    this.changed(e.detail);
  }

  private _getValue() {
    const form = this.el.closest("gl-form");
    if (form.feature == undefined || form.feature.properties == undefined)
      return;
    return form.feature.properties[this.attribute];
  }

  /**
   * Returns whether this field is valid
   * @returns
   */
  @Method()
  async isValid() {
    return this.validate() == null;
  }

  /**
   * Returns the value of the attribute of the feature this form is editing
   * @returns value of the attribute of the feature
   */
  @Method()
  async getValue() {
    return this._getValue();
  }

  /**
   * Checks whether the value is required and also present
   * If it is not returns an error text, if it is valid returns null
   * @returns
   */
  @Method()
  async validate() {
    const value = await this.getValue();
    if (this.required && (value == undefined || value === ""))
      return _t("webmapgl.field.required", { field: this.label });

    return null;
  }

  private changed(value: any) {
    this.glFieldValueChanged.emit({
      field: this,
      value: value,
    });
  }

  private onChange = (e: SelectCustomEvent | RadioGroupCustomEvent) =>
    this.changed(e.detail.value);

  private getRadioField() {
    return (
      <Host>
        <ion-list-header>{this.label}</ion-list-header>
        <ion-radio-group onIonChange={this.onChange}>
          <slot />
        </ion-radio-group>
      </Host>
    );
  }

  private getSelectField() {
    return (
      <ion-item>
        <ion-label>{this.label}</ion-label>,
        <ion-select onIonChange={this.onChange} value={this.getValue()}>
          <slot />
        </ion-select>
      </ion-item>
    );
  }

  private onInput = (e: TextareaCustomEvent | InputCustomEvent) =>
    this.changed(e.detail.value);

  private getTextareaField() {
    return (
      <ion-item>
        <ion-label text-wrap position="floating">
          {this.label}
        </ion-label>
        <ion-textarea
          onIonInput={this.onInput}
          value={this._getValue() || ""}
          legacy={true} // We are opting for the legacy version of labels for textarea as there is no other way to text-wrap the label
        ></ion-textarea>
      </ion-item>
    );
  }

  private getInputField() {
    return (
      <ion-item>
        <ion-label text-wrap position="floating">
          {this.label}
        </ion-label>
        <ion-input
          onIonInput={this.onInput}
          type="text"
          value={this._getValue()}
        ></ion-input>
      </ion-item>
    );
  }

  render() {
    if (this.widget === "radio") {
      return this.getRadioField();
    } else if (this.widget === "select") {
      return this.getSelectField();
    } else if (this.widget == "textarea") {
      return this.getTextareaField();
    } else {
      return this.getInputField();
    }
  }
}
