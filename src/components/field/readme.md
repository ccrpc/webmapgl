# gl-field

The field component adds a field to a [gl-form](../form). It is used
to set the value of a GeoJSON feature property.

<!-- Auto Generated Below -->


## Properties

| Property    | Attribute   | Description                                                    | Type                                           | Default     |
| ----------- | ----------- | -------------------------------------------------------------- | ---------------------------------------------- | ----------- |
| `attribute` | `attribute` | The feature property that corresponds to the form field.       | `string`                                       | `undefined` |
| `image`     | `image`     | Image URL for the field item.                                  | `string`                                       | `undefined` |
| `label`     | `label`     | Label text for the field item.                                 | `string`                                       | `undefined` |
| `required`  | `required`  | The field must be filled out before the form can be submitted. | `boolean`                                      | `false`     |
| `type`      | `type`      | Field type. It is not currently used.                          | `any`                                          | `undefined` |
| `widget`    | `widget`    | Widget used to render the field.                               | `"input" \| "radio" \| "select" \| "textarea"` | `undefined` |


## Events

| Event                 | Description                                          | Type               |
| --------------------- | ---------------------------------------------------- | ------------------ |
| `glFieldValueChanged` | Emitted when the field value is changed by the user. | `CustomEvent<any>` |


## Methods

### `getValue() => Promise<any>`

Returns the value of the attribute of the feature this form is editing

#### Returns

Type: `Promise<any>`

value of the attribute of the feature

### `isValid() => Promise<boolean>`

Returns whether this field is valid

#### Returns

Type: `Promise<boolean>`



### `validate() => Promise<string>`

Checks whether the value is required and also present
If it is not returns an error text, if it is valid returns null

#### Returns

Type: `Promise<string>`




## Dependencies

### Used by

 - [gl-form-page](../form-page)

### Depends on

- ion-list-header
- ion-radio-group
- ion-item
- ion-label
- ion-select
- ion-textarea
- ion-input

### Graph
```mermaid
graph TD;
  gl-field --> ion-list-header
  gl-field --> ion-radio-group
  gl-field --> ion-item
  gl-field --> ion-label
  gl-field --> ion-select
  gl-field --> ion-textarea
  gl-field --> ion-input
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-select --> ion-select-popover
  ion-select --> ion-popover
  ion-select --> ion-action-sheet
  ion-select --> ion-alert
  ion-select --> ion-icon
  ion-select-popover --> ion-item
  ion-select-popover --> ion-checkbox
  ion-select-popover --> ion-label
  ion-select-popover --> ion-radio-group
  ion-select-popover --> ion-radio
  ion-select-popover --> ion-list
  ion-select-popover --> ion-list-header
  ion-popover --> ion-backdrop
  ion-action-sheet --> ion-backdrop
  ion-action-sheet --> ion-icon
  ion-action-sheet --> ion-ripple-effect
  ion-alert --> ion-ripple-effect
  ion-alert --> ion-backdrop
  ion-input --> ion-icon
  gl-form-page --> gl-field
  style gl-field fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
