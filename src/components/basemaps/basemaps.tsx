import { h, Component, Element } from "@stencil/core";
import { popoverController } from "@ionic/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-basemaps",
})
export class Basemaps {
  @Element() el: HTMLGlBasemapsElement;

  private async openPopover(ev: UIEvent) {
    const options = {
      component: document.createElement("gl-basemap-switcher"),
      ev: ev,
    };
    const popover = await popoverController.create(options);
    await popover.present();
    return popover;
  }

  private selectBasemap = (e: UIEvent) => this.openPopover(e);

  render() {
    let title = _t("webmapgl.basemaps.title");
    return (
      <ion-button onClick={this.selectBasemap} title={title}>
        <ion-icon slot="icon-only" name="earth"></ion-icon>
      </ion-button>
    );
  }
}
