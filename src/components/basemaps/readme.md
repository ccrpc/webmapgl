# gl-basemaps

The base maps component renders a button that opens a
[gl-basemap-switcher](../basemap-switcher) in a popover.

<!-- Auto Generated Below -->


## Dependencies

### Depends on

- [gl-basemap-switcher](../basemap-switcher)
- ion-button
- ion-icon

### Graph
```mermaid
graph TD;
  gl-basemaps --> gl-basemap-switcher
  gl-basemaps --> ion-button
  gl-basemaps --> ion-icon
  gl-basemap-switcher --> ion-item
  gl-basemap-switcher --> ion-thumbnail
  gl-basemap-switcher --> ion-radio
  gl-basemap-switcher --> ion-label
  gl-basemap-switcher --> ion-content
  gl-basemap-switcher --> ion-list
  gl-basemap-switcher --> ion-radio-group
  gl-basemap-switcher --> ion-list-header
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-button --> ion-ripple-effect
  style gl-basemaps fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
