# gl-draw-toolbar

The draw toolbar component is displayed during a drawing session. It allows
the user to end the session by canceling or confirming the draw operation.

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                                                                                                                                                          | Type                                                                                                                                             | Default                               |
| ------------- | -------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------- |
| `cancelText`  | `cancel-text`  | Text for the cancel button.                                                                                                                                                                          | `string`                                                                                                                                         | `_t("webmapgl.draw-toolbar.cancel")`  |
| `color`       | `color`        | Background color for the toolbar.                                                                                                                                                                    | `"danger" \| "dark" \| "light" \| "medium" \| "primary" \| "secondary" \| "success" \| "tertiary" \| "warning" \| string & Record<never, never>` | `"primary"`                           |
| `confirmText` | `confirm-text` | Text for the confirm button.                                                                                                                                                                         | `string`                                                                                                                                         | `_t("webmapgl.draw-toolbar.confirm")` |
| `label`       | `label`        | Label for the toolbar.                                                                                                                                                                               | `string`                                                                                                                                         | `_t("webmapgl.draw-toolbar.label")`   |
| `mapId`       | `map-id`       | ID of the map with the drawing session.  This defaults to the empty string as when a gl-map element is created without an id, the mapId passed along the glDraw events defaults to the empty string. | `string`                                                                                                                                         | `""`                                  |
| `maxFeatures` | `max-features` | Maximum number of features the user can draw.                                                                                                                                                        | `number`                                                                                                                                         | `1`                                   |
| `minFeatures` | `min-features` | Minimum number of features the user must draw before confirming.                                                                                                                                     | `number`                                                                                                                                         | `1`                                   |


## Events

| Event           | Description                                        | Type               |
| --------------- | -------------------------------------------------- | ------------------ |
| `glDrawCancel`  | Emitted when the user cancels the draw session.    | `CustomEvent<any>` |
| `glDrawConfirm` | Emitted when the user confirms the drawn features. | `CustomEvent<any>` |


## Dependencies

### Depends on

- ion-toolbar
- ion-title
- ion-buttons
- ion-button
- ion-icon

### Graph
```mermaid
graph TD;
  gl-draw-toolbar --> ion-toolbar
  gl-draw-toolbar --> ion-title
  gl-draw-toolbar --> ion-buttons
  gl-draw-toolbar --> ion-button
  gl-draw-toolbar --> ion-icon
  ion-button --> ion-ripple-effect
  style gl-draw-toolbar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
