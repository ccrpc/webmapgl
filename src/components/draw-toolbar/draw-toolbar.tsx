import {
  h,
  Component,
  Event,
  EventEmitter,
  Listen,
  Prop,
  State,
} from "@stencil/core";
import { Color } from "@ionic/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-draw-toolbar",
})
export class DrawToolbar {
  /**
   * Emitted when the user cancels the draw session.
   */
  @Event() glDrawCancel: EventEmitter;

  /**
   * Emitted when the user confirms the drawn features.
   */
  @Event() glDrawConfirm: EventEmitter;

  @State() visible: boolean = false;
  @State() featureCount = 0;

  /**
   * Text for the cancel button.
   */
  @Prop() readonly cancelText: string = _t("webmapgl.draw-toolbar.cancel");

  /**
   * Background color for the toolbar.
   */
  @Prop() readonly color: Color = "primary";

  /**
   * Text for the confirm button.
   */
  @Prop() readonly confirmText: string = _t("webmapgl.draw-toolbar.confirm");

  /**
   * Label for the toolbar.
   */
  @Prop() readonly label: string = _t("webmapgl.draw-toolbar.label");

  /**
   * ID of the map with the drawing session.
   *
   * This defaults to the empty string as when a gl-map element is created without an id, the mapId passed along the glDraw events defaults to the empty string.
   */
  @Prop() readonly mapId: string = "";

  /**
   * Maximum number of features the user can draw.
   */
  @Prop() readonly maxFeatures: number = 1;

  /**
   * Minimum number of features the user must draw before confirming.
   */
  @Prop() readonly minFeatures: number = 1;

  @Listen("glDrawEnter", { target: "body" })
  handleDrawEnter(e: CustomEvent) {
    if (e.detail.mapId !== this.mapId) return;
    this.featureCount = 0;
    this.visible = true;
  }

  @Listen("glDrawExit", { target: "body" })
  handleDrawExit(e: CustomEvent) {
    if (e.detail.mapId !== this.mapId) return;
    this.visible = false;
  }

  @Listen("glDrawCreate", { target: "body" })
  handleDrawCreate(e: CustomEvent) {
    if (e.detail.mapId !== this.mapId) return;
    this.featureCount += 1;
  }

  @Listen("glDrawDelete", { target: "body" })
  handleDrawDelete(e: CustomEvent) {
    if (e.detail.mapId !== this.mapId) return;
    this.featureCount -= 1;
  }

  private cancel() {
    this.glDrawCancel.emit({
      mapId: this.mapId,
    });
  }

  private confirm() {
    this.glDrawConfirm.emit({
      mapId: this.mapId,
    });
  }

  private callCancel = () => this.cancel();
  private callConfirm = () => this.confirm();

  render() {
    if (!this.visible) return;

    let small = screen.width <= 640;
    let canConfirm =
      this.featureCount >= this.minFeatures &&
      this.featureCount <= this.maxFeatures;
    return (
      <ion-toolbar color={this.color}>
        <ion-title>{this.label}</ion-title>
        <ion-buttons slot="end">
          <ion-button onClick={this.callCancel}>
            <ion-icon
              slot={small ? "icon-only" : "start"}
              name="close"
            ></ion-icon>
            {small ? null : this.cancelText}
          </ion-button>
          <ion-button
            onClick={canConfirm ? this.callConfirm : undefined}
            disabled={!canConfirm}
          >
            <ion-icon
              slot={small ? "icon-only" : "start"}
              name="checkmark"
            ></ion-icon>
            {small ? null : this.confirmText}
          </ion-button>
        </ion-buttons>
      </ion-toolbar>
    );
  }
}
