export interface FormOptions {
  label?: string;
  formId?: string;
  schema: string;
  submitText?: string;
  cancelText?: string;
  translateText?: boolean;
}
