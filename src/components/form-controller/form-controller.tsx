import { Component, Method } from "@stencil/core";
import { modalController } from "@ionic/core";
import { FormOptions } from "./interface";

@Component({
  tag: "gl-form-controller",
})
export class FormController {
  /**
   * Display a form in a modal. If the form is canceled, it resolves to
   * `undefined`. If the form is submitted, it resolves to the GeoJSON
   * feature updated by the form.
   * @param feature The GeoJSON feature that the form will edit.
   * @param options Option object used to set the properties of the `gl-form`.
   */
  @Method()
  async create(
    feature: { properties: { [key: string]: any } },
    options: FormOptions
  ) {
    let form = document.createElement("gl-form");
    form.feature = feature;
    for (let optionName in options) form[optionName] = options[optionName];

    const modal = await modalController.create({
      component: form,
    });

    await modal.present();

    return await new Promise<any>((resolve) => {
      form.addEventListener("glFormCancel", () => {
        modal.dismiss();
        resolve(undefined);
      });

      form.addEventListener("glFormSubmit", () => {
        modal.dismiss();
        resolve(feature);
      });
    });
  }
}
