# gl-form-controller

The form controller component binds a [`gl-form`](../form) to a GeoJSON feature
and displays the form in a modal. 

<!-- Auto Generated Below -->


## Methods

### `create(feature: { properties: { [key: string]: any; }; }, options: FormOptions) => Promise<any>`

Display a form in a modal. If the form is canceled, it resolves to
`undefined`. If the form is submitted, it resolves to the GeoJSON
feature updated by the form.

#### Returns

Type: `Promise<any>`




## Dependencies

### Depends on

- [gl-form](../form)

### Graph
```mermaid
graph TD;
  gl-form-controller --> gl-form
  gl-form --> gl-form-page
  gl-form --> ion-nav
  gl-form-page --> gl-facet
  gl-form-page --> gl-field
  gl-form-page --> gl-option
  gl-form-page --> ion-header
  gl-form-page --> ion-toolbar
  gl-form-page --> ion-buttons
  gl-form-page --> ion-button
  gl-form-page --> ion-icon
  gl-form-page --> ion-title
  gl-form-page --> ion-content
  gl-form-page --> ion-list
  gl-facet --> ion-list-header
  gl-facet --> ion-item
  gl-facet --> ion-label
  gl-facet --> ion-thumbnail
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  gl-field --> ion-list-header
  gl-field --> ion-radio-group
  gl-field --> ion-item
  gl-field --> ion-label
  gl-field --> ion-select
  gl-field --> ion-textarea
  gl-field --> ion-input
  ion-select --> ion-select-popover
  ion-select --> ion-popover
  ion-select --> ion-action-sheet
  ion-select --> ion-alert
  ion-select --> ion-icon
  ion-select-popover --> ion-item
  ion-select-popover --> ion-checkbox
  ion-select-popover --> ion-label
  ion-select-popover --> ion-radio-group
  ion-select-popover --> ion-radio
  ion-select-popover --> ion-list
  ion-select-popover --> ion-list-header
  ion-popover --> ion-backdrop
  ion-action-sheet --> ion-backdrop
  ion-action-sheet --> ion-icon
  ion-action-sheet --> ion-ripple-effect
  ion-alert --> ion-ripple-effect
  ion-alert --> ion-backdrop
  ion-input --> ion-icon
  gl-option --> ion-item
  gl-option --> ion-radio
  gl-option --> ion-label
  gl-option --> ion-select-option
  gl-option --> ion-thumbnail
  ion-button --> ion-ripple-effect
  style gl-form-controller fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
