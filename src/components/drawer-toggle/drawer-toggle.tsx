import { h, Component, Element, Prop } from "@stencil/core";
import { _t } from "../i18n/i18n";
import { findOrCreateOnReady } from "../utils";

@Component({
  tag: "gl-drawer-toggle",
})
export class DrawerToggle {
  private drawer: HTMLGlDrawerElement;

  @Element() el: HTMLGlDrawerToggleElement;

  /**
   * Icon to display in the button.
   */
  @Prop() readonly icon: string = "settings";

  /**
   * Text to display in the button.
   */
  @Prop() readonly buttonTitle: string = _t("webmapgl.drawer-toggle.label");

  /**
   * Whether the button is enabled
   */
  @Prop() readonly disabled: boolean = false;

  async componentWillLoad() {
    if (this.drawer == undefined)
      this.drawer = await findOrCreateOnReady("gl-drawer");
  }

  private toggleDrawer() {
    this.drawer.open = !this.drawer.open;
  }

  private clickToggle = () => this.toggleDrawer();

  render() {
    return (
      <ion-button
        onClick={this.clickToggle}
        title={this.buttonTitle}
        disabled={this.disabled}
      >
        <ion-icon slot="icon-only" name={this.icon}></ion-icon>
      </ion-button>
    );
  }
}
