# gl-drawer-toggle

The drawer toggle component displays a button that opens or closes a
[gl-drawer](../drawer).

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                    | Type      | Default                              |
| ------------- | -------------- | ------------------------------ | --------- | ------------------------------------ |
| `buttonTitle` | `button-title` | Text to display in the button. | `string`  | `_t("webmapgl.drawer-toggle.label")` |
| `disabled`    | `disabled`     | Whether the button is enabled  | `boolean` | `false`                              |
| `icon`        | `icon`         | Icon to display in the button. | `string`  | `"settings"`                         |


## Dependencies

### Depends on

- ion-button
- ion-icon
- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-drawer-toggle --> ion-button
  gl-drawer-toggle --> ion-icon
  gl-drawer-toggle --> ion-thumbnail
  ion-button --> ion-ripple-effect
  style gl-drawer-toggle fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
