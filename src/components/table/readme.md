# gl-table

The gl-table component renders the properties of a list of GeoJSON features.
Each property of these features that is to be displayed needs its own [gl-table-column](../table-column/).

<!-- Auto Generated Below -->


## Properties

| Property             | Attribute              | Description                                                               | Type                                         | Default     |
| -------------------- | ---------------------- | ------------------------------------------------------------------------- | -------------------------------------------- | ----------- |
| `disableDefaultSort` | `disable-default-sort` | Whether or not to disable the default sorting options                     | `boolean`                                    | `false`     |
| `features`           | --                     | The features this table displays                                          | `{ properties: { [key: string]: any; }; }[]` | `[]`        |
| `rowClassAttribute`  | `row-class-attribute`  | The name of the attribute whose value to use as the css class of the row. | `string`                                     | `undefined` |
| `sortAttribute`      | `sort-attribute`       | What attribute to sort on                                                 | `string`                                     | `undefined` |
| `sortOrder`          | `sort-order`           | What order to use for sorting                                             | `"asc" \| "desc"`                            | `"asc"`     |


## Events

| Event         | Description                                        | Type               |
| ------------- | -------------------------------------------------- | ------------------ |
| `glTableSort` | Event emitted when the table sort order is changed | `CustomEvent<any>` |


## Methods

### `getCellContents(feature: { properties: { [key: string]: any; }; }, column: HTMLGlTableColumnElement, format: "pdf" | "text") => Promise<any>`

Returns the contents of a cell

#### Returns

Type: `Promise<any>`



### `getSortedFeatures() => Promise<{ properties: { [key: string]: any; }; }[]>`

Returns the features the table displays in sorted order

#### Returns

Type: `Promise<{ properties: { [key: string]: any; }; }[]>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
