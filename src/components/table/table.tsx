import {
  h,
  Component,
  Element,
  Event,
  EventEmitter,
  Listen,
  Method,
  Prop,
  State,
} from "@stencil/core";
import { _t } from "../i18n/i18n";
import { HTMLStencilElement } from "@ionic/core";

let _nextId = 0;

interface potentialComponent extends HTMLStencilElement {
  feature: { properties: { [key: string]: any } };
  column: HTMLGlTableColumnElement;
  value: any;
  formatValue?: any;
}

@Component({
  styleUrl: "table.css",
  tag: "gl-table",
})
export class Table {
  @Element() el: HTMLGlTableElement;

  /**
   * Event emitted when the table sort order is changed
   */
  @Event() glTableSort: EventEmitter;

  @State() columns: HTMLGlTableColumnElement[] = [];

  /**
   * Whether or not to disable the default sorting options
   */
  @Prop() readonly disableDefaultSort: boolean = false;

  /**
   * The features this table displays
   */
  @Prop({ mutable: true }) features: { properties: { [key: string]: any } }[] = // eslint-disable-line @stencil-community/strict-mutable
    [];

  /**
   * The name of the attribute whose value to use as the css class of the row.
   */
  @Prop() readonly rowClassAttribute: string;

  /**
   * What attribute to sort on
   */
  @Prop({ mutable: true }) sortAttribute: string;

  /**
   * What order to use for sorting
   */
  @Prop({ mutable: true }) sortOrder: "asc" | "desc" = "asc";

  componentWillLoad() {
    if (this.el.id == undefined) {
      this.el.id = "table-" + _nextId.toString();
      _nextId += 1;
    }
    this.updateColumns();
  }

  @Listen("glTableColumnChanged")
  updateColumns() {
    this.columns = Array.from(this.el.querySelectorAll("gl-table-column"));
  }

  private setSort(column: HTMLGlTableColumnElement) {
    if (column.attribute === this.sortAttribute) {
      this.sortOrder = this.sortOrder === "asc" ? "desc" : "asc";
    } else {
      this.sortAttribute = column.attribute;
      this.sortOrder = "asc";
    }

    this.glTableSort.emit({
      tableId: this.el.id,
      attribute: this.sortAttribute,
      order: this.sortOrder,
    });
  }

  /**
   * Returns the features the table displays in sorted order
   * @returns
   */
  @Method()
  async getSortedFeatures() {
    return this.sortedFeatures();
  }

  private sortedFeatures() {
    if (this.disableDefaultSort) return this.features;
    let features = [...this.features];
    features.sort((aFeature, bFeature) => {
      let a = aFeature.properties[this.sortAttribute];
      let b = bFeature.properties[this.sortAttribute];
      if (typeof a === "string") a = a.toUpperCase();
      if (typeof b === "string") b = b.toUpperCase();
      let cmp = 0;
      if (a > b) cmp = 1;
      else if (a < b) cmp = -1;
      return this.sortOrder === "desc" ? cmp * -1 : cmp;
    });
    return features;
  }

  private renderHeader(column: HTMLGlTableColumnElement) {
    let columnProps = {};
    if (column.sortable) {
      columnProps = {
        class: "sortable sorted-none",
        tabindex: 0,
        "aria-controls": this.el.id,
        "aria-label":
          `${column.label}: ` + _t(`webmapgl.table.header.sort-action.desc`),
        onClick: () => this.setSort(column),
      };
      if (column.attribute === this.sortAttribute) {
        columnProps["class"] = `sortable sorted-${this.sortOrder}`;
        columnProps["aria-sort"] =
          this.sortOrder === "asc" ? "ascending" : "descending";
        columnProps["aria-label"] =
          `${column.label}: ` +
          _t(`webmapgl.table.header.sort-action.${this.sortOrder}`);
      }
    }

    return <th {...columnProps}>{column.label}</th>;
  }

  /**
   * Returns the contents of a cell
   * @param feature
   * @param column
   * @param format
   * @returns
   */
  @Method()
  async getCellContents(
    feature: { properties: { [key: string]: any } },
    column: HTMLGlTableColumnElement,
    format: "pdf" | "text"
  ) {
    let value = feature.properties[column.attribute];
    if (column.component == undefined) return this.renderValue(column, value);
    return await this.renderComponentAsText(feature, column, value, format);
  }

  private renderValue(column: HTMLGlTableColumnElement, value: any) {
    if (value == undefined) return null;
    if (column.format) value = value.toLocaleString({}, column.format);
    return value.toString();
  }

  private renderComponentJSX(
    feature: { properties: { [key: string]: any } },
    column: HTMLGlTableColumnElement,
    value: any
  ) {
    let Component = column.component;
    let props = column.componentProps || {};
    return (
      <Component
        feature={feature}
        column={column}
        value={value}
        {...props}
      ></Component>
    );
  }

  private async renderComponentAsText(
    feature: { properties: { [key: string]: any } },
    column: HTMLGlTableColumnElement,
    value: any,
    format: any
  ) {
    let component = document.createElement(
      column.component
    ) as potentialComponent;
    let props = column.componentProps || {};
    for (let prop in props) component[prop] = props[prop];
    component.feature = feature;
    component.column = column;
    component.value = value;
    component.style.display = "none";

    this.el.appendChild(component);
    await component.componentOnReady();

    let result =
      typeof component.formatValue === "function"
        ? component.formatValue(format)
        : component.textContent;

    this.el.removeChild(component);
    return result;
  }

  private renderCell(
    feature: { properties: { [key: string]: any } },
    column: HTMLGlTableColumnElement
  ) {
    let value = feature.properties[column.attribute];
    let cssClass =
      column.cellClassAttribute != undefined
        ? feature.properties[column.cellClassAttribute]
        : "cell";
    return (
      <td class={cssClass}>
        {column.component != undefined
          ? this.renderComponentJSX(feature, column, value)
          : this.renderValue(column, value)}
      </td>
    );
  }

  private renderRow(
    feature: { properties: { [key: string]: any } },
    columns: HTMLGlTableColumnElement[]
  ) {
    let cssClass =
      this.rowClassAttribute != undefined
        ? feature.properties[this.rowClassAttribute]
        : "row";
    return (
      <tr class={cssClass}>
        {columns.map((col) => this.renderCell(feature, col))}
      </tr>
    );
  }

  render() {
    if (this.columns.length === 0) return null;

    let features = this.sortedFeatures();
    let columns = this.columns.filter((col) => col.visible);
    let header = columns.map((col) => this.renderHeader(col));
    let bodyRows = features.map((feature) => this.renderRow(feature, columns));

    return (
      <table id={this.el.id}>
        <thead>
          <tr>{header}</tr>
        </thead>
        <tbody>{bodyRows}</tbody>
      </table>
    );
  }
}
