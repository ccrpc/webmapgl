import { h, Component } from "@stencil/core";
import { _t } from "../i18n/i18n";
import { RadioGroupCustomEvent } from "@ionic/core";

@Component({
  tag: "gl-basemap-switcher",
})
export class BasemapSwitcher {
  private activeBasemap: string;

  componentWillLoad() {
    this.activeBasemap = this.getBasemapStyles().filter(
      (style) => style.enabled
    )[0].url;
  }

  private getBasemapStyles() {
    return Array.from(document.querySelectorAll("gl-style")).filter(
      (style) => style.basemap
    );
  }

  private setBasemap(url: string) {
    this.activeBasemap = url;
    this.getBasemapStyles().forEach(
      (style) => (style.enabled = style.url === url)
    );
  }

  private basemapMakeSelection = (e: RadioGroupCustomEvent<string>) =>
    this.setBasemap(e.detail.value);

  render() {
    const header = _t("webmapgl.basemap-switcher.header");
    let items = this.getBasemapStyles().map((style) => (
      <ion-item>
        {style.thumbnail != undefined ? (
          <ion-thumbnail slot="start">
            <img src={style.thumbnail} alt={style.name} />
          </ion-thumbnail>
        ) : null}
        <ion-radio value={style.url}>
          <ion-label text-wrap>{style.name}</ion-label>
        </ion-radio>
      </ion-item>
    ));
    return (
      <ion-content>
        <ion-list>
          <ion-radio-group
            onIonChange={this.basemapMakeSelection}
            value={this.activeBasemap}
          >
            <ion-list-header>{header}</ion-list-header>
            {items}
          </ion-radio-group>
        </ion-list>
      </ion-content>
    );
  }
}
