# gl-basemap-switcher

The base map switcher provides a user interface for changing which base map
is active in a `gl-map`. It shows as choices all `gl-style` elements with
a `basemap` property of `true` and changes the `enabled` property of the
style based on the user's selection.

<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [gl-basemaps](../basemaps)

### Depends on

- ion-item
- ion-thumbnail
- ion-radio
- ion-label
- ion-content
- ion-list
- ion-radio-group
- ion-list-header

### Graph
```mermaid
graph TD;
  gl-basemap-switcher --> ion-item
  gl-basemap-switcher --> ion-thumbnail
  gl-basemap-switcher --> ion-radio
  gl-basemap-switcher --> ion-label
  gl-basemap-switcher --> ion-content
  gl-basemap-switcher --> ion-list
  gl-basemap-switcher --> ion-radio-group
  gl-basemap-switcher --> ion-list-header
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  gl-basemaps --> gl-basemap-switcher
  style gl-basemap-switcher fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
