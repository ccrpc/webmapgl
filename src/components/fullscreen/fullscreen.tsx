import { h, Component, State } from "@stencil/core";
import screenfull from "screenfull";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-fullscreen",
})
export class Fullscreen {
  @State() fullscreen = false;

  componentDidLoad() {
    screenfull.on("change", () => (this.fullscreen = screenfull.isFullscreen));
  }

  private toggleFullscreen() {
    screenfull.isFullscreen ? screenfull.exit() : screenfull.request();
  }

  private clickToggle = () => this.toggleFullscreen();

  render() {
    let title = this.fullscreen
      ? _t("webmapgl.fullscreen.exit")
      : _t("webmapgl.fullscreen.enter");
    if (screenfull.isEnabled)
      return (
        <ion-button onClick={this.clickToggle} title={title}>
          <ion-icon
            slot="icon-only"
            name={this.fullscreen ? "contract" : "expand"}
          ></ion-icon>
        </ion-button>
      );
  }
}
