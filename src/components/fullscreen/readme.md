# gl-fullscreen

The fullscreen component provides a button to toggle fullscreen mode. If
the application is in an iframe, the iframe must [allow fullscreen
mode](https://developer.mozilla.org/en-US/docs/Web/HTTP/Feature_Policy/Using_Feature_Policy)
for the button to be displayed.

<!-- Auto Generated Below -->


## Dependencies

### Depends on

- ion-button
- ion-icon

### Graph
```mermaid
graph TD;
  gl-fullscreen --> ion-button
  gl-fullscreen --> ion-icon
  ion-button --> ion-ripple-effect
  style gl-fullscreen fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
