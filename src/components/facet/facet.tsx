import {
  h,
  Component,
  Element,
  Event,
  EventEmitter,
  Prop,
} from "@stencil/core";
import { getThumbnail } from "../utils";

@Component({
  tag: "gl-facet",
})
export class Facet {
  @Element() el: HTMLGlFacetElement;

  /**
   * Emitted when the facet is selected by the user.
   */
  @Event() glFormFacet: EventEmitter;

  /**
   * Show the detail arrow in the item.
   */
  @Prop() readonly detail: boolean = true;

  /**
   * Image URL for the item.
   */
  @Prop() readonly image: string;

  /**
   * Text for the item.
   */
  @Prop() readonly label: string;

  /**
   * The value of the facet used to refer to it in fields and other
   * facets. When the facet is selected, the value is emitted as
   * part of the `glFormFacet` event.
   */
  @Prop() readonly value: string;

  /**
   * Widget to use for displaying the facet. A value of `header` renders
   * an `ion-list-header` instead of an `ion-item`.
   */
  @Prop() readonly widget: "header" | "item" = "item";

  private setFacet() {
    this.glFormFacet.emit({
      label: this.label,
      value: this.value,
    });
  }

  private callSetFacet = () => this.setFacet();

  render() {
    if (this.widget === "header")
      return <ion-list-header>{this.label}</ion-list-header>;

    return (
      <ion-item button={true} onClick={this.callSetFacet} detail={this.detail}>
        {getThumbnail(this)}
        <ion-label text-wrap>{this.label}</ion-label>
      </ion-item>
    );
  }
}
