# gl-form-facets

The form facet component provides a means of filtering the fields and
options that appear in a [gl-form](../form), simplifying the user experience
for complex forms.

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                                                                                                                 | Type                 | Default     |
| -------- | --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------- | ----------- |
| `detail` | `detail`  | Show the detail arrow in the item.                                                                                                                          | `boolean`            | `true`      |
| `image`  | `image`   | Image URL for the item.                                                                                                                                     | `string`             | `undefined` |
| `label`  | `label`   | Text for the item.                                                                                                                                          | `string`             | `undefined` |
| `value`  | `value`   | The value of the facet used to refer to it in fields and other facets. When the facet is selected, the value is emitted as part of the `glFormFacet` event. | `string`             | `undefined` |
| `widget` | `widget`  | Widget to use for displaying the facet. A value of `header` renders an `ion-list-header` instead of an `ion-item`.                                          | `"header" \| "item"` | `"item"`    |


## Events

| Event         | Description                                     | Type               |
| ------------- | ----------------------------------------------- | ------------------ |
| `glFormFacet` | Emitted when the facet is selected by the user. | `CustomEvent<any>` |


## Dependencies

### Used by

 - [gl-form-page](../form-page)

### Depends on

- ion-list-header
- ion-item
- ion-label
- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-facet --> ion-list-header
  gl-facet --> ion-item
  gl-facet --> ion-label
  gl-facet --> ion-thumbnail
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  gl-form-page --> gl-facet
  style gl-facet fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
