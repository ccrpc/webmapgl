export const phrases = {
  "webmapgl": {
    "attribute-values": {
      "header": "Característica"
    },
    "address-search": {
      "prompt": "Buscar por dirección o nombre de lugar…"
    },
    "app": {
      "menulabel": "Leyenda"
    },
    "basemap-switcher": {
      "header": "Seleccione un mapa base"
    },
    "basemaps": {
      "title": "Cambie mapa base"
    },
    "draw-toolbar": {
      "cancel": "Cancelar",
      "confirm": "Continuar",
      "label": "Agrega una característica"
    },
    "drawer-toggle": {
      "label": "Cambia de opcion"
    },
    "download-button":{
      "title": "Descargar archivos"
    },
    "download-list":{
      "header": "Seleccione archivos para descargar",
      "download": "Descargar %{file}"
    },
    "feature-add": {
      "success": "Guardado exitosamente.",
      "failure": "Se produjo un error al guardar.",
      "label": "Agrega una característica"
    },
    "feature-list": {
      "loading": "Cargando..."
    },
    "field": {
      "required": "%{field} es requerido."
    },
    "form-page": {
      "back": "Atrás",
      "submit": "Salvar",
      "cancel": "Cancelar"
    },
    "fullscreen": {
      "enter": "Ingresa al modo de pantalla completa",
      "exit": "Sal del modo de pantalla completa"
    },
    "share": {
      "title": "Comparte este Mapa"
    },
    "share-button": {
      "title": "Compartir"
    },
    "table": {
      "header": {
        "sort-action": {
          "asc": "activar para ordenar la columna descendente",
          "desc": "activar para ordenar la columna ascendente"
        }
      }
    },
    "table-column-visibility": {
      "close": "Cerrar la visibilidad de columnas",
      "label": "Visibilidad de Columnas"
    }
  }
};
