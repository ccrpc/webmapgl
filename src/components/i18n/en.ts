export const phrases = {
  "webmapgl": {
    "attribute-values": {
      "header": "Feature"
    },
    "address-search": {
      "prompt": "Search by address or place name…"
    },
    "app": {
      "menulabel": "Legend"
    },
    "basemap-switcher": {
      "header": "Select a Basemap"
    },
    "basemaps": {
      "title": "Change basemap"
    },
    "draw-toolbar": {
      "cancel": "Cancel",
      "confirm": "Continue",
      "label": "Add a Feature"
    },
    "drawer-toggle": {
      "label": "Toggle drawer"
    },
    "download-button":{
      "title": "Download files",
    },
    'download-list':{
      "header": "Select Files to Download",
      "download": "Download %{file}"
    },
    "feature-add": {
      "success": "Saved successfully.",
      "failure": "An error occurred while saving.",
      "label": "Add a Feature"
    },
    "feature-list": {
      "loading": "Loading..."
    },
    "field": {
      "required": "%{field} is required."
    },
    "form-page": {
      "back": "Back",
      "submit": "Save",
      "cancel": "Cancel"
    },
    "fullscreen": {
      "enter": "Enter fullscreen mode",
      "exit": "Exit fullscreen mode"
    },
    "share": {
      "title": "Share this Map"
    },
    "share-button": {
      "title": "Share"
    },
    "story-button": {
      "first": "First",
      "previous": "Previous",
      "next": "Next"
    },
    "table": {
      "header": {
        "sort-action": {
          "asc": "activate to sort column descending",
          "desc": "activate to sort column ascending"
        }
      }
    },
    "table-column-visibility": {
      "close": "Close column visibility",
      "label": "Column Visibility"
    }
  }
};
