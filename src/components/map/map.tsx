import {
  Component,
  Element,
  Event,
  EventEmitter,
  Listen,
  Method,
  Prop,
  Watch,
} from "@stencil/core";
import { getAbsoluteUrl } from "../utils";
import maplibregl, {
  LayerSpecification,
  MapGeoJSONFeature,
  StyleSpecification,
} from "maplibre-gl";
import MapboxDraw from "@mapbox/mapbox-gl-draw";

// We do the following as a hack to get compatibility with Maplibre-gl 3.3.0 and mapbox-draw-gl
// TODO: When compatibility for maplibre 3.x is figured out, remove this and update: https://github.com/mapbox/mapbox-gl-draw/pull/1184
// @ts-ignore
MapboxDraw.constants.classes.CONTROL_BASE = "maplibregl-ctrl";
// @ts-ignore
MapboxDraw.constants.classes.CONTROL_PREFIX = "maplibregl-ctrl-";
// @ts-ignore
MapboxDraw.constants.classes.CONTROL_GROUP = "maplibregl-ctrl-group";
// @ts-ignore
MapboxDraw.constants.classes.ATTRIBUTION = "maplibregl-ctrl-attrib";

@Component({
  tag: "gl-map",
  styleUrl: "map.css",
})
export class Map {
  @Element() el: HTMLGlMapElement;

  /**
   * Emitted when a new feature is created in a drawing session.
   */
  @Event() glDrawCreate: EventEmitter;

  /**
   * Emitted when a feature is deleted in a drawing session.
   */
  @Event() glDrawDelete: EventEmitter;

  /**
   * Emitted when a draw session starts.
   */
  @Event() glDrawEnter: EventEmitter;

  /**
   * Emitted when a draw session ends.
   */
  @Event() glDrawExit: EventEmitter;

  /**
   * Emitted after the map style has been updated.
   */
  @Event() glStyleUpdated: EventEmitter;

  /**
   * Emitted after one or more features has been clicked
   */
  @Event() glClickedFeaturesGathered: EventEmitter;

  /**
   * Webmapgl uses the mapbox-gl-draw plugin for drawing features.
   * This property is a reference to the `MapboxDraw` instance when a draw session is active.
   */
  @Prop({ mutable: true }) draw: MapboxDraw;

  /**
   * Options object used to initialize `MapboxDraw`.
   */
  @Prop() readonly drawOptions?: MapboxDraw.MapboxDrawOptions;

  /**
   * There is an active draw session. It is usually set by `gl-draw-controller`.
   */
  @Prop() readonly drawing: boolean = false;

  /**
   * Default glyphs URL. It can be overridden by child `gl-style` components.
   */
  @Prop() readonly glyphs: string;

  /**
   * Store the map location and zoom level in the URL hash. If the value is
   * a string, that value will be used as the key in the URL hash.
   */
  @Prop() readonly hash: string | boolean = false;

  /**
   * Initial center latitude of the map.
   */
  @Prop() readonly latitude: number;

  /**
   * Initial center longitude of the map.
   */
  @Prop() readonly longitude: number;

  /**
   * Initial zoom level of the map.
   */
  @Prop() readonly zoom: number = 10;

  /**
   * Minimum zoom level.
   */
  @Prop() readonly minzoom: number = 0;

  /**
   * Maximum zoom level.
   */
  @Prop() readonly maxzoom: number = 22;

  /**
   * Default sprite URL. It can be overridden by child `gl-style` components.
   */
  @Prop() readonly sprite: string;

  /**
   * Reference to the `maplibregl.Map` object. It can be used to customize
   * map properties and behaviors beyond those exposed by `gl-map`.
   */
  @Prop({ mutable: true }) map: maplibregl.Map;

  /**
   * How many, and what, feature to return should the user click on the map
   */
  @Prop() readonly clickMode: "all" | "top-layer" | "top-feature" = "top-layer";

  /**
   * Milliseconds to wait before opening the popup. This delay allows for
   * handling multiple feature click events in a single popup. Multiple events
   * can be fired in quick succession when the user clicks on multiple
   * overlapping features.
   */
  @Prop() readonly clickEventDelay: number = 100;

  private _features: MapGeoJSONFeature[] = [];
  private _clickEventTimeout: number;
  private _resizeMapTimeout: number;
  private _style: StyleSpecification;
  private _updateStyleTimeout: number;

  componentWillLoad() {
    this._style = this.loadStyle();
    this.map = new maplibregl.Map({
      container: this.el,
      center: [this.longitude, this.latitude],
      hash: this.hash === "true" ? true : this.hash,
      zoom: this.zoom,
      minZoom: this.minzoom,
      maxZoom: this.maxzoom,
      style: this._style,
    });
    window.addEventListener("resize", this.resizeMap.bind(this));
    this.map.on("draw.create", (e) =>
      this.glDrawCreate.emit({
        mapId: this.el.id,
        features: e.features,
      })
    );
    this.map.on("draw.delete", (e) =>
      this.glDrawDelete.emit({
        mapId: this.el.id,
        features: e.features,
      })
    );
  }

  componentDidUpdate() {
    this.resizeMap();
  }

  private async updateStyle() {
    if (this._updateStyleTimeout == undefined) {
      this._updateStyleTimeout = window.setTimeout(async () => {
        this._updateStyleTimeout = null;
        this._style = await this.loadStyle();
        this.map.setStyle(this._style);
        this.glStyleUpdated.emit(this._style);
      }, 66);
    }
  }

  @Listen("glFeatureClick")
  handleFeatureClick(e: CustomEvent<{ features: MapGeoJSONFeature[] }>) {
    let features = e.detail.features;
    if (features.length === 0) return;

    Array.prototype.push.apply(this._features, features);
    if (this.clickEventDelay != undefined && this.clickEventDelay !== 0) {
      if (this._clickEventTimeout == undefined) {
        this._clickEventTimeout = window.setTimeout(async () => {
          this._clickEventTimeout = null;
          if (this._features.length !== 0) {
            this.glClickedFeaturesGathered.emit({
              features: this.filterFeatureClicks(this._features),
            });
          }
          this._features = [];
        }, this.clickEventDelay);
      }
    } else {
      this.glClickedFeaturesGathered.emit({
        features: this.filterFeatureClicks(this._features),
      });
    }
  }

  @Listen("glStyleElementAdded")
  async handleStyleAdded() {
    this.updateStyle();
  }

  @Listen("glStyleElementModified")
  async handleStyleModified() {
    this.updateStyle();
  }

  @Listen("glStyleElementRemoved")
  async handleStyleRemoved() {
    this.updateStyle();
  }

  @Watch("drawing")
  async updateDrawing() {
    if (this.drawing) {
      this.draw = new MapboxDraw(this.drawOptions);
      // @ts-ignore TODO: Find a better alternative to ignoring the type conversion.
      this.map.addControl(this.draw);
      this.glDrawEnter.emit({
        mapId: this.el.id,
      });
    } else {
      // @ts-ignore
      this.map.removeControl(this.draw);
      this.draw = null;
      this.glDrawExit.emit({
        mapId: this.el.id,
      });
    }
  }

  /**
   * Informs Mapbox GL that the map canvas has been resized. It uses a
   * timeout and can be called repeatedly (e.g., in an event handler) without
   * a performance penalty. For a synchronous resize, use `.map.resize()`.
   */
  @Method()
  async resizeMap() {
    if (this._resizeMapTimeout == undefined) {
      this._resizeMapTimeout = window.setTimeout(async () => {
        this._resizeMapTimeout = null;
        if (this.map != undefined) this.map.resize();
      }, 66);
    }
  }

  private getStyleLayers(
    styleId: string,
    json: StyleSpecification
  ): LayerSpecification[] {
    if (json.layers == undefined) return [];
    let results: LayerSpecification[] = [];
    for (let layer of json.layers) {
      let result: LayerSpecification = {
        ...layer,
        id: this.prefix(styleId, layer.id),
      };
      if (
        "source" in layer &&
        layer.source != undefined &&
        typeof layer.source === "string"
      )
        result["source"] = this.prefix(styleId, layer.source);
      results.push(result);
    }
    return results;
  }

  private getStyleSources(styleId: string, json: StyleSpecification) {
    if (json.sources == undefined) return [];
    let sources = {};
    for (let sourceName in json.sources)
      sources[this.prefix(styleId, sourceName)] = json.sources[sourceName];
    return sources;
  }

  private loadStyle() {
    let style: StyleSpecification = {
      version: 8,
      metadata: {},
      sources: {},
      layers: [],
    };

    if (this.glyphs != undefined) style.glyphs = this.glyphs;
    if (this.sprite != undefined) style.sprite = this.sprite;

    Array.from(this.el.querySelectorAll("gl-style")).forEach((styleEl) => {
      if (!styleEl.enabled) return;

      let id = styleEl.id;
      let json = styleEl.json;
      if (json == undefined) return;

      let styleLayers = this.getStyleLayers(id, json);
      if (
        styleEl.insertIntoStyle != undefined &&
        styleEl.insertBeneathLayer != undefined
      ) {
        const insertIntoIdx = style.layers
          .map((sl) => sl.id)
          .indexOf(`${styleEl.insertIntoStyle}:${styleEl.insertBeneathLayer}`);
        style.layers.splice(insertIntoIdx, 0, ...styleLayers);
      } else {
        style.layers = styleLayers.concat(style.layers);
      }
      style.sources = {
        ...style.sources,
        ...this.getStyleSources(id, json),
      };

      // TODO: Deal with multiple styles that each have their own glyphs
      // or sprites. For now, we use the explicitly set URL on gl-map,
      // or fall back to the first URL found in the child styles. See:
      // https://github.com/mapbox/mapbox-gl-js/issues/4086
      // https://github.com/mapbox/mapbox-gl-js/issues/4000
      if (json.glyphs && style.glyphs == undefined) style.glyphs = json.glyphs;
      if (json.sprite && style.sprite == undefined) style.sprite = json.sprite;
    });

    if (style.glyphs != undefined)
      style.glyphs = getAbsoluteUrl(style.glyphs)
        .replace("%7Bfontstack%7D", "{fontstack}")
        .replace("%7Brange%7D", "{range}");
    if (style.sprite != undefined) {
      if (typeof style.sprite === "string") {
        style.sprite = getAbsoluteUrl(style.sprite);
      } else if (style.sprite.length !== 0) {
        style.sprite.forEach((sprite) => {
          sprite.url = getAbsoluteUrl(sprite.url);
        });
      }
    }

    return style;
  }

  private prefix(styleId: string, value: string) {
    return `${styleId}:${value}`;
  }

  private filterTopLayer(features: MapGeoJSONFeature[]): MapGeoJSONFeature[] {
    let layerName = features[features.length - 1].layer.id;
    return features.filter((feature) => feature.layer.id === layerName);
  }

  private filterTopFeature(features: MapGeoJSONFeature[]): MapGeoJSONFeature[] {
    return [features[features.length - 1]];
  }

  private filterFeatureClicks(
    features: MapGeoJSONFeature[]
  ): MapGeoJSONFeature[] {
    switch (this.clickMode) {
      case "all":
        return features;
      case "top-feature":
        return this.filterTopFeature(features);
      case "top-layer":
        return this.filterTopLayer(features);
      default:
        throw new Error("Popup filtering error");
    }
  }
}
