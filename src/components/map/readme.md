# gl-map

The map component creates a map using Mapbox GL JS. It should contain at least
one child [`gl-style`](../style) component to provide the map style.

## Dependencies

Unlike previous versions of webmapgl, the current version is bundled with both maplibre-gl-js and mapbox-gl-draw and performs some work behind the scenes to keep the two compatible.

To include the css configurations for Maplibre-gl and Mapbox-draw-gl include the following in your global css file.
Without this your application will not display properly.

```css
@import "~@ccrpc/webmapgl/dist/webmapgl/webmapgl.css";
```

<!-- Auto Generated Below -->


## Properties

| Property          | Attribute           | Description                                                                                                                                                                                                                            | Type                                    | Default       |
| ----------------- | ------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------- | ------------- |
| `clickEventDelay` | `click-event-delay` | Milliseconds to wait before opening the popup. This delay allows for handling multiple feature click events in a single popup. Multiple events can be fired in quick succession when the user clicks on multiple overlapping features. | `number`                                | `100`         |
| `clickMode`       | `click-mode`        | How many, and what, feature to return should the user click on the map                                                                                                                                                                 | `"all" \| "top-feature" \| "top-layer"` | `"top-layer"` |
| `draw`            | --                  | Webmapgl uses the mapbox-gl-draw plugin for drawing features. This property is a reference to the `MapboxDraw` instance when a draw session is active.                                                                                 | `MapboxDraw`                            | `undefined`   |
| `drawOptions`     | --                  | Options object used to initialize `MapboxDraw`.                                                                                                                                                                                        | `MapboxDrawOptions`                     | `undefined`   |
| `drawing`         | `drawing`           | There is an active draw session. It is usually set by `gl-draw-controller`.                                                                                                                                                            | `boolean`                               | `false`       |
| `glyphs`          | `glyphs`            | Default glyphs URL. It can be overridden by child `gl-style` components.                                                                                                                                                               | `string`                                | `undefined`   |
| `hash`            | `hash`              | Store the map location and zoom level in the URL hash. If the value is a string, that value will be used as the key in the URL hash.                                                                                                   | `boolean \| string`                     | `false`       |
| `latitude`        | `latitude`          | Initial center latitude of the map.                                                                                                                                                                                                    | `number`                                | `undefined`   |
| `longitude`       | `longitude`         | Initial center longitude of the map.                                                                                                                                                                                                   | `number`                                | `undefined`   |
| `map`             | --                  | Reference to the `maplibregl.Map` object. It can be used to customize map properties and behaviors beyond those exposed by `gl-map`.                                                                                                   | `Map`                                   | `undefined`   |
| `maxzoom`         | `maxzoom`           | Maximum zoom level.                                                                                                                                                                                                                    | `number`                                | `22`          |
| `minzoom`         | `minzoom`           | Minimum zoom level.                                                                                                                                                                                                                    | `number`                                | `0`           |
| `sprite`          | `sprite`            | Default sprite URL. It can be overridden by child `gl-style` components.                                                                                                                                                               | `string`                                | `undefined`   |
| `zoom`            | `zoom`              | Initial zoom level of the map.                                                                                                                                                                                                         | `number`                                | `10`          |


## Events

| Event                       | Description                                                 | Type               |
| --------------------------- | ----------------------------------------------------------- | ------------------ |
| `glClickedFeaturesGathered` | Emitted after one or more features has been clicked         | `CustomEvent<any>` |
| `glDrawCreate`              | Emitted when a new feature is created in a drawing session. | `CustomEvent<any>` |
| `glDrawDelete`              | Emitted when a feature is deleted in a drawing session.     | `CustomEvent<any>` |
| `glDrawEnter`               | Emitted when a draw session starts.                         | `CustomEvent<any>` |
| `glDrawExit`                | Emitted when a draw session ends.                           | `CustomEvent<any>` |
| `glStyleUpdated`            | Emitted after the map style has been updated.               | `CustomEvent<any>` |


## Methods

### `resizeMap() => Promise<void>`

Informs Mapbox GL that the map canvas has been resized. It uses a
timeout and can be called repeatedly (e.g., in an event handler) without
a performance penalty. For a synchronous resize, use `.map.resize()`.

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-map --> ion-thumbnail
  style gl-map fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
