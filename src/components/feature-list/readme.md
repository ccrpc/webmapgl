# gl-feature-list

The feature list component creates an `ion-list` containing features from
a source included in a [`gl-style`](../style). It can display all features from
the source, or just those currently within the map extent. It is currently
compatible with GeoJSON sources containing point features.

<!-- Auto Generated Below -->


## Properties

| Property           | Attribute         | Description                                                                                                         | Type                                                   | Default                               |
| ------------------ | ----------------- | ------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------- |
| `batchSize`        | `batch-size`      | The number of features to render in each batch. Additional batches are added as needed using `ion-infinite-scroll`. | `number`                                               | `20`                                  |
| `component`        | `component`       | The component used to render the features in the list.                                                              | `string`                                               | `undefined`                           |
| `componentOptions` | --                | Options passed to the component as properties.                                                                      | `{ [key: string]: any; }`                              | `undefined`                           |
| `display`          | `display`         | Which features to display: all features from the source, or just those currently within the map extent.             | `"all" \| "visible"`                                   | `"visible"`                           |
| `features`         | --                | An array of GeoJSON features to display. Currently only point features are supported.                               | `Feature<limitedGeometry, { [name: string]: any; }>[]` | `[]`                                  |
| `item`             | `item`            | Wrap the component in an `ion-item`.                                                                                | `boolean`                                              | `true`                                |
| `loadingSpinner`   | `loading-spinner` | The type of spinner used in the `ion-infinite-scroll-content`.                                                      | `string`                                               | `"bubbles"`                           |
| `loadingText`      | `loading-text`    | Text to display while features are loading in the `ion-infinite-scroll-content`.                                    | `string`                                               | `_t("webmapgl.feature-list.loading")` |
| `mapId`            | `map-id`          | ID of the `gl-map`.                                                                                                 | `string`                                               | `undefined`                           |
| `order`            | `order`           | Order in which to sort features.                                                                                    | `"asc" \| "desc" \| "none"`                            | `"asc"`                               |
| `orderBy`          | `order-by`        | Property by which features should be sorted.                                                                        | `string`                                               | `undefined`                           |
| `sourceId`         | `source-id`       | ID of the source containing the features.                                                                           | `string`                                               | `undefined`                           |
| `styleId`          | `style-id`        | ID of the `gl-style` element containing the feature source.                                                         | `string`                                               | `undefined`                           |


## Dependencies

### Depends on

- ion-item
- ion-list
- ion-infinite-scroll
- ion-infinite-scroll-content
- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-feature-list --> ion-item
  gl-feature-list --> ion-list
  gl-feature-list --> ion-infinite-scroll
  gl-feature-list --> ion-infinite-scroll-content
  gl-feature-list --> ion-thumbnail
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-infinite-scroll-content --> ion-spinner
  style gl-feature-list fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
