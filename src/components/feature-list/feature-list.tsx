import { h, Component, Listen, Prop, State, Watch, Host } from "@stencil/core";
import { _t } from "../i18n/i18n";
import { getMap } from "../utils";
import { InfiniteScrollCustomEvent } from "@ionic/core";
import { styleEvent } from "../style/style";
import {
  FeatureCollection,
  Feature,
  Point,
  MultiPoint,
  LineString,
  MultiLineString,
  Polygon,
  MultiPolygon,
} from "geojson";
import { GeoJSONSourceSpecification } from "maplibre-gl";

export type limitedGeometry =
  | Point
  | MultiPoint
  | LineString
  | MultiLineString
  | Polygon
  | MultiPolygon;

@Component({
  tag: "gl-feature-list",
})
export class FeatureList {
  @State() data: FeatureCollection<limitedGeometry>;
  @State() limit: number;

  /**
   * The number of features to render in each batch. Additional batches
   * are added as needed using `ion-infinite-scroll`.
   */
  @Prop() readonly batchSize: number = 20;

  /**
   * The component used to render the features in the list.
   */
  @Prop() readonly component: string;

  /**
   * Options passed to the component as properties.
   */
  @Prop() readonly componentOptions: { [key: string]: any };

  /**
   * Which features to display: all features from the source, or just those
   * currently within the map extent.
   */
  @Prop() readonly display: "all" | "visible" = "visible";

  /**
   * An array of GeoJSON features to display. Currently only point features
   * are supported.
   */
  @Prop({ mutable: true }) features: Array<Feature<limitedGeometry>> = [];

  /**
   * Wrap the component in an `ion-item`.
   */
  @Prop() readonly item: boolean = true;

  /**
   * The type of spinner used in the `ion-infinite-scroll-content`.
   */
  @Prop() readonly loadingSpinner: string = "bubbles";

  /**
   * Text to display while features are loading in the
   * `ion-infinite-scroll-content`.
   */
  @Prop() readonly loadingText: string = _t("webmapgl.feature-list.loading");

  /**
   * ID of the `gl-map`.
   */
  @Prop() readonly mapId: string;

  /**
   * Property by which features should be sorted.
   */
  @Prop() readonly orderBy: string;

  /**
   * Order in which to sort features.
   */
  @Prop() readonly order: "asc" | "desc" | "none" = "asc";

  /**
   * ID of the `gl-style` element containing the feature source.
   */
  @Prop() readonly styleId: string;

  /**
   * ID of the source containing the features.
   */
  @Prop() readonly sourceId: string;

  async componentWillLoad() {
    await this.handleSourceId();
  }

  componentDidLoad() {
    const map = getMap(this.mapId).map;
    if (map != undefined) map.on("moveend", async () => await this.filter());
  }

  @Listen("ionInfinite")
  handleInfiniteScroll(e: InfiniteScrollCustomEvent) {
    this.limit += this.batchSize;
    e.target.complete();
  }

  @Listen("glStyleElementModified", { target: "body" })
  handleStyleUpdated(e: CustomEvent<styleEvent>) {
    if (e.detail.id === this.styleId) this.handleSourceId();
  }

  @Watch("styleId")
  @Watch("sourceId")
  async handleSourceId() {
    const style: HTMLGlStyleElement = document.querySelector(
      `#${this.styleId}`
    );
    if (style == undefined || style.json.sources == undefined) return;
    const data = (
      style.json.sources[this.sourceId] as GeoJSONSourceSpecification
    ).data as FeatureCollection<limitedGeometry> | string;

    if (typeof data === "string") {
      let res = await fetch(data);
      this.data = (await res.json()) as FeatureCollection<limitedGeometry>;
    } else {
      this.data = data;
    }
    this.filter();
  }

  private async filter() {
    if (this.data?.features == undefined || this.data.features.length === 0)
      return;

    let features: Array<Feature<limitedGeometry>>;
    if (this.display === "all") {
      features = this.data.features;
    } else {
      let bounds = getMap(this.mapId).map.getBounds();
      features = this.data.features.filter((feature) => {
        let [lng, lat] = feature.geometry.coordinates;
        return (
          (lng as number) >= bounds.getWest() &&
          (lng as number) <= bounds.getEast() &&
          (lat as number) >= bounds.getSouth() &&
          (lat as number) <= bounds.getNorth()
        );
      });
    }

    this.limit = this.batchSize;
    this.features = this.sort(features);
  }

  private sort(features: Array<Feature<limitedGeometry>>) {
    if (this.order === "none" || this.orderBy == undefined) return features;

    features.sort((aFeature, bFeature) => {
      const a = aFeature.properties[this.orderBy];
      const b = bFeature.properties[this.orderBy];
      if (a < b) return this.order === "asc" ? -1 : 1;
      if (a > b) return this.order === "asc" ? 1 : -1;
      return 0;
    });

    return features;
  }

  render() {
    let ComponentTag = this.component;
    let features =
      this.limit !== 0 ? this.features.slice(0, this.limit) : this.features;
    let hasMore = features.length < this.features.length;

    let items = features.map((feature) => {
      let component = (
        <ComponentTag
          feature={feature}
          {...this.componentOptions}
        ></ComponentTag>
      );
      return this.item ? <ion-item>{component}</ion-item> : component;
    });

    return (
      <Host>
        <ion-list>
          <slot name="start" />
          {items}
          <slot name="end" />
        </ion-list>
        <ion-infinite-scroll disabled={!hasMore}>
          <ion-infinite-scroll-content
            loading-spinner={this.loadingSpinner}
            loading-text={this.loadingText}
          ></ion-infinite-scroll-content>
        </ion-infinite-scroll>
      </Host>
    );
  }
}
