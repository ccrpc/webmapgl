# gl-form

The form component renders an interactive form based on a JSON schema
containing facets and fields. The form is used to set the properties of
a GeoJSON feature.

<!-- Auto Generated Below -->


## Properties

| Property        | Attribute        | Description                                                                                             | Type                                       | Default                 |
| --------------- | ---------------- | ------------------------------------------------------------------------------------------------------- | ------------------------------------------ | ----------------------- |
| `cancelText`    | `cancel-text`    | Text for the cancel button.                                                                             | `string`                                   | `undefined`             |
| `feature`       | --               | The feature being modified by the form. The form sets values in the 'properties' object of the feature. | `{ properties: { [key: string]: any; }; }` | `undefined`             |
| `formId`        | `form-id`        | ID of the form.                                                                                         | `string`                                   | ``gl-form-${formId++}`` |
| `label`         | `label`          | The label displayed in the toolbar of the `gl-form-page`.                                               | `string`                                   | `undefined`             |
| `schema`        | `schema`         | The URL of the form schema JSON.                                                                        | `string`                                   | `undefined`             |
| `submitText`    | `submit-text`    | Text for the submit button.                                                                             | `string`                                   | `undefined`             |
| `translateText` | `translate-text` | Treat labels in the form as i18n keys.                                                                  | `boolean`                                  | `false`                 |


## Events

| Event                  | Description                                                                  | Type               |
| ---------------------- | ---------------------------------------------------------------------------- | ------------------ |
| `glFormCancel`         | Emitted when the user cancels a form submission.                             | `CustomEvent<any>` |
| `glFormFeatureChanged` | Emitted when the feature properties are updated due to a field value change. | `CustomEvent<any>` |
| `glFormSubmit`         | Emitted when the user submits the form.                                      | `CustomEvent<any>` |


## Methods

### `cancel() => Promise<void>`

Cancels and closes the form

#### Returns

Type: `Promise<void>`



### `submit() => Promise<void>`

Submits the form and passes the feature that has been edited along to glFormSubmit

#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [gl-form-controller](../form-controller)

### Depends on

- [gl-form-page](../form-page)
- ion-nav

### Graph
```mermaid
graph TD;
  gl-form --> gl-form-page
  gl-form --> ion-nav
  gl-form-page --> gl-facet
  gl-form-page --> gl-field
  gl-form-page --> gl-option
  gl-form-page --> ion-header
  gl-form-page --> ion-toolbar
  gl-form-page --> ion-buttons
  gl-form-page --> ion-button
  gl-form-page --> ion-icon
  gl-form-page --> ion-title
  gl-form-page --> ion-content
  gl-form-page --> ion-list
  gl-facet --> ion-list-header
  gl-facet --> ion-item
  gl-facet --> ion-label
  gl-facet --> ion-thumbnail
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  gl-field --> ion-list-header
  gl-field --> ion-radio-group
  gl-field --> ion-item
  gl-field --> ion-label
  gl-field --> ion-select
  gl-field --> ion-textarea
  gl-field --> ion-input
  ion-select --> ion-select-popover
  ion-select --> ion-popover
  ion-select --> ion-action-sheet
  ion-select --> ion-alert
  ion-select --> ion-icon
  ion-select-popover --> ion-item
  ion-select-popover --> ion-checkbox
  ion-select-popover --> ion-label
  ion-select-popover --> ion-radio-group
  ion-select-popover --> ion-radio
  ion-select-popover --> ion-list
  ion-select-popover --> ion-list-header
  ion-popover --> ion-backdrop
  ion-action-sheet --> ion-backdrop
  ion-action-sheet --> ion-icon
  ion-action-sheet --> ion-ripple-effect
  ion-alert --> ion-ripple-effect
  ion-alert --> ion-backdrop
  ion-input --> ion-icon
  gl-option --> ion-item
  gl-option --> ion-radio
  gl-option --> ion-label
  gl-option --> ion-select-option
  gl-option --> ion-thumbnail
  ion-button --> ion-ripple-effect
  gl-form-controller --> gl-form
  style gl-form fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
