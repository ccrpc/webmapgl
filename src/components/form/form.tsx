import {
  h,
  Component,
  Element,
  Event,
  EventEmitter,
  Listen,
  Method,
  Prop,
} from "@stencil/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-form",
})
export class Form {
  private _schema: any;

  @Element() el: HTMLGlFormElement;

  /**
   * Emitted when the user cancels a form submission.
   */
  @Event() glFormCancel: EventEmitter;

  /**
   * Emitted when the feature properties are updated due to a field value
   * change.
   */
  @Event() glFormFeatureChanged: EventEmitter;

  /**
   * Emitted when the user submits the form.
   */
  @Event() glFormSubmit: EventEmitter;

  /**
   * Text for the cancel button.
   */
  @Prop() readonly cancelText: string;

  /**
   * The feature being modified by the form.
   * The form sets values in the 'properties' object of the feature.
   */
  @Prop() readonly feature: { properties: { [key: string]: any } };

  /**
   * ID of the form.
   */
  @Prop() readonly formId: string = `gl-form-${formId++}`;

  /**
   * The label displayed in the toolbar of the `gl-form-page`.
   */
  @Prop() readonly label: string;

  /**
   * The URL of the form schema JSON.
   */
  @Prop() readonly schema: string;

  /**
   * Text for the submit button.
   */
  @Prop() readonly submitText: string;

  /**
   * Treat labels in the form as i18n keys.
   */
  @Prop() readonly translateText: boolean = false;

  async componentWillLoad() {
    let res = await fetch(this.schema);
    this._schema = await res.json();
  }

  componentDidLoad() {
    let page = this.createFormPage(this.label);
    this.el.querySelector("ion-nav").setRoot(page);
  }

  @Listen("glFieldValueChanged")
  setValue(e: CustomEvent) {
    if (this.feature == undefined) return;
    this.feature.properties = this.feature.properties || {};
    this.feature.properties[e.detail.field.attribute] = e.detail.value;
    this.glFormFeatureChanged.emit({
      formId: this.formId,
      feature: this.feature,
    });
  }

  @Listen("glFormFacet")
  handleFacet(e: CustomEvent) {
    let page = this.createFormPage(e.detail.label, e.detail.value);
    this.el.querySelector("ion-nav").push(page);
  }

  /**
   * Cancels and closes the form
   */
  @Method()
  async cancel() {
    this.glFormCancel.emit({
      formId: this.formId,
      feature: this.feature,
    });
  }

  /**
   * Submits the form and passes the feature that has been edited along to glFormSubmit
   */
  @Method()
  async submit() {
    this.glFormSubmit.emit({
      formId: this.formId,
      feature: this.feature,
    });
  }

  private createFormPage(label?: string, formFacet?: string) {
    let page = document.createElement("gl-form-page");
    page.formFacet = formFacet;
    page.facets = this.filter(this._schema.facets || [], formFacet);
    page.fields = this.filter(this._schema.fields || [], formFacet);
    page.root = formFacet != undefined;

    if (label != undefined) page.label = label;
    if (this.submitText != undefined) page.submitText = this.submitText;
    if (this.cancelText != undefined) page.cancelText = this.cancelText;

    return page;
  }

  private filter(items: any[], formFacet?: string) {
    return items
      .filter((item) => {
        let facets = item.facets || [];
        if (!facets.length && formFacet == undefined) return true;
        for (let facet of facets) {
          let escaped = facet.replace(/[.?+^$[\]\\(){}|-]/g, "\\$&");
          let re = new RegExp("^" + escaped.split("*").join(".*") + "$");
          if (re.test(formFacet)) return true;
        }
        return false;
      })
      .map((item) => {
        let result = { ...item };
        if (this.translateText) result.label = _t(item.label);
        if (item.options) result.options = this.filter(item.options, formFacet);
        return result;
      });
  }

  render() {
    return <ion-nav></ion-nav>;
  }
}

let formId = 0;
