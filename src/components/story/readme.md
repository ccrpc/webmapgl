# gl-story

A story is, in essence, a series of clicking on features.

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                          | Type     | Default     |
| -------- | --------- | ---------------------------------------------------- | -------- | ----------- |
| `step`   | `step`    | What 'step' of the story is currently the active one | `number` | `undefined` |
| `steps`  | `steps`   | The total number of steps in the story               | `number` | `undefined` |


## Events

| Event           | Description                                                 | Type               |
| --------------- | ----------------------------------------------------------- | ------------------ |
| `glStoryChange` | Event emitted when the number of steps in the story changes | `CustomEvent<any>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
