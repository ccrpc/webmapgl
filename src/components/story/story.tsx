import {
  h,
  Component,
  Element,
  Event,
  EventEmitter,
  Prop,
  Watch,
} from "@stencil/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "gl-story",
})
export class Story {
  @Element() el: HTMLGlStoryElement;

  /**
   * Event emitted when the number of steps in the story changes
   */
  @Event() glStoryChange: EventEmitter;

  /**
   * What 'step' of the story is currently the active one
   */
  @Prop() readonly step: number;

  /**
   * The total number of steps in the story
   */
  @Prop({ mutable: true }) steps: number;

  componentWillLoad() {
    this.updateSteps();
  }

  @Watch("step")
  updateSteps() {
    const steps = this.el.querySelectorAll("gl-story-step");
    this.steps = steps.length;
    Array.from(steps).forEach((step, i) => {
      step.active = i === this.step;
      step.first = i === 0;
      step.last = i === steps.length - 1;
    });
    this.glStoryChange.emit(this);
  }

  render() {
    return <slot />;
  }
}
