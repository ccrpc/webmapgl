import { Component, Element, Listen, Method, Prop, Watch } from "@stencil/core";
import pointOnFeature from "@turf/point-on-feature";
import { featureCollection as makeFeatureCollection } from "@turf/helpers";
import { toArray } from "../utils";
import { Feature } from "geojson";
import { limitedGeometry } from "../feature-list/feature-list";
import { Popup as MaplibrePopup } from "maplibre-gl";

const openPopups: MaplibrePopup[] = [];

@Component({
  styleUrl: "popup.css",
  tag: "gl-popup",
})
export class Popup {
  private popup?: MaplibrePopup;

  @Element() el: HTMLGlPopupElement;

  /**
   * Milliseconds to wait before opening the popup. This delay allows for
   * handling multiple feature click events in a single popup. Multiple events
   * can be fired in quick succession when the user clicks on multiple
   * overlapping features.
   */
  @Prop() readonly clickEventDelay: number = 50;

  /**
   * Key code of the key used to close the popup. By default, it is the `Esc`
   * key.
   */
  @Prop() readonly closeKey: string = "Escape";

  /**
   * Which popups to close when a new popup is opened. `self` removes popups
   * opened by this `gl-popup` element, and `all` closes all open popups.
   */
  @Prop() readonly closeMode: "none" | "self" | "all" = "self";

  /**
   * Name of the component containing the content for the popup
   * (e.g., `gl-attribute-values`). If the popup content is dynamic, the
   * component should accept a `features` property, which will be set to the
   * GeoJSON features clicked by the user.
   */
  @Prop() readonly component: string;

  /**
   * Additional options, which are passed to the content component as
   * properties.
   */
  @Prop() readonly componentOptions: string | { [key: string]: any };

  /**
   * IDs of the layers that can activate this popup when clicked. These
   * layer IDs should also be included in the `clickableLayers` property of
   * the parent style.
   */
  @Prop() readonly layers: string[] | string;

  /**
   * Maximum width of the popup element. It can be any valid CSS width value.
   */
  @Prop() readonly maxWidth: string = "none";

  private _layers: string[];

  componentWillLoad() {
    this.parseLayersString();
  }

  private getOptions() {
    if (typeof this.componentOptions === "string") {
      return JSON.parse(this.componentOptions);
    }
    return this.componentOptions || {};
  }

  @Listen("glClickedFeaturesGathered", { target: "body" })
  doOpenPopup(e: CustomEvent) {
    let features = e.detail.features.filter((feature) => {
      let [styleId, layerId] = feature.layer.id.split(":");
      return (
        this._layers.indexOf(layerId) != -1 &&
        this.el.closest("gl-style").id === styleId
      );
    });
    if (features.length) this.openPopup(features);
  }

  @Listen("keyup", { target: "body" })
  handleKeyup(e: KeyboardEvent) {
    if (e.key === this.closeKey) this.removePopup();
  }

  /**
   * Returns a boolean indicating whether the popup is currently open.
   */
  @Method()
  async isOpen() {
    return this.popup != undefined ? this.popup.isOpen() : false;
  }

  /**
   * Removes the popup from the map.
   */
  @Method()
  async removePopup() {
    if (this.popup != undefined) {
      this.popup.remove();
      let popupIdx: number = openPopups.indexOf(this.popup);
      if (popupIdx > -1) openPopups.splice(popupIdx, 1);
      this.popup = null;
    }
  }

  private removeAll() {
    while (openPopups.length > 0) openPopups.pop().remove();
    this.popup = null;
  }

  private async openPopup(features: Feature<limitedGeometry>[]) {
    let featureCollection = makeFeatureCollection(features);
    let point = pointOnFeature(featureCollection);
    let component = document.createElement(this.component);
    component["features"] = features;
    let options = this.getOptions();
    for (let propName in this.getOptions())
      component[propName] = options[propName];

    if (this.closeMode === "self") await this.removePopup();
    if (this.closeMode === "all") this.removeAll();

    this.popup = new MaplibrePopup({ maxWidth: this.maxWidth })
      .setLngLat(point.geometry.coordinates as [number, number])
      .setDOMContent(component)
      .addTo(this.el.closest("gl-map").map);

    openPopups.push(this.popup);
  }

  @Watch("layers")
  parseLayersString() {
    if (typeof this.layers === "string") {
      this._layers = toArray(this.layers);
    } else {
      this._layers = this.layers;
    }
  }
}
