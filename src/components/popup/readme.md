# gl-popup

The popup component listens for a `glFeatureClick` event and opens a popup
using `mapboxgl.Popup`. It should be a child of [`gl-style`](../style).

The popup is opened if the layer that generated the click event is in
the parent `gl-style` element and matches one of the layers in the `layers`
property. The content of the popup is provided by the component specified
in the `component` property. If the popup content is dynamic, the component
should accept a `features` property, which will be set to the GeoJSON
features clicked by the user.


<!-- Auto Generated Below -->


## Properties

| Property           | Attribute           | Description                                                                                                                                                                                                                                  | Type                                | Default     |
| ------------------ | ------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------- | ----------- |
| `clickEventDelay`  | `click-event-delay` | Milliseconds to wait before opening the popup. This delay allows for handling multiple feature click events in a single popup. Multiple events can be fired in quick succession when the user clicks on multiple overlapping features.       | `number`                            | `50`        |
| `closeKey`         | `close-key`         | Key code of the key used to close the popup. By default, it is the `Esc` key.                                                                                                                                                                | `string`                            | `"Escape"`  |
| `closeMode`        | `close-mode`        | Which popups to close when a new popup is opened. `self` removes popups opened by this `gl-popup` element, and `all` closes all open popups.                                                                                                 | `"all" \| "none" \| "self"`         | `"self"`    |
| `component`        | `component`         | Name of the component containing the content for the popup (e.g., `gl-attribute-values`). If the popup content is dynamic, the component should accept a `features` property, which will be set to the GeoJSON features clicked by the user. | `string`                            | `undefined` |
| `componentOptions` | `component-options` | Additional options, which are passed to the content component as properties.                                                                                                                                                                 | `string \| { [key: string]: any; }` | `undefined` |
| `layers`           | `layers`            | IDs of the layers that can activate this popup when clicked. These layer IDs should also be included in the `clickableLayers` property of the parent style.                                                                                  | `string \| string[]`                | `undefined` |
| `maxWidth`         | `max-width`         | Maximum width of the popup element. It can be any valid CSS width value.                                                                                                                                                                     | `string`                            | `"none"`    |


## Methods

### `isOpen() => Promise<boolean>`

Returns a boolean indicating whether the popup is currently open.

#### Returns

Type: `Promise<boolean>`



### `removePopup() => Promise<void>`

Removes the popup from the map.

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-popup --> ion-thumbnail
  style gl-popup fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
