# gl-like-controller



<!-- Auto Generated Below -->


## Methods

### `create(feature: Feature | MapGeoJSONFeature, options?: LikeOptions) => Promise<LikeProxy>`

Creates a proxy to communicate likes

#### Returns

Type: `Promise<LikeProxy>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
