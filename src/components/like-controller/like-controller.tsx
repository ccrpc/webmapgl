import { Component, Method } from "@stencil/core";
import { LikeOptions } from "./interface";
import { LikeProxy } from "./like-proxy";
import { Feature } from "geojson";
import { MapGeoJSONFeature } from "maplibre-gl";
@Component({
  tag: "gl-like-controller",
})
export class LikeController {
  /**
   * Creates a proxy to communicate likes
   * @param {Feature | MapboxGeoJSONFeature} feature The feature whose likes to change. Can be either just a geojson Feature or a MapboxGeoJSONFeature with extra layer information
   * @param {LikeOptions} options Extra options for the like controller
   * @returns
   */
  @Method()
  async create(
    feature: Feature | MapGeoJSONFeature,
    options: LikeOptions = {}
  ): Promise<LikeProxy> {
    return new LikeProxy(feature, options);
  }
}
