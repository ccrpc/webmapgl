import { LikeOptions } from "./interface";
import { Feature } from "geojson";
import { MapGeoJSONFeature } from "maplibre-gl";
export class LikeProxy {
  /**
   * The feature used with the like proxy.
   * Can be either a normal geojson feature or a MapboxGeoJSONFeature which includes additional information about the layers.
   *
   * If a MapboxGeoJSONFeature is passed in then the layer id will be used in the key likes are stored with. Otherwise 'default' will be used.
   */
  feature: Feature | MapGeoJSONFeature;
  options: LikeOptions;

  constructor(feature: Feature | MapGeoJSONFeature, options: LikeOptions = {}) {
    this.feature = feature;
    if (options.keyPrefix == undefined) options.keyPrefix = "webmapgl.like";
    if (options.clientId == undefined) {
      options.clientId = localStorage.getItem(options.keyPrefix + ".clientId");
      if (options.clientId == null) {
        options.clientId = this.makeId(64);
        localStorage.setItem(options.keyPrefix + ".clientId", options.clientId);
      }
    }
    this.options = options;
  }

  private getLayerId(): string {
    return "layer" in this.feature && Boolean(this.feature.layer?.id)
      ? this.feature.layer.id
      : undefined;
  }

  public getClientId() {
    return this.options.clientId;
  }

  public isLiked(): boolean {
    let likes = this.getArray(this.getLayerId());
    return likes.indexOf(Number(this.feature.id)) !== -1;
  }

  public like() {
    let likes = this.getArray(this.getLayerId());
    if (likes.indexOf(Number(this.feature.id)) === -1)
      likes.push(Number(this.feature.id));
    this.setArray(this.getLayerId(), likes);
  }

  public unlike() {
    let likes = this.getArray(this.getLayerId());
    let idx = likes.indexOf(Number(this.feature.id));
    if (idx !== -1) likes.splice(idx, 1);
    this.setArray(this.getLayerId(), likes);
  }

  protected makeId(length: number) {
    var id = "";
    var chars =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" + "0123456789";

    for (let i = 0; i < length; i++)
      id += chars.charAt(Math.floor(Math.random() * chars.length));

    return id;
  }

  protected getLayerKey(layerId: string) {
    return (
      this.options.keyPrefix +
      "|" +
      document.location.pathname +
      "|" +
      (layerId || "default")
    );
  }

  protected getArray(layer: string) {
    let str = localStorage.getItem(this.getLayerKey(layer));
    if (str == null) return [];
    return str
      .split(",")
      .map((v) => parseInt(v))
      .filter((v) => !isNaN(v));
  }

  protected setArray(layer: string, values: number[]) {
    let str = values
      .filter((v) => !isNaN(v))
      .map((v) => v.toString())
      .join(",");
    localStorage.setItem(this.getLayerKey(layer), str);
  }
}
