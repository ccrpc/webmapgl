import {
  h,
  Component,
  Event,
  EventEmitter,
  Method,
  Prop,
  State,
  Watch,
  Host,
} from "@stencil/core";
import { getMap } from "../utils";

@Component({
  styleUrl: "drawer.css",
  tag: "gl-drawer",
})
export class Drawer {
  @State() content: string;

  /**
   * Title of the drawer displayed in the toolbar.
   */
  @Prop() readonly drawerTitle: string;

  /**
   * Map ID of the map that should be resized when the drawer is opened or closed.
   */
  @Prop() readonly mapId: string;

  /**
   * The drawer is currently open.
   */
  @Prop({ mutable: true }) open = false;

  @Watch("open")
  openChanged(isOpen: boolean) {
    this.glDrawerToggle.emit({
      open: isOpen,
    });
  }

  /**
   * Emitted when the drawer is opened or closed.
   */
  @Event() glDrawerToggle: EventEmitter;

  componentDidUpdate() {
    let map = getMap(this.mapId);
    map.resizeMap();
  }

  /**
   * Opens the drawer if it is closed and closes the drawer if it is open
   */
  @Method()
  async toggle() {
    this.open = !this.open;
  }

  private closeDrawerClick = () => (this.open = false);

  render() {
    return (
      <Host
        class={{
          "gl-drawer-closed": !this.open,
          "gl-drawer-open": this.open,
        }}
      >
        <ion-header>
          <ion-toolbar>
            <ion-buttons slot="start">
              <slot name="toolbar-start-buttons" />
            </ion-buttons>
            <ion-title>{this.drawerTitle}</ion-title>
            <ion-buttons slot="end">
              <slot name="toolbar-end-buttons" />
              <ion-button title="Close" onClick={this.closeDrawerClick}>
                <ion-icon slot="icon-only" name="close"></ion-icon>
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <slot />
        </ion-content>
      </Host>
    );
  }
}
