# gl-drawer

The drawer component provides a secondary content area where information
related to the map can be displayed. It can be opened or closed using the
[gl-drawer-toggle](../drawer-toggle) component.

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                                   | Type      | Default     |
| ------------- | -------------- | ----------------------------------------------------------------------------- | --------- | ----------- |
| `drawerTitle` | `drawer-title` | Title of the drawer displayed in the toolbar.                                 | `string`  | `undefined` |
| `mapId`       | `map-id`       | Map ID of the map that should be resized when the drawer is opened or closed. | `string`  | `undefined` |
| `open`        | `open`         | The drawer is currently open.                                                 | `boolean` | `false`     |


## Events

| Event            | Description                                  | Type               |
| ---------------- | -------------------------------------------- | ------------------ |
| `glDrawerToggle` | Emitted when the drawer is opened or closed. | `CustomEvent<any>` |


## Methods

### `toggle() => Promise<void>`

Opens the drawer if it is closed and closes the drawer if it is open

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- ion-header
- ion-toolbar
- ion-buttons
- ion-title
- ion-button
- ion-icon
- ion-content
- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-drawer --> ion-header
  gl-drawer --> ion-toolbar
  gl-drawer --> ion-buttons
  gl-drawer --> ion-title
  gl-drawer --> ion-button
  gl-drawer --> ion-icon
  gl-drawer --> ion-content
  gl-drawer --> ion-thumbnail
  ion-button --> ion-ripple-effect
  style gl-drawer fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
