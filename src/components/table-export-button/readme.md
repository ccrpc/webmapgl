# gl-table-export-button

The export button component works with the [table export controller](../table-export-controller/)
to allow a user to export the contents of a [gl-table](../table/) either as a pdf or csv document.

<!-- Auto Generated Below -->


## Properties

| Property          | Attribute          | Description                                                                                                                                                                                                            | Type                        | Default     |
| ----------------- | ------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------- | ----------- |
| `author`          | `author`           | If this is for a pdf export, the author variable of the pdf document.                                                                                                                                                  | `string`                    | `undefined` |
| `customButton`    | `custom-button`    | If true the JSX component for the table-export-button will expose a <slot /> for a custom button whereas otherwise it is this.format.toUpperCase() aka CSV or PDF                                                      | `boolean`                   | `false`     |
| `filename`        | `filename`         | When the document is exported, what file name it should be saved as.                                                                                                                                                   | `string`                    | `undefined` |
| `format`          | `format`           | The format in which the document should be exported, either csv or pdf.                                                                                                                                                | `"csv" \| "pdf"`            | `"csv"`     |
| `pageOrientation` | `page-orientation` | The orientation of the exported document                                                                                                                                                                               | `"landscape" \| "portrait"` | `undefined` |
| `pageSubtitle`    | `page-subtitle`    | The subtitle of the document if the document is exported as a pdf                                                                                                                                                      | `string`                    | `undefined` |
| `pageTitle`       | `page-title`       | The title of the document if the document is exported as a pdf.                                                                                                                                                        | `string`                    | `undefined` |
| `tableId`         | `table-id`         | The id of the [table]{@link ../table} this export button is for.                                                                                                                                                       | `string`                    | `undefined` |
| `url`             | `url`              | If this value is defined the application will attempt to send the contents of the pdf in a POST request to the specified url when the export button is clicked, in addition to presenting it to the user for download. | `string`                    | `undefined` |


## Dependencies

### Depends on

- ion-button
- ion-thumbnail

### Graph
```mermaid
graph TD;
  gl-table-export-button --> ion-button
  gl-table-export-button --> ion-thumbnail
  ion-button --> ion-ripple-effect
  style gl-table-export-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
