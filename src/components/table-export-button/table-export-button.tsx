import { h, Component, Prop, State } from "@stencil/core";
import { findOrCreateOnReady } from "../utils";

@Component({
  tag: "gl-table-export-button",
})
export class TableExportButton {
  /**
   * If this is for a pdf export, the author variable of the pdf document.
   */
  @Prop() readonly author?: string;

  /**
   * When the document is exported, what file name it should be saved as.
   */
  @Prop() readonly filename: string;

  /**
   * The format in which the document should be exported, either csv or pdf.
   */
  @Prop() readonly format: "csv" | "pdf" = "csv";

  /**
   * The orientation of the exported document
   */
  @Prop() readonly pageOrientation?: "landscape" | "portrait";

  /**
   * The subtitle of the document if the document is exported as a pdf
   */
  @Prop() readonly pageSubtitle?: string;

  /**
   * The title of the document if the document is exported as a pdf.
   */
  @Prop() readonly pageTitle?: string;

  /**
   * The id of the [table]{@link ../table} this export button is for.
   */
  @Prop() readonly tableId?: string;

  /**
   * If this value is defined the application will attempt to send the contents of the pdf in a POST request to the specified
   * url when the export button is clicked, in addition to presenting it to the user for download.
   */
  @Prop() readonly url?: string;

  /**
   * If true the JSX component for the table-export-button will expose a <slot /> for a custom button whereas otherwise it is this.format.toUpperCase()
   * aka CSV or PDF
   */
  @Prop() readonly customButton: boolean = false;

  @State() exportCtrl: HTMLGlTableExportControllerElement;

  private async clickHandler() {
    if (this.exportCtrl == undefined) {
      this.exportCtrl = await findOrCreateOnReady("gl-table-export-controller");
    }
    this.exportCtrl.create(this);
  }

  private buttonClicked = () => this.clickHandler();

  render() {
    return (
      <ion-button ion-button onClick={this.buttonClicked}>
        {this.customButton ? <slot /> : this.format.toUpperCase()}
      </ion-button>
    );
  }
}
