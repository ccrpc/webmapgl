# Web Map GL

Web map components using Stencil and Mapbox GL. For usage of the components,
see the included [example application](src/index.html).

## Components

### Core

- [`app`](src/components/app)
- [`map`](src/components/map)
- [`style`](src/components/style)

### Basemaps
- [`basemap-switcher`](src/components/basemap-switcher)
- [`basemaps`](src/components/basemaps)

### Download
- [`download-button`](src/components/download-button)
- [`download-file`](src/components/download-file)
- [`download-list`](src/components/download-list)

### Drawing
- [`draw-controller`](src/components/draw-controller)
- [`draw-toolbar`](src/components/draw-toolbar)

### Drawer
- [`drawer`](src/components/drawer)
- [`drawer-toggle`](src/components/drawer-toggle)

### Feature List
- [`feature-list`](src/components/feature-list)

### Forms
- [`facet`](src/components/facet)
- [`field`](src/components/field)
- [`form`](src/components/form)
- [`form-controller`](src/components/form-controller)
- [`form-page`](src/components/form-page)
- [`option`](src/components/option)
- [`rest-controller`](src/components/rest-controller)

### Fullscreen
- [`fullscreen`](src/components/fullscreen)

### Geocoding
- [`address-search`](src/components/address-search)
- [`geocode-controller`](src/components/geocode-controller)

### Legends
- [`legend`](src/components/legend)
- [`legend-item`](src/components/legend-item)

### Likes
- `like-button`
- `like-controller`

### Popups
- [`attribute-values`](src/components/attribute-values)
- [`popup`](src/components/popup)

### Sharing
- [`share`](src/components/share)
- [`share-button`](src/components/share-button)

### Stories
- `story`
- `story-button`
- `story-step`

### Tables
- `table`
- `table-column`
- `table-column-visibility`
- `table-column-visibility-button`
- `table-column-visibility-toggle`
- `table-export-button`
- `table-export-controller`

## License
Web Map GL is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/webmapgl/blob/master/LICENSE.md).
Icons used in Web Map GL are from Ionicons and are licensed under
the [MIT license](https://github.com/ionic-team/ionicons/blob/master/LICENSE).
