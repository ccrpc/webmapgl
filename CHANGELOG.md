# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [0.18.2](https://gitlab.com/ccrpc/webmapgl/compare/v0.18.1...v0.18.2) (2024-08-26)


### Features

* Adding Skaffold to the project ([d02baa2](https://gitlab.com/ccrpc/webmapgl/commit/d02baa2a2e36729b93656a501c4e499cfc0133db))
* Updating side menu to match previous versions ([6d6c7d8](https://gitlab.com/ccrpc/webmapgl/commit/6d6c7d8612c560ba872d5f70ae00fcb13435e5ca))

## [0.18.1](https://gitlab.com/ccrpc/webmapgl/compare/v0.18.0...v0.18.1) (2023-09-13)

## [0.18.0](https://gitlab.com/ccrpc/webmapgl/compare/v0.17.80...v0.18.0) (2023-09-13)


### Features

* Adding a format option to gl-attribute-values ([3283063](https://gitlab.com/ccrpc/webmapgl/commit/328306331c5f14c83b55a7a30bde2e4645e2928a))
* The gl-drawer-toggle can now be disabled ([5aaadc6](https://gitlab.com/ccrpc/webmapgl/commit/5aaadc6df1c2ce2b2d3bdc8cbba28cdaba7e3c16))

## [0.18.1](https://gitlab.com/ccrpc/webmapgl/compare/v0.17.80...v0.18.1) (2023-09-13)


### Features

* Adding a format option to gl-attribute-values ([3283063](https://gitlab.com/ccrpc/webmapgl/commit/328306331c5f14c83b55a7a30bde2e4645e2928a))
* The gl-drawer-toggle can now be disabled ([5aaadc6](https://gitlab.com/ccrpc/webmapgl/commit/5aaadc6df1c2ce2b2d3bdc8cbba28cdaba7e3c16))

## [0.17.80](https://gitlab.com/ccrpc/webmapgl/compare/v0.17.73...v0.17.80) (2023-09-06)


### Bug Fixes

* Was changing id ([76b2665](https://gitlab.com/ccrpc/webmapgl/commit/76b26658f88c1b6be1e6022a35fc7a7e902fbfac))

### [0.17.4](https://gitlab.com/ccrpc/webmapgl/compare/v0.17.3...v0.17.4) (2021-01-27)


### Bug Fixes

* **popup:** fix broken import from @turf/helpers ([83b97b9](https://gitlab.com/ccrpc/webmapgl/commit/83b97b98db04cd6941d21363e703340ca0d21a5f)), closes [#25](https://gitlab.com/ccrpc/webmapgl/issues/25)

### [0.17.3](https://gitlab.com/ccrpc/webmapgl/compare/v0.17.2...v0.17.3) (2020-12-18)


### Features

* **map:** add support for click modes ([3feb039](https://gitlab.com/ccrpc/webmapgl/commit/3feb03996a94ee96414752855b3f2abf06851e5a))
* **style:** add support for insertion into another style (author: [@ethanallgood](https://gitlab.com/ethanallgood)) ([a6832d9](https://gitlab.com/ccrpc/webmapgl/commit/a6832d9febd2f7b244e2d858cecfaac7d3b56607))

### [0.17.2](https://gitlab.com/ccrpc/webmapgl/compare/v0.17.1...v0.17.2) (2020-04-08)


### Features

* **story-step:** add click handling options ([9c53274](https://gitlab.com/ccrpc/webmapgl/commit/9c53274c0e331dcd2237dbf38a9984bc88162aa1))

### 0.17.1 (2020-04-08)


### Features

* **drawer:** add toggle event ([84fc636](https://gitlab.com/ccrpc/webmapgl/commit/84fc63681fc2afa65aeb30c80e3a41f138697be1))
* **popup:** add property to control close mode ([6722763](https://gitlab.com/ccrpc/webmapgl/commit/6722763a96610bcb0d49a2f35d16453280011f3c))
* **story:** added story, button, and step components ([a906cd9](https://gitlab.com/ccrpc/webmapgl/commit/a906cd9f82cc390b0ab86f1e6c3458923f87a3d0))
* **story-button:** add English strings for story button ([80775bf](https://gitlab.com/ccrpc/webmapgl/commit/80775bf31b8554feb4a66d964a1294ebbdcaea53))
* **story-step:** support selecting click features by ID ([f198bf9](https://gitlab.com/ccrpc/webmapgl/commit/f198bf95dcdb2a0f646577243f588a1f7e6b5286))


### Bug Fixes

* **story-button:** fix selectors for story element ([42196e2](https://gitlab.com/ccrpc/webmapgl/commit/42196e23d3740a2ad0b3ecefcc3bf19434183b87))

# [0.17.0] - 2020-01-30
- Added `hash` property to `gl-map` to sync URL parameters with map location
  (#15).
- Use `@stencil/core@1.8.6` and `@ionic/core@4.11.10`.
- Fix deprecations in preparation for Ionic 5 (#17).

# [0.16.0] - 2019-08-28
- Added `csv` and `pdf` boolean property to `gl-column` for export filter (#21)
- Added `url` property to `gl-table-export-button`

# [0.15.0] - 2019-06-12
- Added `gl-share` and `gl-share-button` components.
- Use `@stencil/core@1.1.6` and `@ionic/core@4.6.2`.
- Added `cell-class-attribute` property to `gl-table-column` (#12).
- Removed or renamed reserved properties.

## Breaking Changes
- `gl-form`: `translate` is now `translateText`
- `gl-style`: events contain an object with properties `id` and `style`

See the [breaking changes for Stencil
One](https://github.com/ionic-team/stencil/blob/master/BREAKING_CHANGES.md)
for upgrade instructions.

# [0.14.0] - 2019-06-21
- Use `@ionic/core@4.5.0`, Mapbox GL 1.0.0, and Mapbox GL Draw 1.1.2.
- Added `max-width` property to `gl-popup` (default: `none`) (#11).

# [0.13.0] - 2019-06-03
- Added column visibility components from TIP application.

# [0.12.0] - 2019-05-30
- Align value text in `gl-attribute-values` right by default.
- Added `gl-download-button`, `gl-download-file`, and `gl-download-list`
  components for file download.
- Added `gl-table` and `gl-table-column` components.
- Fixed unecessary retrieval of `gl-map` in `gl-style`.
- Added `gl-table-button` and `gl-export-controller` from TIP application.

# [0.11.0] - 2019-04-11
- Added `gl-attribute-values` component.

# [0.10.0] - 2019-04-09
- Wait for style to be ready in `gl-legend-item`.
- Use `@ionic/core@4.2.0` and `@stencil/core@0.18.1`.

# [0.9.0] - 2019-02-28
- Use `@ionic/core@4.0.2` and `@stencil/core@0.18.0`.
- Moved `text-wrap` property to `ion-label`.

# [0.8.1] - 2019-02-06
- Don't attempt to fetch style JSON when the URL is not set.

# [0.8.0] - 2019-01-31
- Added `glpyhs` and `sprite` properties to `gl-map`.
- Fall back to the first `glpyhs` and `sprite` URLs found in the child styles
  if these properties are not set on `gl-map`.

# [0.7.0] - 2019-01-29
- Use `@ionic/core@4.0.0` and `@stencil/core@0.17.0`.
- Added `split-pane` property to `gl-app`.
- Added `toolbar-start-buttons` and `toolbar-end-buttons` slots to `gl-drawer`.
- Added `map-id` property to `gl-drawer`.
- Added `gl-address-search` component from Transportation Voices 2045.

# [0.6.0] - 2019-01-04
- Use `@ionic/core@4.0.0-rc.0`, `@stencil/core@0.16.1`, and `screenfull@4.0.0`.
- Added ability to provide a custom menu in `gl-app`.
- Added a delay to `gl-popup` to allow it to consolidate click events
  from multiple layers.

# [0.5.1] - 2018-12-05
- Fixed popup coordinates.

# [0.5.0] - 2018-12-05
- Removed unused dependency on `@stencil/router`.
- Added getters and setters for paint and layout properties.
- Made all public methods asynchronous.
- Handle multiple and non-point features in popups.
- Consolidated components, and exposed properties to avoid relying on methods.

# [0.4.0] - 2018-08-29
- Added zoom methods to gl-map.
- Made gl-form-page and gl-draw-toolbar responsive.
- Wrap text in form elements.
- Trigger an event on gl-like-button click.
- Wrap basemap names in switcher.
- Added header facet widget.
- Added facet class to form page.
- Block click actions on disabled buttons.
- Use polyglot for translation.
- Added Spanish translation.
- Made forms translatable.
- Fixed map resize on drawer close.
- Fixed an issue that caused the like button not to update.
- Added `fitBounds` method to map.
- Fixed map content area display when multiple components are present.
- Use infinite scroll to limit the number of items rendered in a feature list.
- Added `getCenter` and `setCenter` methods to map component.
- Fixed response for Nominatim reverse geocoding.
- Changed gl-feature-list to work without a map component.
- Handle form schemas without facets or fields.
- Handle geocoding errors.

# [0.3.1] - 2018-06-08
- Prevent form submission if there are no fields in the page.

# [0.3.0] - 2018-06-05
- Made feature a property of the like button.
- Changed feature list to be source-based.
- Added easeTo and flyTo methods to gl-map.
- Removed gl-property and gl-template components.
- Added glFeatureAdd event.
- Use flexbox for gl-drawer.

# [0.2.1] - 2018-06-01
- Ensure that gl-form-page is loaded by dependent modules.

# [0.2.0] - 2018-06-01
- Refactored forms to use a JSON schema file.

## [0.1.2] - 2018-05-30
- Added detail option to facet.
- Fixed extra height in gl-modal-form scroll area.

## [0.1.1] - 2018-05-29
- Added TS definitions for screenfull.
- Added facet back button in modal form.

## [0.1.0] - 2018-05-24
- Initial release
